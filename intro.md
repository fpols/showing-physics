# Showing Physics

```{figure} front.jpg
---
width: 40%
align: center
---
```

This book presents a selection of the 99 'best', most beautiful physics demonstrations from the Dutch book series "Show*de*Fysica" {cite:t}`showdefysica1,showdefysica2,showdefysica3` as published by the <a href="http://nvon.nl" target="_blank">Dutch Society of Science Teachers</a>. Where most physics demonstration books cover a large range of demonstrations, this book includes also various strategies to make the most of these! 

The demonstrations are structured using the four categories:
* Demonstrations on nature of science
* Demonstrations on teaching scientific inquiry
* Demonstrations for conceptual development
* Demonstrations for special occassions

So, whether you want to deepen students' understanding of a specific topic, want to engage them in thorough thinking or if you were asked to demonstrate physics on a festive occasion, you can find demonstrations and inspiriation in this book. 

## References
```{bibliography}
:filter: docname in docnames
```