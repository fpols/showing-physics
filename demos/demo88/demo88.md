

# Cloud Formation


Author: Norbert van Veen\
Time:	  10 minutes	\
Age group:	12 - 18\
Concepts:	Clouds, evaporation, condensation

## Introduction
Clouds captivate children's imagination. On a beautiful day, or when looking down from an airplane they resemble cushions. Often, students do not know how they form. This demonstration quickly and clearly illustrates this. The demonstration is well-suited for meteorology.

```{figure} demo88_figure1.jpg
---
width: 50%
align: center
---
Vacuum bell jar, pump, match, and beaker with hot water
```

## Equipment
- Vacuum pump connected to bell jar
- Beaker with hot water
- Box of matches
- Towel (to remove condensation from the bell jar after the demonstration)

## Preparation
Set the bell jar and vacuum pump on a table clearly visible to all studens. Heat some water in a beaker or use hot tap water, ensuring it's not too hot (between 50 and 70°C).

## Execution
1. Place a beaker of hot water under the bell jar. Strike a match and when it goes out, ensure that the smoke ends up under the bell jar by placing the jar over the match.
2. Start the vacuum pump. The water will start to evaporate. Optionally, you can place a thermometer or temperature sensor under the jar to demonstrate that evaporation extracts energy from the liquid, thus lowering the water temperature.
3. Clouds form under the jar at a certain air pressure. The jar is no longer transparent.
4. Would clouds form if you hadn't used the match?
   1. Yes, there would be more cloud formation;
   2. No, there would be no cloud formation;
   3. It makes no difference.
5. Control question: In what practical situations does this type of cloud formation occur?

## Physics Background
Due to the low pressure, the water in the beaker evaporates faster, causing the temperature in the beaker to drop. A lot of water vapor enters the bell jar. At a certain air pressure, water vapor condenses on the present smoke particles.

At high altitudes, the air pressure is also lower, allowing water vapor to condense on suitable condensation nuclei, such as particles emitted by an airplane engine. Because the water droplets in the cloud scatter sunlight of all colors equally, clouds appear white.

There are variations of this experiment, where a soda bottle with some alcohol or water drops in it is pressurized using, for example, a bicycle pump. The pressure is then rapidly decreased (by releasing the cap), after which a cloud becomes visible.

```{tip}
Search for 'Cloud Bottle' for a student version of this experiment.
```