

# Upward and downward force


Author:     \
Time:	15 minutes  	\
Age group:	Grade 10\
Concepts:	weight, buoyant force, action/reaction, pressure

The demo is derived from the demo "the-bolt-in-the-beaker" from the York Science Education Center.

## Introduction
This is a typical example of a Predict-Explain-Observe-Explain demonstration about weight and buoyant force. Students predict what happens to the mass indicated on the scale when a weight is hung in water and explain their prediction. This demonstration can lead to a lot of confusion, even amongst physicists that see it for the first time. 

```{figure} demo26_figure1.jpg
---
width: 50%
align: center
---
A cup of water on an electronic balance. We hang a substantial screw in the water or another metal object.
```

## Equipment
* Electronic scale (accuracy to 0.1 g)
* Beaker or drinking glass
* String
* Substantial screw or other solid metal object 
* Spring balance (e.g., 10 g)

## Preparation
Set up the needed materials on the table.

## Procedure
1. Present the setup (figures 1a and 2a). Explain that you will lower the screw into the water and ask for individual predictions in multiple-choice format, as follows: \
What will happen to the mass indicated on the balance?
   1. Will increase
   2. Will remain the same
   3. Will decrease 
   
   How do you explain this?
   1. The weight of the screw is fully supported by the string.
   2. There is an buoyant force on the screw and therefore an equal force downward on the cup.
   3. The screw displaces some water by pushing it upward.
   4. Other, namely ...... (fill in).

2. Allow students to discuss their predictions with each other.
3. Plenary: Survey the predictions and the arguments for them.
4. Conduct the experiment and write down the results on the board.
5. Have students write down a complete explanation before the discussion starts. *If we rest the screw on the bottom, what will the scale indicate? Why?*
6. The teacher writes down some student explanations on the board, after which the discussion begins. Don't forget to conclude the discussion with a brief but complete explanation on the board including a force diagram.
7. Repeat with string attached to the spring balance instead of the stand.



## Physics background
The screw experiences an buoyant force equal to the weight of the displaced liquid. Therefore, there is an buoyant force from the water on the screw ($F_{\text{water on screw}}$) and hence, according to Newton's 3^{rd}^ law, a downward force from the screw on the water ($F_{\text{screw on water}}$). The latter force acts on the scale. An alternative explanation is that the water level rises, increasing the pressure on the bottom ($p = F/A = ρgh$) and thus increasing the force on the scale.<br>
If the string is connected to a spring balance, the increase on the scale will be exactly equal to the decrease in force on the spring balance. This can be demonstrated at the end. Use the setup without the spring balance first.

```{tip}
There will be clever students who still get it wrong. Console them with the information that many physicists still make mistakes here.
```
