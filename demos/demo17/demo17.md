

# Who Dares? Stable equilibrium using small bottles


Author:     Wouter Spaan\
Time:	  	5 minutes\
Age group:	14+\
Concepts:	(Stable) equilibrium, gravity, normal force, line of force

## Introduction
This demonstration demonstrates the stability of an equilibrium when the center of mass is directly below its support. You can present this experiment as a challenge to the students, engaging them meaningfully. 

## Equipment
* Flat bottle opener
* Two bottles with crown cork caps
* Extra bottle-opener (of any design)
* Cleaning utensils (if desirable) 

## Preparation
None, except gathering the equipment. The performance is more convincing if you prepare the set-up in front of the students instead of before the lesson.

## Procedure
In my experience it is best to set up the bottles while the students are watching, emphasizing the importance of the upper bottle by very diligently placing it on top of the opener while pushing the opener down demonstratively. You could provide a cleaning cloth or a mop in plain sight, warning the students that whoever makes a mess should clean it himself.

Presumably you will have everyone’s attention by now, so it is time to put up the challenge: drink from the upper bottle without touching the lower bottle or the bottle opener and without using any other equipment except for one extra bottle opener (only to be used to open the top bottle). Pretty soon a student will say something like: ‘you can just grab it’. The teacher’s challenge than becomes to test this student’s self-assurance. Is he or she sure enough about this possibility to try it? Is the student willing to clean up the mess he might make? Of course, you also ask why the student thinks he can take the top bottle without breaking anything. When you feel the time is appropriate, the student can actually try his solution (and thus collect his reward).

If no student suggests the correct solution, you can play a little trick with your class. Take another bottle from below your desk with some other drink than is in the upper bottle (in the pictured case something else than coke). Now you slowly change this bottle with the original top bottle, lifting the original one clearly, saying something like: ‘perhaps you do not like coke?’

```{tip}
This demonstration can provide a valuable introduction to the differences between a stable, an unstable and an indifferent equilibrium. You can show that the system is in stable equilibrium by pushing the bottle opener (without the top bottle, but with the lower bottle) towards the edge of the table. Right before it topples, it will tilt slightly until the shifted center of gravity is exactly under the edge of the table, which now constitutes the support point.
```

## Physics background
The center of gravity of the lower bottle is below the support, formed by the bottle opener. Therefore, the system is in equilibrium even without the top bottle. Depending on the (required) level of the students you can explain this more thoroughly by drawing the appropriate lines of action. The lines of action of the gravitational force and the normal force are exactly aligned. The line of action of the normal force will position itself ‘automatically’ in this exact configuration, which is possible whenever the center of gravity is below the supporting surface. In fact, the normal force probably consists of many small normal forces. Adding them correctly will result in one single resultant normal force with the described property.

