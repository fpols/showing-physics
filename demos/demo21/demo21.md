

# Making Einstein young again with an overhead projector
Author:     Maarten van Woerkom\
Time:	  	15 minutes\
Age group:	14 and up\
Concepts:	object distance, image distance, focal length, thin lens formula 

## Introduction
With a clever use of the overhead projector we can make the old Einstein young again. See figures 1 and 2 and provide a nice conceptual exercise with the thin lens formula.

## Equipment
* An overheadprojector (OHP)
* A box (see instructions below)
* A white carton screen
* A fixed screen or white wall
* Transparencies of the old and young Einstein (figure 1).

Construct a box with a transparent top and bottom of Perspex or glass with a distance of about 6 cm between top and bottom. On the top put a transparency with the old Einstein and on the bottom the young Einstein. The sides of the box are opaque so the young Einstein is hidden from view. Surrounding the bottom of the box is a black mask (cut from black carton) which covers the remaining part of the overhead projector (figure 2). 

```{figure} demo21_figure1a.jpg
---
width: 50%
align: center
---
some caption
```

```{figure} demo21_figure1b.jpg
---
width: 50%
align: center
---
some caption
```

```{figure} demo21_figure2a.jpg
---
width: 50%
align: center
---
some caption
```

```{figure} demo21_figure2b.jpg
---
width: 50%
align: center
---
some caption
```

## Preparation
Put the box on top of the overhead projector (figure 4). At the start adjust the overhead projector such that the old Einstein is on the screen.
```{figure} demo21_figure3.jpg
---
width: 50%
align: center
---
some caption
```


## Procedure

1.	The old Einstein is projected on the screen. When you put the white carton on top of the fixed screen, one sees a sharp image of the old Einstein. However, when the white carton is moved in the direction of the OHP the old Einstein fades away and the young Einstein appears. When you have moved the carton 1 or 2 meters towards the OHP, then only the young Einstein appears.  Pay attention to details such as the “growth” of the hair of the moustache and the hair on the head.  

2.	Again start with the old Einstein. Then move the head of the OHP (with mirror and lens) downward. The old Einstein fades away and the young Einstein appears. Moving the head of the OHP up again shows the reverse process, the old Einstein reappears at the expense of the young Einstein. 

```{figure} demo21_figure4.jpg
---
width: 50%
align: center
---
some caption
```

```{tip}
* The glass/Perspex top and bottom should be surrounded by carton so the audience cannot look inside and cannot see the young Einstein picture. The audience only sees the box with on top the old Einstein. The audience often suspects that there is some kind of computer inside the box. This computer is thought to change the image.  
* A three-layer box is possible, but complicated and it is difficult to tune the distances properly.
* Instead of Einstein one could also use the head of a colleague in the school, but then you need an expert drawer of portraits. 
* This demonstration can also be done as a laboratory experiment for students in different ways: 
    * By letting students work with the OHP. 
    * By letting students use typical laboratory materials. In that case one needs the portraits as slide to be placed in slide holders of a typical lab optics set. 
```


## Physics background
1.	You start with object distance o1 and image distance i1. When moving the white carton towards the projector, the image distance becomes smaller: i2.
But with a smaller image distance, the object distance becomes greater: o2. At that distance is the transparency with the young Einstein (figure 3) which now shows well defined on the screen. Because of the small “depth of field” of the lens, the old head is not visible anymore, unless the height of the box is too small.  

2.	Again start with object distance o1 and image distance i1. When the head (mirror + lens) of the OHP is moved downward, then the image distance i2 will not change. So i = i1=i2, the object distance will decrease. 
So the object distance must remain the same as well. But at that distance is now the young Einstein instead of the old one. So if the head moves down by a distance which is the same as the height of the box, then the young Einstein will have a sharp image on the screen.  
