# Stick-slip motion, kinetic versus static friction

Author:     Ineke Frederik\
Time:	  	  15 minutes\
Age group:	16 - 18\
Concepts:	  kinetic and static friction

Inspired by {cite:t}`Balsma`.

<div style="display: flex; justify-content: center;">
    <div style="position: relative; width: 70%; height: 0; padding-bottom: 56.25%;">
        <iframe
            src="https://www.youtube.com/embed/8SAOsvKrJk8"
            style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
        ></iframe>
    </div>
</div>


## Introduction
Friction is an everyday phenomenon. How would the world behave without friction? We describe two inexpensive demonstrations which show very clearly the difference between friction when there is no movement (static friction) and friction when two surfaces move relative to each other (kinetic friction). Many interesting applications can be found when static friction becomes kinetic friction.   

## Equipment
* Smooth pvc-tube (diameter32 mm) 
* piece of cloth which easily slips over the surface of the tube (figure 1)
* small metal tray  (figure 2)
* spring (not stiff)
* string
* washers.  

## Preparation
1. Test whether the cloth slides easily across the tube. Check whether it slides off towards one side as soon as the tube is tilted a bit. 
2. Connect the spring to the small metal basket and attach a string on the other side of the spring. Make the container heavier using some washers or weights. 


## Procedure
1. Hang the piece of cloth across the horizontal pvc-tube. One end should be lower than the other, but the cloth should not slide sideways (static friction)! Then move the tip of the tube gradually downward. The cloth starts to slide, not only downward, but also sideways! As soon as the cloth starts moving a bit, it will also slip sideways and rapidly slide off the tube. When starting to move, static friction becomes kinetic friction. Kinetic friction is smaller than the static friction which kept the cloth in its place initially. 
```{figure} demo14_figure1.jpg
---
width: 50%
align: center
---
Here it is shown that static friction is larger than kinetic friction.
```

2. Put the tray on the floor then pull the string and walk with constant speed across the room, it is a bit like walking a dog (here the tray). The tray will jump ahead, stand still, jump ahead, etc. Ask your students for more examples of this phenomenon.

```{figure} demo14_figure2.jpg
---
width: 50%
align: center
---
Stick-slip demonstrated
```

## Physics background
When the piece of cloth is not moving, static friction ensures that the cloth does not move, even though one side is longer than the other. Once it starts moving, friction becomes kinetic rather than static and this kinetic friction turns out to be insufficient to compensate for the difference in weight of both sides of the cloth. 

Through static friction the tray remains at rest until the force is big enough to overcome friction and the tray starts moving. While moving, the spring becomes shorter and yet the tray keeps moving, kinetic friction is smaller than static friction. That is until the spring becomes so short that the pulling force is smaller than kinetic friction. Now the tray stops and the process repeats itself. 

Stick-slip is jerking motion which occurs when a moving object stands still for a moment (sticks) and then jumps forward (slips). The effect may repeat itself continuously. It is the same phenomenon, which occurs when a piece of chalk is moved perpendicular to a traditional blackboard and produces a very unpleasant screeching sound. But also the screeching brakes of a car and the movement of a bow across the violin strings are examples of stick-slip friction, as well as earth quakes when layers of rock move across each other.   
A bag drawn across a table with a string might exhibit stick-slip motion as well.

```{tip}
YouTube has many stick-slip examples.
````

## References
```{bibliography}
:filter: docname in docnames
```