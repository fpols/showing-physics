

# Why does the water rise?

Author:     \
Time:	15-30 minutes  	\
Age group:	Grade 10 and up\
Concepts:	heating and expansion of air, pressure, combustion


```{figure} demo27_figure1.JPG
---
width: 50%
align: center
---
Place the jam jar over the burning candle.
```

## Introduction
Everyone knows that, when placing a glass over a burning candle, the candle extinguishes when oxygen in the glass runs out. But is that the whole story? What exactly are you observing in this demonstration? Even when we're all looking at the same thing, we often see very different things.

What causes the liquid level in the glass to rise? This can be explained in different ways. What are the arguments for and against each explanation? Do those arguments fit all observations?

From observation alone, no scientific insight follows, especially not when more than one explanation seems plausible. The result of research is often the conclusion that more research is needed. Sometimes even for an old and stale problem like this...
The teacher performs the experiment but mainly directs the discussion to generate these insights.

## Equipment
* Candle
* Plate 
* Glass 
* Some water 
* (optionally) coloring 
* Matches

## Preparation
Pour some water into the plate, if possible with coloring, and place the candle in the middle and light it. Have the glass ready. It must be large enough that it does not touch the candle or flame when placed over it.

## Procedure
A detailed scenario for a Predict-Observe-Explain approach for this demo can be found at the bottom of this page. You will find here an outlined, hypothetical lesson plan, with suggestions for questions and assignments. 

If you place the glass over the burning candle, it will extinguish after a while, causing the water level in the glass to rise. At least three explanations are plausible for this:
1. The water takes the place of the oxygen that is consumed during combustion.
2. When the flame goes out, the temperature drops. According to the general gas law, the pressure and/or volume decrease. Atmospheric pressure pushes water inward until a new equilibrium is reached.
3. Water is formed in the flame, which precipitates when the flame goes out. Then the number of particles in the gas decreases, so does the pressure. The atmosphere pushes water inward until a new equilibrium is reached.

Let students brainstorm arguments for and against each explanation, exchange them, and come to a conclusion. The teacher can also ultimately contribute their own input.

```{figure} demo27_figure2.JPG
---
width: 50%
align: center
---
The candle is extinguished. Why did the liquid level rise?
```


## Physics background
(For explanation 3). With complete combustion of candle wax (mainly paraffin or stearin), roughly for every two molecules of O$_2$, one molecule of CO$_2$ and two molecules of H$_2$O are formed. When the flame extinguishes, the temperature drops sharply in that area, and H$_2$O precipitates there. The pressure in the glass quickly drops, the atmosphere pushes water inward until equilibrium is reached.

(For explanation 2). Heat release to the surroundings of the glass also causes pressure reduction according to the general gas law. However, this process is slow, while the liquid level rises noticeably quickly.

(For explanation 1). For every oxygen molecule in the air, exactly one water molecule is formed. If that precipitates, the resulting space can be filled by liquid. If we ignore the formation of CO$_2$, then even explanation 1 (according to quite a few biology textbooks the 'correct' one) is somewhat true.

More important than the 'correct' explanation here is that students themselves come up with, defend, and evaluate arguments for the explanations. Another important aspect is that a need arises for empirical evidence.

```{figure} demo27_figure3.jpg
---
width: 50%
align: center
---
The balloon on the bottle inflates when you place the bottle in hot water.
```

```{tip}
Illustration of explanation 2: place an empty balloon over the opening of a bottle, and place it in a bowl of hot water (Liem, 1989, p. 36). **REFERENCE!!!!** Due to the heating of the air in the bottle, the balloon inflates, an illustration of Charles's law ($V/T = \text{constant}$ if $p$ and $N$ do not change).
```

Illustration of explanation 3: bring some water in an empty soda can to a boil, then quickly invert it in a bowl of cold water, with the opening below the water surface. The can collapses because the water vapor condenses.

## Follow-up
With three candles instead of one, the final water level is higher (Liem, 1987, p. 37). Which of the explanations does this observation correspond to?

```{warning}
Hold the can in the illustration of explanation 3 with tongs or oven mitts.
```

## References
```{bibliography}
:filter: docname in docnames
```


## Worksheet

Bijlage van ShowdeFysica, demo03, Waardoor stijgt het water?
Aanvulling op demonstratie nr 03
Waardoor stijgt het water?
Een nieuwe kijk op een oud proefje
Scenario bij een POE-aanpak
**Predict**
Giet wat (gekleurd) water in het schoteltje, zet de kaars erin en steek
die aan. Zet het glas gereed. Leg uit dat je zo meteen het glas over
de kaars gaat zetten.
Vraag om een voorspelling van wat er gaat gebeuren. Zelfs heel jonge kinderen weten
dat de kaars na een tijdje uit zal gaan. Oudere leerlingen weten misschien ook al dat de
vloeistof op zal stijgen in het glas.
(Het is mooi als voorspellingen door leerlingen wel deels, maar niet helemáál waar zijn.
Dan is het waarnemingsresultaat niet deprimerend maar ontstaat toch ruimte tot leren.)
Observe.
Geef de opdracht om zorgvuldig waar te nemen wat er gebeurt vanaf het moment dat je
het glas over de kaars zet, en dat zo precies mogelijk op te schrijven. Plaats dan het glas
en wacht af.
Vergelijk daarna de waarnemingen. Iedereen zal gezien hebben dat de kaars uit gaat en
sommigen, dat er aan het begin luchtbellen ontsnappen. Meestal ziet niet iedereen, dat
het vlammetje geleidelijk aan kleiner wordt, niet plotseling. Bijna iedereen ziet dat het
water stijgt in het glas, maar het valt niet altijd op dat dit pas echt begint als de vlam uit
is. Sommigen merken op dat de pit nog een tijdje rookt als hij al uit is, en een enkeling
zelfs, dat er wat condens op het glas neerslaat.
Vrijwel altijd blijkt dat niet iedereen hetzelfde waarneemt. De docent kan opmerken dat
dergelijke verschillen ook bij wetenschappers heel gewoon, soms zelfs noodzakelijk zijn.
Bijvoorbeeld, omdat ze geïnteresseerd zijn in verschillende aspecten van de gebeurtenis:
voor een bioloog, scheikundige of natuurkundige zijn verschillende aspecten van het
dovende kaarsje interessant.
Explain.
Leerlingen leren dat zuurstof nodig is voor verbranding. Is de zuurstof op dan gaat het
kaarsje uit. In sommige biologie-schoolboeken staat deze proef met als uitleg: het water
neemt de plaats van de verdwenen zuurstof in. Daarom stijgt het op. Het water vult vaak
inderdaad een deel van het glas dat ruwweg past bij het percentage zuurstof in de
atmosfeer volgens Binas: 21%.
Maar dat is vreemd: volgens de scheikunde zijn de zuurstofatomen er gewoon nog,
alleen in andere verbindingen dan in het begin. Als er nog precies evenveel atomen zijn,
hoe kan er dan ruimte ontstaan voor het water om te vullen? Wat denken de leerlingen
daarvan?
Tot dusver zijn we drie verklaringen tegen gekomen, maar er kunnen er nog wel meer
zijn:
1. Het water neemt de plaats in van de zuurstof die bij verbranding opgebruikt is.
2. Als de vlam dooft daalt de temperatuur. Volgens de algemene gaswet nemen dan de
druk en/of het volume af. De atmosferische druk duwt water naar binnen tot een
nieuw evenwicht is ontstaan.
3. In de vlam ontstaat gasvormig water. Als de vlam dooft slaat het water neer. Dan
neemt het aantal moleculen in het gas af, dus ook de druk. De atmosfeer duwt water
naar binnen tot een nieuw evenwicht ontstaat.
Jampot wordt over
brandende kaars geplaatst.
In het schoteltje zit water.
Bijlage van ShowdeFysica, demo03, Waardoor stijgt het water?
Laat leerlingen de verklaring kiezen die hen het beste lijkt en argumenten voor de eigen
verklaring en tegen die van de anderen bedenken. Argumenten zijn aannemelijker
naarmate ze beter passen bij al bekende theorie, en naarmate ze beter passen bij de
waarnemingen. Op basis van die criteria kunnen de groepjes hun visies vergelijken.
Afsluitend kan besproken worden: het is helemaal niet vreemd voor wetenschappers om
het oneens te zijn. Ook zij kunnen verschillende waarnemingen belangrijk vinden en
verschillende verklaringen accepteren. De slimste en creatiefste wetenschappers vinden
de beste waarnemingen en argumenten voor hun eigen gelijk en tegen dat van anderen.
Dit is daarom een uitstekende aanpak gebleken voor de snelle ontwikkeling van
betrouwbare wetenschappelijke kennis.

### Follow-up
**Observe**
Zet, om een keer goed het waarnemen te oefenen, een webcam bij het experiment en
speel de gebeurtenissen opnieuw af. Laat leerlingen opnieuw hun kwalitatieve
waarnemingen noteren, en tijdstippen daarbij aangeven. Bouw op het bord een
beschrijving van de gebeurtenissen en tijdstippen op. Vervolgens kan de discussie van
verklaringen steeds gekoppeld worden aan directe verwijzingen naar de observaties. Zo
komt het wetenschappelijk gehalte van de activiteiten al op een wat hoger niveau.

**Explain**
In plaats van leerlingen mogelijke verklaringen voor te schotelen kun je ze die ook zelf
laten bedenken, en daarbij een praktische manier om die zelf te testen. Dat gaat wel
(veel) meer tijd kosten.
Doe je deze proef met drie kaarsen in plaats van één, dan komt het uiteindelijke water
niveau hoger (Liem, 1987, p. 37). Met welke van de verklaringen stemt deze
waarneming overeen?
Het valt te overwegen sensoren in het glas aan te brengen, het temperatuur- en
drukverloop gaandeweg het proces te registreren, en met videometen het volumeverloop
vast te stellen. Zo is misschien kwantitatief vast te stellen welke vorm van de algemene
gaswet het best dit proces beschrijft. Dit is (nog) niet uitgeprobeerd.