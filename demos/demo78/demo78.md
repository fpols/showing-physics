

# Rotating Balloon

Author:     \
Time:	  	10-20 minutes\
Age group:	16 - 18\
Concepts:	

## Introduction
Many students find centripetal force difficult to grasp. The misconception of a 'centrifugal force' is constantly looming. This experiment challenges students to seek an explanation for the displayed situation. By using the correct scientific terminology, you can encourage students to move away from the possible misconception of centrifugal force. This experiment can contribute to a 'conceptual change.'

```{figure} demo78_figure1.JPG
---
width: 50%
align: center
---
The setup.
```

## Equipment
- Balloon
- Unwound cord
- Dark-colored liquid
- Hanging point

## Preparation
Fill a balloon with dark-colored liquid. Inflate the balloon partially and tie it closed. Attach a cord to the balloon and hang the balloon freely. An example of a setup is shown in the figure.

## Procedure
1. Rotate the balloon several times so that the cord winds around itself.
2. Release the balloon. The cord will now unwind, and the balloon will start rotating at a considerable angular velocity.
3. Due to the rotation, the liquid in the balloon will assume a significantly different position inside the balloon. At a significant angular velocity, a situation may arise where a ring of liquid forms along the wall of the balloon at a certain height.
4. *What is the explanation for the phenomenon?* The expectation is that many students will come up with a wrong explanation, but that is not initially a problem. Present the physically correct explanation yourself and have the students compare it with their own (possibly) incorrect or incomplete explanation. If the student accepts that their own explanation is wrong and accepts the correct explanation, 'conceptual change' can occur.
5. Control question: *Provide students with a schematic diagram or photo of the situation where the balloon is rotating. Determine the rotation speed of the balloon from the photo or diagram.*

## Physics background
If you consider the liquid in the balloon as one object, you can state that there is a normal force from the balloon on the liquid. When the balloon rotates, a certain centripetal force is required to also rotate the liquid. Due to the rotation, a greater normal force is required, which is higher in the balloon. This results in an optimal relationship between the velocity of the liquid and the radius of the circular path of the liquid.

```{tip}
*	The experiment works well only when there is a high rotation speed of the balloon. In a wound cord, there is torsion, so you cannot achieve high rotation speeds.
*	Another way to rotate the balloon is by attaching it to a cord connected to a cordless drill.
*  It takes some trial and error to get the optimal amount of liquid in the balloon. If the mass of the liquid is too large, the moment of inertia of the balloon is too large, and the effect does not occur or is too small. If the amount of liquid is too small, the phenomenon is not clearly visible. A quantity of about 200 mL works well.
```

## Follow-up
The variables in this experiment are the size of the balloon, the amount of liquid in the balloon, and the rotation speed. You can vary these parameters extensively, with particular emphasis on varying the rotation speed (by winding the wire more or less), providing many possibilities.