

# Cooling Metal Spheres

Author:     \
Time:	  	20-30 minutes\
Age group:	15 - 18\
Concepts:	

## Introduction
To let students experience the process of cooling solid objects, we observe the cooling of iron spheres with different diameters using a FLIR camera (C5). It is clearly visible that the smallest sphere cools down faster than the larger spheres. An experiment also conducted in the 18th century by George-Louis Leclerc to determine the age of the Earth.

```{figure} demo75_figure1.jpeg
---
width: 50%
align: center
name: fig:demo75_setup1
---
The setup with a plastic test tube rack to place the balls on.
```




## Equipment
- Setup with tripod and clamp or a camera tripod
- IR camera
- Non-conductive construction to place the metal spheres on (plastic test tube rack)
- Several metal spheres (of the same material) differing in diameter
- Kettle
- Ladle
- Tea towel.
This description uses Coach 7 as measurement software. The required Coach 7 file is available on the website. LINK NEEDED?

## Preparation
1. Set up a measurement setup as shown in Figures {numref}`{number}<fig:demo75_setup1>` and {numref}`{number}<fig:demo75_setup2>`.

```{figure} demo75_figure3.jpeg
---
width: 50%
align: center
name: fig:demo75_setup2
---
Another view of the setup looking at the test tube rack to place the balls on and an IR camera filming.
```
2. Set the IR camera to the correct IR distance and visual distance. Set up a measurement with a measuring spot or, if desired, a measurement with a 'hottest spot rectangle' (drag it to the desired size on the touchscreen). Set the emissivity to around 0.95 (for iron), look up the value of this emissivity for other metals if spheres of other materials are used.
3. When using Coach 7, set up 'measurement with synchronized display'. You can also film the setup with any camera app on your device, then select the FLIR C5 as the camera.

## Procedure
1. Show the spheres to the students and give them the diameters of the spheres. Let them calculate the mass using the material density.
2. Let the students calculate how much thermal energy a sphere possesses when it heats up from room temperature to a certain temperature (for example, 60°C). They can find the specific heat of the material in BINAS.
3. *Are the temperatures of the spheres at the start of the experiment the same or different?*
4. *What do you expect to happen?* 
    1. The largest sphere will cool down the fastest.
    2. The smallest sphere will cool down the fastest.
    3. Cooling will occur at the same rate for both spheres.
    Preferably, have them write down their choice and come up with an explanation.
5. Boil some water in the kettle, leave the lid of the kettle open and place the spheres in a ladle in the kettle. Wait a moment until the metal spheres are heated. Remove the ladle from the kettle and place them on a tea towel to quickly dry the spheres. Then place the spheres in the desired position in your measurement setup.
6. Perform the measurement. After heating the spheres in boiling water, quickly place them in the setup. Start the measurement with the FLIR IR camera.
```{figure} demo75_figure2.jpg
---
width: 50%
align: center
name: fig:FLIR_C5_measurement
---
Screenshot of the FLIR C5 measurement. The temperature range is set manually.
```

7. Discuss the results. *Which explanation fits best?*
8. Optionally: Tell about George-Louis Leclerc and his determination of the age of the Earth (see website).

```{figure} demo75_figure4.jpg
---
width: 50%
align: center
---
Painting of George-Louis Leclerc
```

9. Control question: *Can you determine the age of the Earth with this method?*

## Physics background
A smaller sphere has a relatively larger surface area, allowing the sphere to release energy to the surroundings (A/V = 3/r for a sphere). A smaller radius will result in faster energy loss to the surroundings. For more detailed explanation, see the website.

```{tip}
*	Project the measurement via the computer onto a screen or interactive whiteboard. Ensure that the temperature is easy to follow, for example by aiming a temperature spot from the camera at a sphere. The FLIR C5 camera can also be set to a manual temperature range, so that the room temperature is not visible (see Figure 2).
*	Place the spheres on a surface such as chocolate bars, so that the amount of thermal energy per sphere is visible afterwards by the amount of melted chocolate.
*	Place the spheres in an oven to reach a higher temperature. Be extra careful when handling the spheres then.
```

## Follow-up
Use a temperature sensor attached to the large and a slightly smaller sphere, perform the measurement in Coach 7. How meaningful is the measurement this way? How quickly do they both cool down? What influence does the temperature sensor itself have? The sensor also needs to heat up, in the case of a too small sphere, this will cost a lot of thermal energy from the sphere and therefore it will cool down even faster.

