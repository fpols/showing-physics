

# Falling spring

Author: Maarten van Woerkom     \
Time:	15 minutes  	\
Age group:	Grade 11\
Concepts:	Force, gravity, acceleration

## Introduction
Everyone is familiar with the falling motion of an object. When you drop an object, all parts of the object undergo the same movement. Then we are talking about so-called rigid objects. But what about an object where the parts can move relative to each other? A so-called large slinky, for sale in toy shops, shows an unexpected effect.

## Equipment
* A large slinky 
* ball
* camera

## Preparation
Use a large (blank) wall ensuring that the large slinky is clearly distinguishable. Put a horizontal stripe on the background near the bottom of the slinky.

## Procedure
Hold a large slinky at one end and let it hang down. Wait until it hangs still. Let the lower end coincide with the line on the wall.
Above all, do not drop the spring yet. Ask to predict what will happen to the bottom end of the spring when you release it. There will be a number of elements to the discussion. In the superstructure, the fall acceleration will be discussed. The spring will contract. If so, not each of the parts of the spring will undergo the same acceleration immediately. 
- Will that contraction happen because the top is falling faster? 
- Or because the bottom goes up first? And so on.
After discussion, offer the following four possibilities.

```{admonition} Question
After releasing the spring, the bottom of the spring will:<br>
A.	immediately fall down as well.<br>
B.	stay in place for a while (how long then?).<br>
C.	first go up a bit.<br>
D.	do something different than the above.
```

```{figure} demo96_figure4.jpg
---
width: 50%
align: center
---
Timelapse of the drop of a slinky.
```

After everyone has decided on their choice, drop the feather. Do warn that it is very fast and the result is difficult to observe.

It happens so quickly that not everyone is immediately convinced. So repeat it a few times.

Before or afterwards, make a video of the fall with the whole group. With the help of the video, you can see very convincingly what is happening.

The answer turns out to be **B**: the bottom of the spring will stay in place for a while. There are few, students, physicists, or engineers who choose B however.

## Physics background
The non-rigid body behaves very differently from the rigid body. The shape has changed: windings at the bottom of the spring have a much smaller distance between them than those at the top. The topmost winding is number 1; the next one is number 2. The second-bottom one is N-1, the bottom one is N.
As long as the distance between N-1 and N does not change, N remains in place. 
After letting go, it will take a while for this distance to decrease.
But after letting go, 1 experiences two forces downwards: its own gravity and a force of 2. So 1 will undergo a greater acceleration than $g$.

```{tip}
Make sets of about four coloured cards. For example, a green card with the letter A, an orange card with the letter B, a red card with the letter C and a white card with the letter D. If every pupil has such a set, you can visualise what a pupil chooses as an answer after the result of the discussion.
You can take another photo of the class when everyone has raised their cards.
``` 

## Follow-up
There are several opportunities for follow-ups, for instance using an acceleration meter at various points attached to the slinky. Or have you noticed how the distance between each winding changes with height? See, for instance [The suspended Slinky—A problem in static equilibrium](https://doi.org/10.1119/1.2343983) for an explanation.

## References
```{bibliography}
:filter: docname in docnames
```