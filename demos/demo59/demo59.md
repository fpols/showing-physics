

# Crookes' radiometer: Not colliding photons but:... crawling gas molecules

Author: Kirsten Staderman    \
Time:	15 - 30 minutes  	\
Age group:	from grade 11\
Concepts:	

## Introduction
A Crookes radiometer or light mill can be found in many physics cabinets. If you put it in front of the window it will start to spin, but what makes it do that? Turn this beautiful phenomenon into a research project showing how science works.

```{figure} demo59_figure1.JPG
---
width: 50%
align: center
---
A radiometer from Crookes
```

## Equipment
A light mill; 
sunlight; 
different light sources (laser pointer, torch, LED);
hairdryer; 
ice cubes; 
black cloth.

## Preparation
Lay out all supplies

## Procedure
1.	Demonstrate that the mill turns as soon as it hits the sunlight.
2.	In pairs, write what a reasoned explanation (= hypothesis) for this phenomenon is. 
3.	Collect all the ideas and then ask, again in pairs, to devise an experiment to test the hypothesis. For this, all the objects that are ready may be used. Other proposals are also fine, as long as the student can explain what makes the proposal bring you closer to an answer.
4.	Take the opportunity to stimulate learning to investigate. To do so, structure the investigation. For instance by
a.	Systematically applying PEOE (Predict-Explain , Observe-Explain) at each step, see ShowdePhysics1 p.16 .
b.	To make the difference between observation and conclusion (or interpretation) clear, you can put a table on the board with the headings: 

Experiment Observation Interpretation/Conclusion 
(= next testable hypothesis)
		
5.	In case no one suggests it: let predict what happens when you use ice cubes to cool the light mill. 
6.	If necessary, give as homework assignment to search for explanations on the internet. There are lots of different explanations there!
We came across many explanations that don't make sense. How do ideas stay alive, how do they die, or don't they die? What about science and science communication?
7.	Control question: Why is the light mill inside the glass sphere with low air pressure? Wouldn't it work much better with more air, because then more molecules collide with the fins?

## Physics background
Possible outcome of the investigation:
Experiment Observation Interpretation/conclusion 
(= next testable hypothesis)
Sunlight falls on the mill Mill starts turning (shiny side at the front, black side of the blades at the back) It has something to do with light
LED lamp shines on the mill Mill does not turn.	It is not light but heat that matters.
Using the hair dryer to warm up the light mill Mill turns quickly It could indeed be heat.
Warm up the light mill in the dark Well, how do you do that in the dark?	Light doesn't matter heat does.

Light mill with ice cubes make mill turn the other way round.	The colour (black or reflective) of the blades matters. 
Light mill completely vacuum Because this is not possible at school, tell what comes out: the mill will then no longer turn, even in sunlight.	Since the mill only turns when there is still a little gas at the blades, it could have to do with the movement of the gas molecules. 

A commonly found explanation is the following: The black-coloured side of the blade is warmer than the shiny side, so the gas molecules on the black-coloured side have a higher average speed. When these fast gas molecules collide with the dark side of the fins, the mill (on average) gets a bigger "kick" than the average "kick" of the shiny side, where the molecules move less fast, because it is less hot there. The recoil of these "kicks" pushes on the black side of the fins and is responsible for the observed rotation.
Unfortunately, this is not the latest state of science: The currently accepted explanation was formulated by Osborne Reynolds in 1879. According to this explanation, an effect akin to thermal transpiration would be the cause of the movement: On average, gas molecules move from the hot side to the cold side. Due to the tangential force of the movement of the rarefied gas moving from the warmer to the colder side, you get a the pressure difference and the cold (reflective) side moves forward.

```{tip}
It is fun to carry out this experiment when discussing Examination Problem VWO Physics 12 2006 Period I: Task 2 Solar sail with the students of vwo 6. Have students write down or explain exactly what the difference is between the two effects for rotation by light. Discuss whether you could still rotate Crookes radiometer in the other direction under certain conditions.
```

## Further investigation
The English-language explanation on Wikipedia (Crookes radiometer) shows the whole history of the different possible explanations.

## References
```{bibliography}
:filter: docname in docnames
```
https://en.wikipedia.org/wiki/Crookes_radiometer