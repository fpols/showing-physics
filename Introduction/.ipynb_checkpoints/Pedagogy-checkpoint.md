# The pedagogy of physics demonstrations

demos are almost always entertaining, but are these educating as well? 
educational value is increased with proper way of conducting them
we provide ...

## Development of inquiry skills through demonstrations
Conducting scientific research is a matter of making targeted and careful observations, using your knowledge to understand them, and vice versa, adjusting your knowledge and determining with which subsequent observations you can test and substantiate claims. Demonstrations are an excellent opportunity to practice this with students. Many important questions can be asked and addressed:
* What can I conclude from the observations? 
* How confident am I in this, can the conclusion be strengthened? 
* If I change something, what will happen? 
* And if it does or does not, what will I learn from it? 

This back-and-forth thinking between practice and theory characterizes research (Van den Berg, 2012). *In demonstrations, you can stimulate that thinking, guide it, and help improve it in a
pleasant, motivating and safe way.* As always, the trick here is to maintain the balance between challenging students, provide autonomy to think and do for themselves on the one hand, and supporting, scaffolding, encouraging and directing them on the other. What students know and can do themselves already should be done by them. But as a teacher you can take over, or help them, when students find it still too difficult. In between, choose the learning objectives for your demonstration. There should not be too many, and they should be achievable by the students with some effort. This will only succeed if you offer your students a route using targeted questions and assignments, but one in which they can also have their input and make their own contributions.

Learn physics means doing physics. But can students participate in a demonstration? After all, scientific problems are not really solved and students do not manipulate the materials... Your students may assume that with a little effort they can eventually understand the surprising phenomena - a guarantee you don't get in normal physics research. You already know in advance "what must come out," you as a teacher need only reproduce (well, only) the observations, and you can suffice with giving the corresponding explanation. You can do it that way, but it doesn't have to stop there. Your demonstration can also contribute to the development of inquiry skills, and that is fairly easy to incorporate.

To that end, as a teacher, reduce the research problems and confront students with them. Reenact the solving of those problems, as it were, during your demonstration, because there is a lot to learn from that. The easiest way to do this is to ask your students questions like:

* What do you think will happen?
* What do you know about the set up?
* What exactly did you see? What did you notice?
* How would you describe your observations as accurately as possible?
* Did happen what you expected to happen?
* If we would repeat the demonstration, what would we pay special attention to?
* How can we improve our observations?
* Can you explain what happened and why it happened?
* Did we learn something from it? How does that fit with what we already know? What do we not yet know and how can we find out?

By finding the answers together, valuing them, testing them where possible, students practice the skills they need when they "learn physics" and "do physics" on their own. The demonstrations in Part XXXX of this book focus explicitly on that, but you can work on those skills in this way in all the other demonstrations as well. Of course, you adapt them to your own needs, supplementing them with your own ideas. Furthermore, you don't ask all these questions in one demo, of course; that would be far too difficult (for students as well as for the teacher). A clear, defined goal is given some extra attention based on a few appropriate questions. Examples follow below.

You can distinguish three levels of difficulty, depending on what you want to achieve didactically. In the simplest demonstration, you show a surprising phenomenon and your students practice scientifically "observing" and "interpreting". It gets a little trickier when you not only make the connection between the observations and the interpretation but also substantiate it by describing, explaining and predicting the observations. The third type of demo involves skills associated with the limits of the knowledge found. *What assumptions have been made? In what situation is the conclusion no longer valid? Once your students have identified the limits to the found knowledge based on such questions, you can ask them if it is still valid
beyond those limits. Can the conclusion be generalized? Does the conclusion raise new questions, and how can they be answered?* In the step that then follows, you need all the skills at once and, as a teacher, you can often run out of answers. After all, in this most beautiful but most difficult type, it becomes real physics research.

## References
Van den Berg 2012