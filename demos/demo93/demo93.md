

# Chapter title


Author:     \
Time:	10 minutes  	\
Age group:	Grade 11 & 12\
Concepts:	Diffraction, interference, DNA-structure,

## Introduction
This demonstration allows you to bridge the gap between biology and physics. The demonstration optically duplicates (using a laser and a ballpoint pen spring) a photo of the X-ray diffraction pattern of DNA that has been used in unravelling DNA structure. As a pure physics demonstration, it is valuable to show the use of diffraction to unravel patterns. In addition, this diffraction pattern is more complex than that of a lattice, but still simple enough to calculate.

## Equipment
* Laser (a laser pointer in a tripod can be done, but does not work as well); 
* beam extender of at least two lenses; 
* ballpoint pen spring with fairly small pitch; 
* tripod material; 
* at least 5 metres of space to the screen and darkened room.

```{figure} demo93_figure1.jpg
---
width: 50%
align: center
---
The setup consists of a laser, a beam widener of two positive lenses and a spring. The inset below right shows the exposed spring in more detail. The screen is about 6.5 metres to the right of the feather.
```

## Preparation
Look for a ballpoint pen spring with a fairly small pitch. The spring in this example has a pitch of about 3 mm. It is advisable to prepare the whole setup in advance.

The beam widener used here consists of two positive lenses (SCHEMATISCHE WEERGAVE NODIG), slightly further apart than the sum of their focal points. Assembly is easiest without a spring. You then ensure a sharp image of the laser on the screen by adjusting the distance between the two lenses. You slide the spring into the beam and try to sharpen the image. The beam should be widened to reveal at least five windings.

## Procedure
It is possible to demonstrate and question quite a few physical concepts with students. In any case, you can link it to unravelling the structure of DNA. That immediately shows a broader and interesting application of diffraction than accurately determining wavelengths.

Other concepts that could be addressed are:
- The operation of the beam broadener. For example, state that it is two positive lenses and that the beam is widened X times and ask students to make a construction drawing;
- The creation of two crossed patterns. You can use the angle between the patterns to determine the angle between the spring windings and then check it (e.g. put the spring on the overhead projector).
- The superposition of two diffraction patterns: the lattice pattern (the 'small' pattern) and the bending pattern around a wire (the 'large' pattern). Both patterns can be computed (see Braun et al (2011)). REFERENTIE OPNEMEN
- The ratios between order of magnitude of ballpoint pen spring versus DNA and wavelength of visible light versus X-ray.
- The creation of natural scientific knowledge. Watson and Crick's use of the Wilkins and Franklin picture is controversial.

```{figure} demo93_figure2a.jpg
---
width: 50%
align: center
---
The interference pattern of a ballpoint pen spring in laser light. The pattern is a combination of a lattice pattern with a pattern of bending around a single thin wire. This image was in reality about 22 cm high. 
```

```{figure} demo93_figure4.jpg
---
width: 50%
align: center
---
Photo of the X-ray diffraction pattern of DNA. This picture, taken by Wilkins and Franklin, was used by Watson and Crick to confirm the double-helix structure of DNA. Find the similarities with the interference pattern of the ballpoint pen spring!
```

## Physics background
When radiation falls through an aperture, a diffraction pattern will be created. See further **Braun et al (2011)**.

```{tip}
Prepare this demonstration well, including in terms of didactics and scope. It takes some effort, but it is well worth it.
```

```{warning}
Apply the safety measures required when using a laser.
```

## References
```{bibliography}
:filter: docname in docnames
```