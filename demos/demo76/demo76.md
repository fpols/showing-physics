

# A Balloon that Can Do Everything: Sinking, Floating, and Hovering Balloon

Author:     \
Time:	  	10 minutes\
Age group:	14-16\
Concepts:	

## Introduction
Students learn about density in the eighth grade. Sinking and floating are phenomena they have experienced. Hovering is something they have often not consciously observed. This demonstration illustrates all three phenomena.

```{figure} demo76_figure1.JPG
---
width: 50%
align: center
---
The balloon is on its way up and hovers for a while in the warm water.
```

## Equipment
- Aquarium tank (large)
- Balloons
- Warm (approximately 35 °C) and cold water.

## Preparation
Pour warm water into the aquarium. Fill a balloon with cold water and a little air, then tie it closed. For better results, you can also cut off the nozzle.

## Procedure
You can start this demonstration with a discussion about sinking and floating. What causes something to float or sink? Then the concept of density comes up. What happens when an object has the same density as the liquid? Hovering.
So, three possibilities. Students can usually come up with these themselves. Then move on to the demonstration.
1. Explain what you are going to do (see steps 2 and 3) and have students note down what they think will happen to the balloon: sink, float, hover (or something else?).
Place the balloon filled with cold water in the tank with warm water.
2. Wait patiently until the balloon begins to hover and after a few minutes, it will eventually start floating.\
You can have a student who chose that prediction explain each of the three phenomena.
3. *Why does the balloon sink first?*
4. *When does the balloon start to hover?*
5. *Why does the balloon eventually float?*
6. Control question: *What can you say about the (average) temperature of the water inside the balloon when it hovers?* 
    1. It is higher than the temperature of the water surrounding the balloon.
    2. It is approximately equal to the temperature of the water surrounding the balloon.
    3. It is lower than the temperature of the water surrounding the balloon.

## Physics background
The balloon filled with cold water has an average density greater than that of warm water and will therefore sink. The water inside the balloon gradually heats up. The water balloon will have precisely the average density of the warm water for a while. Then the balloon hovers. If the water inside the balloon continues to increase in temperature, the average density decreases and the balloon floats.

```{tip}
*	To prolong the process, you can put some ice in the balloon or leave the balloon with water in a refrigerator or freezer for a while. (Note: the water in the balloon should not be frozen, otherwise the demonstration will not work.) This way, you can reuse the water balloon in another class.
*	 If you cut off the nozzle of the balloon, the balloon will nestle nicely on the bottom.
```