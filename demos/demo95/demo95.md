

# Gauss' rifle

Author:  Ineke Frederik\
Time:	10 minutes\
Age group:	Grade 9\
Concepts:	magnetic field, potential well, energy conservation, Lenz's law

## Introduction
A steel ball rolls off on row of bullets. The row starts with a magnetic bullet. 
The rear bullet shoots off at a much greater speed than the original bullet. A 'cannonball'? Have the laws of mechanics been defied? What about the laws of conservation? 

## Equipment
* Strong magnetic balls;
* at least 4 steel balls of approximately the same dimensions; 
* a plastic or wooden gutter

## Preparation
Place three steel balls on the gutter.

## Procedure
Three ordinary balls lie in a row in a gutter made of plastic or wood. A fourth ball rolls quietly towards it. After the collision, the last bullet in the row leaves its place at the speed of the incoming ball. Little special happens... 

Now replace the first ball from the row with a magnetic bullet identical in shape and size. Have students predict what happens when you repeat the test.

Again, a fourth ball calmly rolls towards the balls. The last ball from the row shoots away with great speed. *How is this possible?*

Do the test again. But look very carefully at the colliding balls. What do you see happening?

```{figure} demo95_figure1.jpg
---
width: 50%
align: center
---
A steel ball rolls down a row of balls; one of them is a magnetic ball
```

```{figure} demo95_figure2.jpg
---
width: 50%
align: center
---

```

```{figure} demo95_figure3.jpg
---
width: 50%
align: center
---
some caption
```

## Physics background
Ball **B** is on the ground. To bring ball **B** up the little hill on the right, you have to make some effort. Either tap ball **B** so that it gains speed and rolls up the little hill.
Ball **A** is on a little hill, on the left. If it gets a little push, it rolls down, gaining considerable speed.
Ball **A** rolls against **B**, **A** comes to a halt, **B** takes over speed and rolls up the hill on the right. Arriving at the top, he even has energy left and rolls on.
(A loses more gravitational energy than **B** needs to get out of the pit).

On a rail, there are three balls and a magnet M. That magnet attracts the blue ball firmly, because the blue ball is close to the magnet. Ball **B** is less strongly attracted, because it lies further away from the magnet.
To loosen ball **B**, you have to make some effort. Either give ball **B** a tap so that it gains speed and gets loose.
If you give ball **A** a push, it rolls towards M. It is attracted considerably, accelerates and collides with M with momentum. **A** comes to a standstill and the impulse is transmitted via the blue ball to ball **B**. That ball **B** takes over the speed and rolls away to the right. Once detached from the magnet, it still has energy left and rolls on with considerable speed.
(A loses more magnetic energy than **B** needs to get loose).

## Follow-up
Does it matter where the magnetic bullet is? Place the magnetic ball on the other side of the row and see what happens. What happens if the magnetic bullet is in the middle? Can you shoot to the moon with this 'cannon'? 
This demo is easy to expand into a practical assignment. In it, you can measure the speed before and after the collision. Is there a relationship between the speed of the bullet before the collision and the speed of the bullet after the collision? 

## References
```{bibliography}
:filter: docname in docnames
```