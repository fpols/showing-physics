

# Optics with LEDs: LEDs Provide Fun and Insight.

Author: Norbert van Veen\
Time:	10 to 15 minutes	\
Age group: 14 - 18\
Concepts:	LEDs, Optics,

## Introduction

Monochromatic LEDs are suitable for fun and educational demonstrations. They are also excellent for investigation by students themselves. The bright colors in a darkened classroom create an atmosphere that students will surely remember. 

```{figure} demo89_figure1.jpg
---
width: 50%
align: center
---
The monochromatic LEDs used.
```

```{figure} demo89_figure2.jpg
---
width: 50%
align: center
---
Spectra of the monochromatic RGB LEDs determined with a spectrometer.
```

## Equipment

Monochromatic LEDs in red, green, and blue are required for all the following components: 
- Making spectrum: spectrometer; measurement program such as Coach.
- Overlap of light beams: white screen or ceiling. 
- Showing diffraction spectra: LED TV or laptop screen.
- Scattering of light: inexpensive white plastic soccer ball.
- Calculating the strength of the lamp's lens: holder for the lens.

```{tip}
- The LED lamps used were purchased from various online stores. Look for the light intensity that the LED lamps generate and choose ones that emit a lot of light.
- The inexpensive soccer balls were purchased at Action, but you can also do the demonstration with ping pong balls.
```

## Preparation

Determine which demonstration you want to do and prepare these materials. 
1. Start Coach 7 with the spectrometer connected and choose the option Spectrometer -> emission spectrum. 
2. Test if the classroom can be darkened enough. Let the beams overlap and see if they are clearly visible. Replace the batteries if necessary. 
3. Stack the LED lamps on top of each other and simultaneously or individually aim at the TV or laptop screen and see if the diffraction spectrum is visible. 
4. Place the LED lamp under the ball and ensure the ball cannot roll away. 
5. Disassemble the LED lamp and remove the lens from the head.

## Procedure

1. Place the LED lamp at an appropriate distance from the spectrometer or ensure with the computer that the spectrometer is not overexposed. Take a spectrum of the lamps. Identify the peaks of the wavelength and discuss with the students whether they find the lamp monochromatic or not. Optionally compare with a spectrum of a white-light LED lamp. 
2. Let the beams overlap and discuss the mixed colors of light. It is also fun to create a shadow with different colors. Let students reason in what order you laid the LED lamps. See figure 3. 
3. Shine the red LED lamp on a TV in a darkened room. Discuss the diffraction spectrum you see. Ask the students to predict how the spectrum of green and blue differs from the displayed spectrum (figure 4). 

```{figure} demo89_figure3.jpg
---
width: 50%
align: center
---
Color Shadow: the LED lamps are placed next to each other, but in what order?
```

```{figure} demo89_figure4.jpg
---
width: 50%
align: center
---
Diffraction spectra of three colors on the surface of a TV screen caused by the pixel pattern of the LED TV.
```

## Physics background
a. The spectrum of the lamps is reasonably monochromatic. \
b. The mixed colors of light are well known; second-grade material. \
c. The shortest wavelength has the smallest deflection. In figure 5, it can be seen that the diffraction spectrum on the LED TV is a combination of a double and single slit. \
d. The light from the LED lamp enters the white ball and is internally diffusely scattered. The entire ball will then emit the color of the lamp. \
e. Figure 6 shows how the magnification is determined. In another situation, we found: $N = -3.0$. Distance from the lens to the paper: $v = 0.68$ cm measured. For $b$, we find: $-2.0$ cm. ($b$ is virtual). Using the lens formula: $f = 1.0$ cm or $S = 1.0·10^2$ dpt.

```{figure} demo89_figure5.jpg
---
width: 50%
align: center
---
Scattering on a ball.
```

```{figure} demo89_figure6.jpg
---
width: 50%
align: center
---
Determination of the strength of a lens. The lens is placed in a holder. This holds the lens at a fixed distance from the paper. On the paper, a measurement of 0.5 cm is marked. When looking through the lens, it is now enlarged to 1.0 cm. The magnification here is therefore -2.0.
```


NOG EEN BESTAND TOEVOEGEN