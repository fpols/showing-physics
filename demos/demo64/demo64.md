

# Chapter title


Author:  Ed van den Berg   \
Time:	  	\
Age group:	14 - 18\
Concepts:	

## Introduction

## Equipment

## Preparation

## Procedure

```{figure} demo02_figure2.JPG
---
width: 50%
align: center
---
some caption
```

## Physics background


## References
```{bibliography}
:filter: docname in docnames
```