

# Rich Boiling Phenomena

Author: Ron Vonk \
Time: 15 minutes \
Age group: 14 - 18 \
Concepts: Heating, Expansion, Boiling, Condensation, Bubbles, Water, Air, Pressure

## Introduction

Boiling might seem ordinary at first glance, but there's a lot to observe and explain physically in this seemingly mundane situation.

## Equipment

- Thick-walled Erlenmeyer flask
- 500 mL beaker or water basin
- 1 m rubber tubing
- Sealing cork or rubber stopper with hole for tubing for the Erlenmeyer
- Glass U-tube that fits the tubing
- Burner
- Tripod
- Stand with male connectors and clamps
- Slightly colored water
- Matches or lighter
- Safety goggles

## Preparation

Prepare the setup as described in the document.

## Procedure

1. Fill the 500 mL beaker with water and color it slightly.
2. Put a layer of 1 to 2 cm water in the Erlenmeyer and seal it with the stopper + tubing. Place it on the tripod and secure the whole to the stand.
3. Hang the tubing in the water of the beaker, ensuring the end is a few cm below the water surface.
4. Discuss predictions about the experiment's outcome in different stages: before boiling, during boiling, and after removing the heat source.

```{figure} demo36_figure1.png
---
width: 50%
align: center
---
The setup of the experiment.
```

## Physics background

The experiment demonstrates several physical phenomena, such as expansion of air upon heating, condensation of water vapor, and the behavior of bubbles formed by boiling water. The process of heating causes air expansion seen as large bubbles, which are then replaced by water vapor that condenses in the cooler environment of the beaker. This leads to a fascinating observation of the water boiling again after the heat source is removed due to the rapid condensation of water vapor and resultant pressure change.

## References

    Vonk, R. (2015). Dutch Journal of Physics, December 2016.
    A very complete description is available at: www.natuurkunde.nl/artikelen/3007/water-koken-bij-30-c
    YouTube video demonstration

Further investigation ideas and safety precautions such as the use of safety goggles are also suggested due to the potential for the Erlenmeyer flask to burst when in contact with cold water after heating.