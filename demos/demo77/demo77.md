

# Standing Wave with an Electric Toothbrush

Author: Karel Langendonck\
Time:	20-40 minutes\
Age group:	17 - 18\
Concepts:	Frequency, standing waves

## Introduction
When a vibration is applied to the end of a taut cord, under certain conditions, a standing wave can arise in the cord. The frequency of the vibration, the wave speed of the resulting wave in the cord, and the length of the cord must be in the correct proportions to each other. In the standard classroom experiment, you often vary the frequency. But what if you keep the frequency constant and take the wave speed and the length of the cord as variables? We use an electric toothbrush as the vibration source.

```{figure} demo77_figure1.JPG
---
width: 80%
align: center
---
The setup.
```

## Equipment
- Electric toothbrus
- Cord or elastic
- Pulley
- Mass blocks
- Tape measure
- Accurate scale
- Tripod materials

## Preparation
1.  Construct the setup as shown in Figure 1. An electric toothbrush is clamped in a tripod, and an elastic is attached to it. The other end is passed over a pulley, and one or more mass blocks are attached to it.
2. Adjust the length of the elastic to get a good standing wave.
3. Determine the linear mass density of the elastic.

## Procedure
1. Let students count the number of half wavelengths visible in the elastic and then calculate the wavelength using the length of the cord.
2. Adjust the tension in the elastic by attaching more (or fewer) weights to the end. Then test again with the length of the elastic so that a visible standing wave forms again. Have the students determine the wavelength again.
3. Repeat the previous step several times until, for example, five measurements (wavelength and mass) are available.
4. Have students draw a graph with wavelength plotted against tension in the elastic. Students will see that there is no proportional relationship.
    Question: *Which quantities should you plot against each other to get a proportional relationship?*
6. Then have students draw a graph with the square of the wavelength plotted against tension in the elastic. Now there is a proportional relationship.
7. *Calculate the linear mass density of the elastic. Or provide it.*
8. *Determine the frequency of the electric toothbrush from the slope of the obtained proportional relationship.*
9. Control question: *Sketch the (λ², F) diagram that corresponds to an elastic with a larger linear mass density.*

## Physics background

For standing waves, the following formula applies:
$v=\lambda f$

Here $v$ represents the wave speed (in m/s), $\lambda$ represents the wavelength (in m), and $f$ represents the frequency (in Hz). The wave speed in a taut cord can also be calculated using the following formula:
$v=\sqrt{\frac{F_s}{\rho_L}}$

In this formula, $v$ again represents the wave speed (in m/s), $F_s$ represents the tension in the cord (in N), and $\rho_L$ represents the linear mass density of the cord (in kg/m). The linear mass density can be determined by dividing the mass of the cord by its length. Both formulas can be equated. This results in:
$\sqrt{\frac{F_s}{\rho_L}}=\lambda f$

The variables in the experiment are the wavelength and the tension in the cord. By relating these two quantities to each other within this experiment, you can note the above relationship as follows:
$\lambda^2=\frac{F_s}{\rho_L f^2}$

By plotting the square of the wavelength against the tension in the cord in a diagram, a proportional relationship arises. The slope of the graph is equal to and from this, you can then calculate the frequency. An example of a graph is shown in Figure 2.

```{figure} demo77_figure2.jpg
---
width: 50%
align: center
---
The square of the wavelength plotted against the tension in the cord.
```

```{tip}
*	The execution of the experiment requires some skill, for which good preparation is necessary. Make sure you have a set of measurement results ready in case obtaining the standing wave in a certain situation does not go smoothly.
*	The best results of the standing wave occur when the setup is on a stable surface, such as a demonstration table.
*  Searching for a proportional relationship is a method common in physics. However, for students, this is not easy. Let them first experience that directly plotting the variables against each other does not result in a proportional relationship. This clarifies the relevance of the coordinate transformation. Let the students first think about the necessary coordinate transformation themselves.
*  It is important for the students to actively follow the experiment and process the obtained measurement results themselves. This way, a certain level of skill is developed. Processing measurement results, drawing graphs, and arriving at a set of data for a proportional relationship are then the learning objectives.
* This context has been used in a [national Dutch physics exam](https://www.examenblad.nl/system/files/2018/ex2018/VW-1023-a-18-1-o.pdf).
```

## Follow-up
Using a different elastic (with a different linear mass density) or a different model or brand of electric toothbrush can inspire further research.