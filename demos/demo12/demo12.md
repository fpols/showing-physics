

# Rotational inertia


Author:     Ed van den Berg\
Time:	  	  10 minutes\
Age group:	15 and up in physics lessons but also at 'open house' or other 'events' for all ages\
Concepts:	  acceleration, mass, inertia, rotational acceleration, rotational inertia (= moment of inertia), center of gravity, mass distribution

## Introduction
What does the $m$ mean in $\Sigma F=ma$ ? Inertia, or 'resistance to acceleration', this is the essence of the mass concept which usually receives little attention. Rotational inertia - "resistance against rotational acceleration" - is usually not included in secondary curricula, but it is very simple and can be spectacularly demonstrated with everyday objects. No time in the secondary curriculum? Ten minutes of inspiration always pay off!

```{figure} demo12_figure1.jpg
---
width: 50%
align: center
---
SOME Caption
```

```{figure} demo12_figure2.jpg
---
width: 50%
align: center
---
SOME Caption
```

```{figure} demo12_figure3.jpg
---
width: 50%
align: center
---
SOME Caption
```
## Equipment
* A glass of water on a dry sheet of A4 paper; a ballpoint or pencil
* a slightly longer stick or rod
* a hammer
* a meterstick or a rod
* a broom
* a broomstick or PVC pipe
* other items of your choice
* a full roll of toilet paper and one that is almost finished

## Preparation
Prepare the objects needed. A few powerpoint slides might help. Make sure the bottom of the glass of water is dry and so it does not stick to the A4 sheet as a glass of tap water collects condense easily.

## Procedure
Linear inertia, elicit prerequisite knowledge: Put a glass of water on a dry sheet of A4. Pull the paper with the glass of water slowly at a constant speed to the edge of the table, the glass just goes along. Then give a sudden jerk to the paper. The paper is off the table, the glass stays behind on the table. Explanation: at a constant speed the glass goes along nicely. When accelerated the glass (mass) resists acceleration, it stays behind, we call that inertia. The glass of water resists linear acceleration. In formula language we know $\Sigma F=ma$; in it $m$ the mass of glass plus water, is the inertia. With a given constant force, heavier objects experience a smaller acceleration than lighter ones. Heavier objects have more “resistance” against acceleration.
Now take a ballpoint pen or pencil, put it upright on your hand and try to balance it on your hand (upright). Let pupils participate with their pencil or pen. It does not work. The pen immediately falls and we are not fast enough to correct the rotational movement. Take a ruler or meterstick. That is much easier. When it starts to fall, we can always correct in time before the stick really falls over. Take a longer stick or broomstick and the balancing becomes much easier. You can easily keep balancing a broomstick for an hour or more! You could almost keep a book in the other hand and read while balancing. Explanation: analog to the translation movement, there is resistance to rotational acceleration. The longer the stick, the slower the stick begins its rotation. We call this resistance to an angular acceleration: rotational inertia. The demonstration shows that longer sticks have a greater rotation inertia.
Now take a hammer or a stick with a weight on top. Put the handle on the finger and the iron on top and balance. That is relatively easy. Turn the hammer over and try to balance it with the iron on the finger. That turns out to be a lot more difficult. Again, the longer the stick or handle, the easier it is to balance. And the farther the weight is from the pivot point (from the finger), the slower rotation sets in. The rotational inertia increases with the mass of the object and the distance from the center of gravity to the supporting pivot point.
Story and test: In the circus they do this kind of experiment with a long stick and a girl on top. Suppose we have a stick with the girl, and another, equally long stick with the mother of the girl, for whom is rotation easier to prevent, for the girl or the mother? Why? 
  
5.	Application in daily life: in the toilet you can easily tear off a piece of toilet paper from a new roll with one hand with a jerk. With a roll that is almost finished, everything will come off if you try. Put both rolls on a stick and demonstrate!

Next time: take rolling objects such as empty cans, cans filled with sand, cans filled with water, or even cans of soft drinks, shaken or not. Let them roll from a sloping surface (table) and see what happens. Which has the greatest rotational inertia?

```{tip}
Increase attention and entertainment, for example by pulling the glass in (1) close to the edge before the paper is pulled away. When balancing a long stick: see how long I can keep this up, who knows you're still here at 6 pm.
```

```{warning}
No safety problems, but do not let the broom hit someone's head when balancing.
```

## Physics background
The parallel between linear movements and rotations is interesting. The linear inertia is the mass m. The rotational inertia is called the moment of inertia in physics and is indicated by the letter I. For an object with a mass $m$ that turns around a fixed point at distance $r$ is: $I=mr2^$
Then compare the following situations:
Linear movement:     $F = ma$	(1)
The sum of all forces on an object is equal to the product mass $m$ * acceleration $a$.
Rotational movement:  Στ = Iα  	(2)
The sum of all torques that cause an angular acceleration α is equal to the product of rotational inertia I * angular acceleration α.

See a university level mechanics text for the restrictions to equations (1) and (2).

## Follow-up
Put the table higher on one end and then let different objects roll off the slanting surface. For example, cola cans shaken or not, empty tin, tin filled with sand, and so on. Which comes first, which last? Relating to rotation support. More mass at the edge; than greater moment of inertia, so slower. With liquids a can can rotate without the liquid rotating, but with first shaking and therefore turbulence in the can that process is disturbed.

## References
```{bibliography}
:filter: docname in docnames
```