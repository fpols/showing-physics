# About this book

This book originated in the collaboration between physics teachers and physics teacher trainers, united in the Dutch Society for Science Teachers [(NVON)](https://nvon.nl). The three Dutch volumes of Show*de*Fysica cover over a 200 demonstrations for secondary school physics, along with additional text on the pedagogy of physics demonstrations. Each of these demonstrations are tested by various secondary school physics teachers and their technicians ensuring that these demonstrations have value in their description and are accessible by teacher. 

```{figure} Figures/metalballs.jpeg
---
name: fig:metalballs
width: 60%
---
The cooling of [two heated metal balls](../demos/demo75/demo75.md). Which one cools down fastest, the big or the small ball?
```

Are our demonstrations original? Don't expect them to be so. Most demonstrations were inspired by earlier versions of the experiments - where possible referenced to earlier descriptions - and edited by the [Authors](Authors) to fit the didactic categories of this book. They have been widely tested in Dutch physics education by large group of physics teachers and technicians who were willing to cooperate. You find a list of names of those who took the pictures, drew figures, tested the materials or helped in writing this version of the book. 

```{tip}
Your name can be amongst the list of contributors. If you have suggestions, you can open an issue using the git button at the top right of this page.
```

This book is published under the CC-BY-NC, what means that you are allowed to use and adapt the materials for non-comercial use and under the condition that you refer to the original work. 

## How to use the book
Demonstrations are fantastic. They offer so many wonderful possibilities that we really should do at least one in every physics class. That may not always work out, but the objection "which demo then?" no longer applies. After all, you now have this book!

You can read this book, of course, but the main idea is to do the activities described yourself in your classroom and adapt them to your own needs. Therefore, in choosing and developing the demonstrations, we always had in mind a teacher who wants to realize in his or her class activities from which something valuable is learned.

```{figure} Figures/soapbells.jpg
---
name: fig:soapbells
width: 60%
---
Why do soapbells behave strangely in the surrounding of a [vanderGraaf](../demos/demo10/demo10.md) generator?
```

Specifying exactly which student audience the demonstration is intended for proved to be difficult. If it is a matter of observing phenomena and the beginning of the development of vocabulary, many demonstrations can be used in elementary schools. In lower secondary schools, the demonstrations can usually be used for qualitative explanations at the phenomenon level. As soon as chains of concepts are needed for an explanation you end up in higher grades of secondary education. If you need to establish relationships between different areas of physics or if quantitative reasoning is necessary, the level will soon be upper secondary school.

```{Warning}
We have carefully conceived, developed, described and tested each of the experiments. Moreover, we specified safety issues where necessary. However, we are not responsible for whatever accidents, or injuries when doing a demonstration described in this book. We want to emphasize as well the requirement to test a demonstration before doing it with public. 
```