

# Writing with a Laser using Phosphorescence

Author:     Norbert van Veen\
Time:	  	10 minutes\
Age group:	14 - 18\
Concepts:	Phosphorescence, laser, photon energy, excitation, emission

## Introduction
Phosphorescence (luminescence) is a familiar phenomenon for many students. They recognize this from various "Glow in the Dark" stickers and objects. What they do not know is that a threshold energy is needed to make phosphorescence possible. We do this experiment using several lasers, each with different wavelengths. The intensity (or output power) of the laser makes no difference whether or not the surface will illuminate. We show this by using a He-Ne laser and by spreading the beam using a concave lens.

```{figure} demo24_figure1.JPG
---
width: 50%
align: center
---
some caption
``` 

## Equipment
* ‘Glow in the Dark’ surface
* 3 laser pointers with different wavelength (red, green and violet)
* He-Ne Laser
* A darkened classroom.

## Preparation
Paint a wooden board (any size) a few times with "glow in the dark" paint. Make sure to keep the board in a dark bag until the room has been darkened. Place the board in front of the classroom and make sure you start with the red, then green and finally the violet laser.

```{tip}
* Provide for a darkened room so that the effect is stronger.
* There are many sources for “glow-in-the-dark” paint, see internet.
```

```{figure} demo24_figure2.jpg
---
width: 50%
align: center
---
some caption
``` 


## Procedure
1.	*Today we will try a new kind of pen and school board.* With the violet laser draw some lines on the white wall. As soon as the laser is off, nothing visible remains. *Apparently, our pen does not work well on the wall.*
2.	*Now write with a laser on the upper left-hand corner of the special board.* Just choose a word that is interesting for the class. Now the letters still light after the laser is off. *That is interesting, writing with a laser!*
3.	*I have also a green and a red laser and a Helium-Neon laser with high intensity red light. Can we also write with those on this board? Why or why not?* Class discussion, or let students predict individually and discuss predictions and explanations with their seat mates.
4.	*Let’s try.* Write on the board with the green and then with red laser. No letters after the lasers are off.
5.	*And if we use a much higher intensity?* Does not work.
6.	The teacher summarizes the results in a table on the board.
7.	*How can we explain these results?* The expected answer is that “something” is excited by the laser light that then decays while emitting light. That “something” could be atoms of a special paint.
8.	The teacher presents one-by-one the four energy schemes of figure 3 and asks: could this be the proper energy diagram? Which property fits and which one does not? The alternative is that students in duo’s or trio’s discuss diagrams A, B, C, and D and consider to what extent these diagrams can or cannot explain the phenomena.

```{figure} demo24_figure3.jpg
---
width: 50%
align: center
---
some caption
``` 

```{admonition} Possible explanations of emission with the energy schemes
In energy scheme A emission should have the same color as the laser light because the distance between energy levels is the same for excitation and emission (levels 1 and 3). 
In energy scheme B the emission energy is indeed lower (from 3 to 2, green and not blue) but one would also expect emission of violet (directly from 3 to 1). Also, one might expect to cause also excitation with a green laser (directly from 2 to 3) which does not seem to happen.
Option C seems more likely (level 2 closer to level 3) but also here one would expect emission of both violet (from 3 to 1) and green light (from 2 to 1), and why is there no excitation from 1 to 2 directly with a green laser?
```
9. After this exercise with energy levels students will not yet come to a solution. Something new is needed. The teacher now tells that usually emission occurs within 10-8 seconds so there should not be any “after glow”. One can see that by shining the violet laser on various objects in the classroom. However, there are special materials such as “glow-in-the-dark” paint which exhibit “after glow”. In these materials there are energy levels which can exist much longer because the transition to a lower level or the ground state (for example level 2 to 1 in example D of figure 3) is forbidden by certain selection rules. Such levels cannot be reached by direct excitation (from 1 to 2) but only via a higher level (3). Then decay (from 2 to 1) will be very slow and so we will see “after glow”. This phenomenon is called phosphorescence. 

## Physics background
Phosphorescence happens when a substance (in our case ZnS doped with Cu) is excited by a light source emitting photons with the right minimum energy, The substance then emits light, which slowly fades out. In the case of the ‘glow in the dark’ paint, the copper ensures that extra metastable energy states exist. These provide an energy difference in order of green light (+/- 500 nm) and for the gradual emission of this light. The afterglow can last for some time.

## Follow-up
The emission of the greenish light is temperature dependent (Randall & Wilkins, 1945), so increasing the temperature will shorten the afterglow. The surface can be cooled or heated to examine this effect.

## References
```{bibliography}
:filter: docname in docnames
```