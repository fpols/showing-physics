

# Mysterious fountain
Author: Peter Dekkers\
Time:	10-15 minutes\
Age group:	12\
Concepts:	Siphon, gravitational energy, conservation of energy

```{figure} demo91_figure1.jpg
---
width: 50%
align: center
---
The mysterious fountain.
```

## Introduction
In this 'black box' demonstration, the 'tap' continues to flow once you pour some liquid into the funnel and then keeps flowing for a long time. The challenge is to think together about what might be inside the bottles. Students practice developing, comparing, and testing explanations for phenomena and reflect on the role of creativity and personal input in scientific research.

## Equipment
* Two bottles or Erlenmeyer flasks; 
* two tightly fitting rubber stoppers, each with two holes that the glass tubes fit exactly; 
* two large envelopes, large enough to completely cover the bottles; 
* a small funnel; 
* glass tubes or rubber hoses.

The delicate tube that acts as the 'tap' can also be made of rubber. Use a transparent hose to connect the two bottles. Preferably use water with different colors to contribute to the mystery. Also needed: a cup with some extra liquid.

## Preparation
Construct the setup according to the sketch. Place the bottles in the large envelopes, which cover the bottles up to the stoppers, so that the contents of the bottles remain invisible.

## Procedure
Pour the liquid from the cup into the funnel. Then, liquid will rise in the tap tube on the right: as soon as it 'rounds the corner,' the tap starts to run and keeps running (as long as the liquid flows into the funnel). Yet, 'nothing' (visible) goes through the connecting hose between the bottles.

The discussion during the experiment is more interesting than the execution and the events. Obvious questions and tasks (see a possible 'scenario' in the {ref}`worksheet<worksheet>`):
* How do you think it works? (Draw it and explain, compare with your neighbor.)
* Will the tap run forever? Why or why not?
* Nothing goes through the connecting tube. Will it still work if we remove it?
* What do you think will happen if... (see the {ref}`worksheet<worksheet>` for possibilities)
With dyes, the experiment becomes more visible and more interesting. After all, could the explanation involve chemical processes?


```{figure} demo91_figure2.jpg
---
width: 50%
align: center
---
Sketch of the setup
```

## Physics background
Explanation for lower grades (use figure 2). When you pour a little water into the funnel, the level in the left bottle rises. Air is pushed through the hose to the right bottle. The water there is pushed down and thus comes up in the tube on the right. The tube of the 'tap' is narrow, and soon the water rounds the bend and falls into the funnel. After that, the water flowing from the tap ensures the process continues. The water stops running when the level difference in the bottles is the same, or when the right bottle is empty.

More precisely: once the column of liquid under the funnel is built up, and the flow is stable, the gas pressure above the liquids inside both bottles is the same. The gas pressure above the liquid in the funnel is also always equal to the atmospheric pressure prevailing at the end of the tap. Thus, the water continues to flow from the tap as long as the liquid column on the left is higher than the one on the right. According to Bernoulli, the potential energy of the excess water in the left column is then converted into the kinetic energy of the water flowing from the tap. It is a siphon effect: the water keeps flowing until there is no more excess potential energy available, either when the right bottle is empty or the liquid columns are of equal height.

## Follow-up
{cite:t}`lederman1998avoiding` describes how various characteristics of scientific research can be clarified to students based on their own experiences.

```{tip}
If it does not 'work,' check if the stoppers are securely fastened and the long tubes reach the liquid level in both bottles. Afterward, you can pull out the stoppers from the bottles, swap them, and start over. Be careful of overflow as some liquid has been added.
```

## References
```{bibliography}
:filter: docname in docnames
```

```{admonition} Worksheet
:name: worksheet

**Overview**

This section presents a scenario, which is a possible progression of the experiment along with examples of questions, comments, and explanations from the teacher aimed at stimulating the exchange of ideas and insights among students.

---

**Discussion**

**Teacher:** "I have a cup here with a strange liquid. There's a setup here, and I'm going to pour a bit of this liquid into this funnel. What do you think will happen?"

*Allow students to respond.*

**Teacher:** "Shall we give it a try?"

*Pour liquid into the funnel until the tap starts to flow.*

**Teacher:** "As you can see, the tap begins to flow. What's going on here?"

**Teacher:** "Do you think it will stop if I hold my finger against the end? And if I let go, does it start again or not?"

*(The tap stops when you hold it closed with your finger but starts again when you let go.)*

**Teacher:** "And if I catch the liquid? Do you think it will start again if I pour the liquid back in?"

*(When you catch the liquid, the tap stops. Pour the caught liquid back into the funnel, and the tap starts again unless the bottle is empty.)*

**Teacher:** "Does it make a difference if I hold one bottle lower or the other higher?"

*(The height difference between the bottles does not affect the flow. The difference in liquid levels on the left must be greater than the difference on the right; the greater this difference, the faster the water flows from the tap. The right bottle must be higher than the left to make this difference in height difference possible.)*

**Teacher:** "Will this tap run forever? When does it stop, where does all that liquid come from, and where does it go?"

*(The flow stops when the right bottle is empty, or the difference in height difference is zero.)*

**Teacher:** "What's the purpose of that connecting tube? It looks like nothing is going through. Does it still work if we disconnect it?"

*(No, then it stops. If you reconnect it and add some water to the funnel, it starts again.)*

**Teacher:** "What do the colors of the liquids have to do with anything?"

*(Nothing.)*

***

**Assignments**

**Students:** (in groups) Answer the following questions and present them to each other:

- What do you think is in the bottles?
- Draw your answer in the figure.
- Explain how it works.

*Possible extension: try to make such a device at home / with your group.*

<hr size="3" color ="black">

**Comparison of Answers**

*Everyone naturally thinks their answers are the best. But can you also say why they are the best? If you only look at the solutions from other groups, which do you think are the best? Why?*

*(After the bottles are 'revealed':)* 

**Teacher:** "Do you understand how it works now? Explain it. Which solution(s) do you now think are the best?"

---

**Conclusions: Characteristics of Scientific Research**

Does this experiment have something to do with science?

*Most people are surprised when the tap starts running and continues for a while. It seems there is nothing that keeps the water running. In science, surprise is very important. If you didn't expect it, you can learn something new, and that's what scientists find very important. To learn new things, they conduct research. One way to conduct research is to think: what could be in the bottles, and how does it work? Then, various solutions are thought of.*

*Most scientists naturally think their own solutions and those of their friends are the best. To find out which solution is really the best, you have to do more research. You investigate whether a solution matches the setup, or you think carefully: is that solution understandable, or is it fantasy? You can also rebuild the solution and see if it works. Scientists have various ways to find the best solutions.*

*Of course, there is only one solution that is completely correct for this setup. You see it when you take off the envelopes.*

*(If you haven't removed the envelopes yet, you're setting the bar very high! Show what's in the bottles and project the figure if possible.)*

---

**Explanation**

*Lower Secondary*

The color of the liquids has nothing to do with it; it was only meant to be interesting. It's just colored water. If you pour a little water into the funnel, the level in the left bottle rises, pushing air through the tube to the right bottle, which also causes the water there to rise. The tube of the 'tap' is narrow

, so soon the water rises, goes around the bend, and falls into the funnel. Then you don't need to pour anymore, the water coming from the tap keeps the process going, usually until the right bottle is empty. The water stops running when the level difference in the bottles is the same, or the right bottle is empty.

*Upper Secondary*

If you want to go a step further than the examination program, consider: the liquid is incompressible, the flow is frictionless, and the temperature is constant. Then the energy of a piece of liquid can change in three ways:

1. If the speed changes, the kinetic energy changes,
2. If the height changes, the potential energy changes,
3. If the pressure changes, work is performed.

The law of conservation of energy can be written as: $0 = ΔEkin + ΔEpot + W$

Thus, for each volume element at every point in the circuit: $½ρv^2 + ρgh + P = constant$

($ρ$ is the density of the liquid, $v$ is the speed at that point, $h$ is the height, $P$ is the pressure.)

This is Bernoulli's law (which also explains flying when applied to a gas).

The pressure at the inflow and outflow points is atmospheric pressure. Apply Bernoulli at those points (and use also that the pressure in the air within the bottles is equal everywhere) then it turns out: the difference in height of the liquid columns on the left and right results in the speed of the outgoing liquid.
```