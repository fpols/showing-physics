

# Air or not, reasoning about flames and vapor


Author:     \
Time:	  	  5-10 minutes\
Age group:	12 - 14\
Concepts:	  phases of water, evaporation, condensation, combustion, air, oxygen

## Introduction
Just above the surface of boiling water, nothing noteworthy seems to be happening or visible. Slightly higher, however, a kind of rising fog is seen. An object placed in that fog gets wet: the fog consists of water droplets. But then what fills the space between the bubbling water and the fog? Is that ‘just hot air’, or not? How could you investigate that?

```{figure} demo09_figure1.jpg
---
width: 50%
align: center
---
The required materials
```


## Equipment
* Electric boiler
* water
* tea light with container (for example a spaghetti spoon)
* matches (see figure 1)
* webcam and screen to make a candle visible in the holder for the whole class.

## Preparation
•	Put some water in the kettle and let it come to a boil.
•	Leave the lid open so that the kettle does not turn itself off.
•	Put the candle in the holder and light it.

## Procedure
Discuss the questions in the introduction with the students. Between the bubbling liquid in the kettle and the rising fog above it, you see 'nothing'. What is actually there? Experience shows that most students say '(hot) air'. That idea can be tested, because if it is ordinary air, like the air we breathe, there must be oxygen in it. So if we hold a burning candle in the kettle, it would just keep burning if that space contains normal air.
 
Bring the burning candle down into the kettle, and note that it goes out. Summarize the observations: 
* the candle goes out: above the water is a substance that looks like air but it is not;
* bubbles come out of the water;
* water droplets float above the kettle.

 ```{figure} demo09_figure2.jpg
---
width: 50%
align: center
---
Above the kettle, the flame burns somewhat weaker
```

Draw a conclusion together. It may well be that the conclusion is not: that there is water vapor above the liquid. That is not proven. What we can agree on is: 

*	what comes out of the bubbles and hangs above the liquid looks like air;
*	it is not air, because a candle does not keep burning in it;
*	this substance turns into water droplets when it rises higher.
 
The substance concerned has a name, "steam". Often the fog is said to be made of steam, but that is not scientifically correct. You could conclude the demonstration together through establishing a proper name for the droplets hanging above the kettle. ‘Cloud’, ‘mist’ and ‘fog’ are acceptable names, but your students may think of more imaginative ones. 

```{figure} demo09_figure3.jpg
---
width: 50%
align: center
---
Inside the kettle, above the water, the flame extinguishes
```

```{warning}
Working with boiling water has risks. Do not let students come too close; if possible, use a camera and projection to give everyone a good view.
```

## Physics background
The air that is above the liquid before it boils is quickly pushed away when the liquid starts to boil. The temperature inside the kettle remains at 100 $^o$C. The water vapor only condenses above the kettle, where temperature is lower. Due to lack of oxygen, the flame goes out (some knowledge about burning is desired prior knowledge).

