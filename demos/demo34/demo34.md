

# Playing with Density

Author:     Oda Warringa \
Time:	  	10 - 15 minutes\
Age group:	13\
Concepts:	density, floating, sinking, suspension

## Introduction
This demonstration can be performed as an introduction to or as practice with the concept of density. Students will observe that liquids can float on other liquids and that the mass/quantity of the liquid does not play a role. Students will also see that liquids do not automatically mix (when poured slowly). The materials used are familiar to students. Additionally, there is a good color contrast, so even students at the back of the class can see it well.

## Equipment
* Transparent sealable bottle of approximately 1 L with a large opening
* 250 g honey
* 250 mL sunflower oil
* 250 mL water; a cork
* A cherry tomato
* A coin
* A plastic bottle cap. **Note! Not every cap is suitable. The density should lie between that of water and sunflower oil. In the example (Figure 3), a green cap from an AA bottle was used.**
* Possibly for supplement (see tip): three small measuring cylinders and a scale.

## Preparation
Ensure that all materials are within reach. Fill three small measuring cylinders with the three liquids (see tip).

## Procedure
Below are step-by-step instructions with tasks and questions that can be adjusted according to your own insights.

1.	Ensure the large bottle is empty and clean.

2.	Pour about 250 g honey into it. 
    * Task: *Write down what will happen when sunflower oil is poured into the oil? Also, write down why this happens.* (about 1-2 minutes). Then quickly go through a few answers.

3.	Pour about 250 mL sunflower oil into the bottle.
    * Question: *What would happen if I were to pour water into it now? And why?*

4.	Now pour about 250 mL of water into the bottle.
    * Question: *Why does the water go under the sunflower oil? (or why doesn't it stay on top?)*
    * Question: *Is it the mass that is greater? Or is it something else?*

5.	Hold a coin above the bottle and drop it after the first question. 
    * Question: *If I drop this, where will the coin stop?*
    * Question: *Why does the coin sink to the bottom?*
    * Question: *What happens if I put a cork in the bottle? Where does it end up?*

6.	Put a cork in the bottle.
    * Question: *What happens if I put a tomato in the bottle? And why? Consider what you already know about a tomato.*

7.	Put a cherry tomato in the bottle.

8.	Put a plastic bottle cap in the bottle, where does it go?

9.	Close the bottle.

10.	Turn the bottle slowly, the liquids and objects rearrange (Figure 2).



```{figure} demo34_figure1.jpg
---
width: 50%
align: center
---
An example of the demo
```

## Physics background
Due to the difference in density, the substances will arrange themselves in the bottle (Figure 3) as schematically indicated in Figure 4.

After just a few minutes, it is visible that the water changes color. This is because substances (sugars) from the honey slowly dissolve in the water. This increases the density of this layer and the tomato will rise a layer. Solubility is a concept that students probably do not yet know well because it is often taught after the concept of density.

```{tip}
* Measure equal (small) amounts of honey, water, and oil in three measuring cylinders and weigh them. Write down the result on the board and refer to it in the explanation. Can this demonstration be made even more spectacular? Yes, it can! See below for the sources.

* Leave the bottle for a few hours and then show it to the students again. If all goes well, some of the substances from the honey will have dissolved in the water by then, and the tomato will have floated on this 'sugar water' layer.
```
## Follow-up
It is possible to make sugar solutions in water with different densities. When using cold water (from the refrigerator), you can neatly layer them on top of each other. This offers many possibilities for experimenting with density.

## References
```{bibliography}
:filter: docname in docnames
```