

# Falling faster than g: free fall with rotation


Author:     \
Time:	  	  5 minutes\
Age group:	15 and up\
Concepts:	  acceleration of gravity g (=9.81 m/s2), free fall, rotation

This description was inspired by {cite:t}`ehrlich1990`.

## Introduction
Everything that falls freely should experience an acceleration $g$? Or not? Are there exceptions? 

```{figure} demo11_figure1.jpg
---
width: 350%
align: center
---
Are some of the dices falling faster than $g$?
```

## Equipment
* Meter stick or equivalent
* About 30 coins (teacher can borrow from students, they could be pennies or pounds, whatever)
* If possible a high-speed camera. Most phones can do 120 or 240 frames per second. Cheap action cameras can do 120 frames/second as well

## Preparation
Think how the visibility of observations can be maximized. For example, put a chair on top of a table and use that as one of the endpoints of the meter stick. Distribute the 20 coins more or less equally across the length of the stick. 

## Procedure
1.	Can I accelerate my hand faster than g? How can we investigate that?  (Coin or stone on the hand, then pull down the hand as quick as you can). 
2.	What is free fall? Can objects in free fall be accelerated faster than g? 
3.	The teacher holds the meter stick and the 1-meter end while the other side rests on the chair on top of the table (figure 1). Suppose I let the end of the stick go, what is then the acceleration of the end? What is the acceleration of other points of the meter stick? (All points of the meter stick have the same angular acceleration, but must have a different linear acceleration as they cover different vertical distances in the same time.) 
4.	How can we investigate this with our set-up? 
5.	Let’s first keep the meterstick horizontal and drop it. The teacher now holds both ends. Would the coins keep their contact with the stick? Execute! If there is a student with an iPhone, then film with 120 or 240 frames/second. 
6.	Now put the meter stick with one end on the chair on the table while the teacher holds the other end. Mark the 67 cm point but do not yet tell students why. The teacher: let’s go, hopefully a student can film this.
7.	It would be good to repeat and now ask students to specifically pay attention to the end of the stick. 
8.	Then follows a discussion about the linear acceleration of different points of the meter stick. This cannot be the same in rotational movement. If there are points which accelerate at less than g, shouldn’t there be points that accelerate at more than g? Did we see that? 
9.	From our observations, which points accelerate with g or less? Which points with more than g? The separation is at 2/3 of the stick, so at 67 cm.
10.	Rotation physics is often not in the curriculum. So we will not derive theoretically that the point with acceleration g is at 2/3th of the meter stick. But we do ask our students for examples of similar phenomena. For example, a ladder which is put up too steep and turns over, a factory chimney which is blown up and falls over, a swimmer who stands on the diving board and lets himself fall over without jumping, etc. 

```{tip}
Using the coins as indicator one can investigate shorter and longer sticks. You can move the center of mass of the stick by taping on pieces of wood in different positions. You can also conduct video measurement using films from the iPhone or a high-speed camera. Students could do projects and could then study rotational physics first, for example from well known physics texts such as {cite}`young2014`.
```

## Physics background
The moment of force on a meterstick with length $L$ is $\frac{mgL}{2}$. Dividing this by the rotational inertia $(1/3)mL^2$ we obtain $\frac{3g}{2L}$ for the angular acceleration (analog to $\frac{ma}{m}$ for linear movement). 
The linear acceleration at a point on the stick is the product of (distance from point to the rotation) * angular acceleration. This will be greater than $g$ for points farther than $\frac{2L}{3}$.


## References
```{bibliography}
:filter: docname in docnames
```