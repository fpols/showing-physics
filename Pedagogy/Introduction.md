# The pedagogy of physics demonstrations

Physics demonstrations are fascinating, inspiring, and educational—they truly deserve to be spotlighted in our classrooms. However, they also require a significant investment of time. Gathering and coordinating materials, setting up components in such a way that the focus is on the key aspects, preparing the presentation—these are just the beginnings. Engaging students and involving them deeply, sparking curiosity, building suspense, to ultimately unveile the phenomenon. All this effort aims to ensure that your audience will never forget your demonstration! And, finally, cleaning up... Should you even bother? **Absolutely!!!** But when you do it, do it right! Make it memorable and impactful, turning a simple lesson into an unforgettable learning experience.


```{figure} ../demos/demo12/demo12_figure2.jpg
---
width: 50%
align: center
---
A teacher demonstrating [stability](../demos/demo12/demo12.md)
```

## What can you accomplish with demonstrations?
**Do we learn from demonstrations?**
Almost everyone who is enthusiastic about STEM education remembers a great demonstration. One that sticks in the memory. One that you would like to share because you enjoyed it so much yourself. But did you learn anything from it? Are physics concepts better anchored? In an article in the *American Journal of Physics* ( {cite:t}`crouch2004classroom`):

> Despite popular beliefs to the contrary, students learn little, if anything, from traditionally presented classroom demonstrations.

So... should we leave out those demonstrations? Then at least you save time... a lot of time. {cite:t}`crouch2004classroom` fortunately also reports that you can significantly increase the learning effect of demonstrations. To do so, you have to modify the demonstration in such a way that your students [predict the result](PoE). Students who predict the result of the demonstration before they see that result, learn much more. They also show significantly more insight and understanding, which in the study was reflected in a better test result. Whether you learn anything from a demonstration is then much less in question.

Without any strategy to the didactical approach, demonstrations are not likely to increase students' comprehension of the phenomenon at hand. Hence, this book not only presents many physics demonstrations, it also delves in the strategies that can be applied when conducting a physics demonstration. Before elaborating on specific strategies, we cover some basic ideas. Moreover, pertaining to each of the four categories in this book, a section is devoted to the pedagogy that fits that specific purpose.

``` {figure} Figures/singingrod.jpg
---
width: 40%
align: center
---
A teacher demonstrating the [singing rod](../demos/demo81/demo81.md) to teach students about standing waves.
```

So what exactly can you accomplish with demonstrations? The overall learning goals can be categorized as follows (after {cite:t}`hodson1993re`):

**Learning physics**

*"Students learn to better understand theory by relating it to the world around them,"* we often hear. *"The students learn to better understand the world around them by linking it to theory"* we also consider very important. This is "learning physics". Our [Demonstrations for conceptual development](../demos/Conceptdemos1.md) cover this learning objective. 

Most of our demonstrations  relate to conceptual development. These demonstrations have been chosen for their suitability to elucidate a physical phenomen or concept. They belong in the regular instruction lessons. The descriptions often include step-by-step suggestions for didactic implementation, including opportunities to get students thinking for themselves as much as possible. They stimulate physical thinking and help students build a physical concept network.

**Learning about physics**

When teaching students how to investigate the world around them for themselves, you teach them to ask scientific questions like
1. What does *"It is scientifically proven"* mean? 
2. What makes it so valuable? 
3. What do you have to do to prove something scientifically, and how sure are you then of your case?

and to answer them based on observations, with whatever it takes to get that done. During this process, the students get an idea what it is to be a scientist and how science work. In other words, they learn to *"do physics"*. Our [demonstrations on teaching scientific inquiry](../demos/Inquirydemos.md) and [nature of science](../demos/NOSdemos.md) cover this learning objective.

The demonstrations described here revolve around the skills that are indispensable in scientific research on the one hand and about the characteristics of doing science on the other. These skills include carefully observing, critically analyzing a problem, developing a model to explain observations, or testing expectations with an experiment ({cite:t}`pols2022defining`). The knowledge created in the process is man-made. That knowledge is as certain as knowledge ever gets and yet will always be open to improvement. 

**Affective goal**

Demonstrations motivate students, evoke pleasure in physics, arouse interest in the subject, develop fascination with the phenomena. They contribute to an understanding of the motivations of physicists and an appreciation of their achievements. This is called "learning about physics". [Demonstrations for special occassions](../demos/Specialdemos.md), well, needs no further explanation.

Demonstrations also play a role at special occasions: at an open day, at an anniversary, at a farewell. In this book you  will find examples of demonstrations that stimulate thought, but which also have a surprising or playful twist, making them suitable to show at such special occasions. The topics are by no means always part of the school physics curriculum. However. they all appeal to common sense and the outcome is often counterintuitive. The charm of physics is shown to its full advantage in this volume!

## Different goals demand different methods
Many of our demonstrations can be easily adapted so they are used for a different purpose. Where you can tell students what is happening and why it happens in a certain way when focussing on conceptual developement, you can ask students as well to think of new experiments to come up with their own explanation. This approach better fits with the idea of teaching them how to set up a scientific investigation. It needs little explanation that each of these goals require different teaching strategies and, hence, diserve their own chapter in this book.

We have found the following questions of value: 
1. What is happening here? Do I understand this, or is it different than I thought?
2. What does it mean what is happening here? When we say that physically it is really like this, what exactly do we mean?
3. Is this really true? Can I convince myself and others that it really is?

In physics research, all of these questions are important but in demonstrations not always. And certainly not all of them at the same time. Demonstrations play a role in a learning process and so there you have to offer questions in steps and gradually shift the emphasis over the course of the learning process to more complex questions. That gives the viewer an opportunity for processing and of developing skills that ultimately add up to an inquisitive attitude.

Questions like the first question encourage [back-and-forth thinking between theory and practice](../Pedagogy/BackAndForthThinking.md). This includes demonstrations in which something happens that you don't expect and that raises questions. With this you can illustrate how in physical research you are constantly testing theory and practice against each other. Students experience this with the support of the teacher in the demonstrations. 

Questions like question two focus on [argumentation](../Pedagogy/Argumentation.md). Students seem satisfied with any answer, whereas only the most *convincing* answer possible is good enough.
But what do we find convincing, and why? Demonstrations can develop scientific understanding of this in students. 

Questions such as question three deal with the [nature of science](../Pedagogy/Nos.md). In successful research the researcher is looking for something that was not already known. 
But what exactly does it mean when that researcher says, "I have scientifically established this". Is it then definitely and definitely true? Does another researcher then always and everywhere come to the same conclusion? Demonstrations contribute to an understanding of the nature and quality of the product of physical research: scientific knowledge. 




## References
```{bibliography}
:filter: docname in docnames
```