

# Chapter title


Author: Ed van den Berg    \
Time:	  	\
Age group:	14 - 18\
Concepts:	

## Introduction

## Equipment

## Preparation

## Procedure

```{figure} demo43_figure1.jpeg
---
width: 50%
align: center
---
some caption
```

## Physics background

## Follow-up

## References
```{bibliography}
:filter: docname in docnames
```