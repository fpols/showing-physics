

# Reflection
Author:     Ineke Frederik\
Time:	  	10 minutes\
Age group:	12 - 15\
Concepts:	laws of reflection, Snell’s law, reversibility of light rays

## Introduction
The start of a new topic should be somewhat exciting, stimulating curiosity. We describe a start for optics in which all students participate. This way of starting is also a model for physics itself: students make predictions which are then investigated with experiments.\
The students receive stickers which they have to paste on the wall at the spot where they expect the laser dot will appear after the laser light is reflected by the mirror.  

## Equipment
* Laser on a stand
* Flat mirror clamped on a stand, the mirror is covered by a piece of black cardboard which can be flipped over (adhesive tape on one side)
* Classroom which can be darkened
* Round colored stickers (at least one for each student, the stickers could also be small post-its)


## Procedure
1.	Each student writes his/her initials on the sticker(s). 
2.	Show the set-up. Point to the laser and the mirror covered with the cardboard. The laser causes a dot of light on the cover of the mirror. Then switch off the laser and flip over the cover of the mirror. 
3.	Tell the students that you will switch on the laser again after they paste their sticker somewhere on the wall where they expect the laser dot to appear after reflection.  They have to remember where they put it and why there. There is a prize for the owner of the sticker closest to the actual dot. 
4.	Students walk around with their stickers.  You ask them why they put their sticker at a certain place.
5.	When all stickers are on the wall, ask the class to predict which one of the stickers will be the closest.
6.	Switch on the laser. Discuss the results using Snell’s Law of Reflection and the reversibility of light rays. Use three rods: one for the incident ray, one for the normal, and one for the reflected ray. Show clearly that angles of incidence and reflection are equal AND that incident and reflected rays and the normal are in one plane.
7.	You may repeat the whole procedure, but now with two mirrors. 

## Physics background
The spot on the wall from which you can see the laser through the mirror is the spot where the laser ray will land after reflection. You use the principle of reversibility of light rays. 
