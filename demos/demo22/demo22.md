

# Infra-red investigations and surprises


Author: Norbert van Veen\
Time:	  	10 minutes\
Age group:	12 - 18\
Concepts:	infrared, visible light, absorption, transparency

```{figure} demo22_figure1.jpg
---
width: 50%
align: center
---
CAPTION
```

## Introduction
Infrared radiation is a well-known subject in physics. Students get acquainted with it in junior secondary and learn about the properties of this kind of radiation in senior secondary. This demonstration is suitable for both junior and senior secondary physics. With the help of the IR- camera (FLIR) which can take photos and video, we can show the world in visible light and IR on an interactive white board or screen.

## Equipment
* Black garbage bag
* Glass plate/door/window
* IR camera (we used the FLIR C2)

## Preparation
Provide a garbage bag and a glass plate. Connect the IR-camera to the computer and set it to streaming mode so that you can use the live streaming feature of the software. Show the video on a digibord or screen.


```{figure} demo22_figure2.jpg
---
width: 50%
align: center
---
CAPTION
```

## Procedure
1.	Ask students to mention differences between IR and visible light.
2.	What similarities do students know between IR and visible light?
3.	Show examples with the IR camera of objects that are heat sources and explain that the camera creates false colors so that we can distinguish areas with different temperatures. The camera assigns different colors to different intensities.
4.	Let a pupil stand behind a glass plate and film showing the pupil in the infrared modus (not in MSX mode). Students notice that infrared does not pass through the glass.
5.	What will the IR camera show when a student is behind a glass window? Let students predict and then try this!
6.	What does the glass do with IR? Does it absorb infrared, or does it mirror infrared? How can you test your answer with this equipment?
7.	Use this experiment to discuss transparency, opacity or absorption of objects for electromagnetic radiation. For example, UV radiation and glass.
 

```{figure} demo22_figure3.jpg
---
width: 50%
align: center
---
CAPTION
```
## Physics background
Visible light is not absorbed by the glass, but is absorbed by the black garbage bag. The opposite is true for IR radiation. Glass reflects infrared like a mirror.

```{tip}
* When filming students behind a glass window, point the IR camera at a slight angle, or else you film your own  IR reflection.
* Photograph yourself in front of a metal surface (for example aluminum foil), the photo will then indicate almost 37 degrees as surface temperature.  You just measured the temperature of your body and not the temperature of the metal surface!
```

## Follow-up
Investigate if IR radiation penetrates all kinds of plastic bags. Can you find material that is the most transparent to IR radiation?
Of course, there are countless possibilities for research with an infrared camera. For example, where is the greatest heat loss of a person? Is there a difference between bald people and people with a lot of hair? What is the influence of a beard? Are there diseases that you can diagnose with infrared 'thermometry'?
