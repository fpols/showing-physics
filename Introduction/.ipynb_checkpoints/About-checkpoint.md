# About this book
This book presents a selection of the 99 best physics demonstrations from the Dutch book series "ShowdeFysica" (Frederik, 2015, 2017, 2023). The book includes a chapter on the pedagogy of the demonstrations. The demonstrations are structured using three categories:
* Demonstrations for conceptual development
* Demonstrations on teaching scientific inquiry and/or nature of science
* Demonstrations for good fun



So, whether you want to deepen students' understanding of a specific topic, want to engage them in thorough thinking or if you were asked to demonstrate physics on a festive occasion, you can find demonstrations and inspiriation in this book. 

[]

## Origin of the book
The book originated in the collaboration between physics teachers and physics teacher trainers, united in the Dutch Society for Science Teachers [(NVON)](https://nvon.nl). The three Dutch volumes of Show*de*Fysica cover over a 200 demonstrations for secondary school physics, along with additional text on the pedagogy of physics demonstrations. Each of the demonstrations described are tested by various secondary school physics teachers and their technicians. 

Are the demonstrations described original? Don't expect them to be so. Most demonstrations were inspired by earlier versions of the experiments - where possible referenced to earlier descriptions - and edited by the authors to fit the didactic categories of this book. They have been widely tested in Dutch physics education. Not only by the authors of this book - all experienced teachers and physics didacticians - but also by a large group of physics teachers and technicians who were willing to cooperate. In the chapter [Authors](Authors) you will find the names of the authors and a small description of their working history. Moreover, you find a list of names of those who took the pictures, drew figures, tested the materials or helped in writing this version of the book. **NOTE** that your name can be amongst the list of contributors. If you have suggestions, you can either ... oro ... 

This book is published under the CC-BY-NC, what means that you are allowed to use and adapt the materials for non-comercial use and under the condition that you refer to the original work. Note that you can contribute to the demonstrations is this book. 

## How to use the book

Specifying exactly which student audience the demonstration is intended for 
proved to be difficult. If it is a matter of observing phenomena and the beginning of the development of vocabulary, many demonstrations can be used in elementary schools. In lower secondary schools, the demonstrations can usually be used for qualitative explanations at the phenomenon level. As soon as chains of concepts are needed for an explanation you end up in higher grades of secondary education. If you need to establish relationships between different areas of physics or if quantitative reasoning is necessary, the level will soon be upper secondary school.

![figure1](metalballs.jpeg) <center><i> Fig.1 - The cooling of two heated metal balls. Which one cools down fastest, the big or the small ball?</i></center>



### Demonstrations for conceptual development
Most of the demonstrations in this book relate to conceptual development. These demonstrations have been chosen for their suitability to elucidate a physical phenomen or concept. They belong in the regular instruction lessons. The descriptions often include step-by-step suggestions for didactic implementation, including opportunities to get students thinking for themselves as much as possible. They stimulate physical thinking and help students build a physical concept network.

### Demonstrations on teaching scientific inquiry and/or nature of science
What does *"It is scientifically proven"* mean? What makes it so valuable? What do you have to do to prove something scientifically, and how sure are you then of your case? The demonstrations described here revolve around the skills that are indispensable in scientific research on the one hand and about the characteristics of doing science on the other. These skills include carefully observing, critically analyzing a problem, developing a model to explain observations, or testing expectations with an experiment (Pols, Dekkers & de Vries, 2022). The knowledge created in the process is man-made. That knowledge is as certain as knowledge ever gets and yet will always be open to improvement. 

### Demonstrations for good fun 
Demonstrations also play a role at special occasions: at an open day, at an anniversary, at a farewell. In this book you  will find examples of demonstrations that stimulate thought, but which also have a surprising or playful twist, making them suitable to show at such special occasions. The topics are by no means always part of the school physics curriculum. However. they all appeal to common sense and the outcome is often counterintuitive. The charm of physics is shown to its full advantage in this volume!

## Disclaimer
```{Warning}
We have carefully conceived, developed, described and tested each of the experiments. Moreover, we specified safety issues where necessary. However, we are not responsible for whatever accidents, or injuries when doing a demonstration described in this book. We want to emphasize as well the requirement to test a demonstration before doing it with public. 
```

## References
> Frederik, I. et al (2015) ... Show*de*Fysica

> Frederik, I. et al (2017) ... Show*de*Fysica 2

> Frederik, I. et al (2023) ... Show*de*Fysica 3

> Pols, Dekkers & de Vries, Defining and assessing understandings of evidence with the assessment rubric for physics inquiry: Towards integration of argumentation and inquiry, Physical Review Physics Education Research (2022). https://journals.aps.org/prper/abstract/10.1103/PhysRevPhysEducRes.18.010111
