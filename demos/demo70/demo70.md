

# Cylinder Puzzle: Exploring the Nature of Science

Author: Peter Dekkers     \
Time:	  15-30 minutes	\
Age group:	14 - 18\
Concepts:	

## Introduction
This activity does not involve physics but demonstrates a model for how physicists (and other scientists) work and what they try to achieve. Students take on the role of scientists to describe and explain confusing and astonishing observations. Then, they reflect on them.

```{figure} demo70_figure1.jpg
---
width: 50%
align: center
name: fig:cylinder_puzzle1
---
The cylinder puzzle - A model of the universe
```

## Equipment
Opaque cylinder with opaque lids on both sides; about 50 cm of sturdy rope or cord.

## Preparation
Make four holes in the cylinder. Use two pieces of rope, thread them through the holes, with one inside the cylinder passing over the other, for example, as shown in the top left of figure 2. Adjust them: both strings should loosely fit inside the cylinder. Tie knots at the ends. Then, pull on one of the knots: it should result in something like figure 1. Each time, a few centimeters of rope should come out, while pulling the protruding piece inward.
```{figure} demo70_figure2.png
---
width: 50%
align: center
---
All these models of the contents of the cylinder are consistent with all observations.
```


## Procedure
Explain that the cylinder you have is a model of the universe. You can only look at it but not enter. The task for the class is to study this universe in a scientific manner. So, observing carefully what happens is important!
Successively, pull on three knots with a short, firm tug, for example, A-B-D-B. Repeat it a few times. Also, try A-D-B-D-A-B. Leave knot C alone.

1. **Observing and recording.** Carefully and systematically document your observations.
2. **Explaining.** What could be inside the cylinder so that the strings go in and out as we have seen? Explain. An explanation that could be correct but still needs testing is called a hypothesis.
3. **Predicting.** What will happen if you pull on knot C? Collect predictions. Then, pull on knot C. Make a drawing. Also, perform other actions suggested by students, so they can test their hypothesis(es). They may adjust them to match the new observations.
4. **Comparing answers.** Discuss the results together.
    - *Did you manage to come up with a good explanation? Why do you find it good?*\
   The explanation must match the observations, and you must be able to make accurate predictions with it.
    - *Are you sure now what's inside the cylinder? Why?*\
    Coming up with a good explanation is an excellent achievement. But you still don't know for sure what's inside: you would have to look.
    - *Do you think everyone found the same solution? Compare the solutions.*\
    There are many possible solutions: figure 2 shows a few. This also indicates that we still don't know for sure what's inside the cylinder.
    - *How certain are you of your model? Does it need further testing? What would you do? How much more work is needed?*\
    You're only really finished testing when you know what's inside the real cylinder. If your predictions keep coming true, you may assume that your model is very good. But all models in the figure predict the same thing, so which one is the real one?
5. **Reflection.** Is this science? 
    - *Why didn't you all find the same answer?*\
    It depends on your imagination and ideas, which are different for everyone. The same goes for scientists. Scientists also can't always open things up to see how they work inside: gravity, magnetism, living animals, chemical reactions, the universe. They only see the outside. If you try to "open" it, it won't work anymore.

6. Opening the cylinder? Let whoever really wants to open the cylinder do it now. Real scientists would also look inside the cylinder if they could because they are terribly curious.

## Physics background
Many of the things you did to solve the cylinder puzzle resemble what scientists do during research:
- You collect data (observe what happens when you pull the knots and then write it down).
- You draw conclusions from the data (for example: it doesn't matter which string you pull, a piece always comes out while the protruding piece goes in).
- You make a model (you used your imagination and creativity to think about what could be inside the cylinder).
- You use your model to make predictions and test those predictions.
- Maybe you refine your model during or after testing the predictions.

There are also differences with the work of scientists. In the universe that scientists study, you often can't open things up to see the inside. We can look, but scientists never know for sure how everything really works.

By making more observations and adjusting their models, scientists gain more insight into what happens and how it works. They use their creativity and imagination, and thus often come up with different answers and predictions. They can test these and improve their explanations and models. But they are never completely finished, and what they know is never absolute and completely certain.

## References
```{bibliography}
:filter: docname in docnames
```
Lederman, N. G. and Abd-El-Khalick, F. (1998) Avoiding De-natured Science - Activities That Promote Understandings of the Nature of Science. In W.F. McComas (Ed.) The nature of science in science education: Rationales and strategies. Dordrecht, Netherlands: Kluwer Academic Publishers. pp. 243-254