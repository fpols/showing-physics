

# Tug-of-War: can one girl be stronger than four boys?


Author:     Freek Pols\
Time:	  	  5 minutes\
Age group:	15 - 18\
Concepts:	  forces, components of forces, analyzing forces

## Introduction
Can one girl be stronger than 4 boys? Sure! if she is clever…

## Equipment
* 4 boys 
* 1 girl
* 1 strong rope

```{figure} demo13_figure1.jpg
---
width: 70%
align: center
---
The young lady will always be stronger
```

## Preparation
Ask four strong boys and choose one girl as volunteers to help.

## Procedure
Ask the four boys whether they are, together, stronger than the girl. Let the four boys take up the rope, two on each end and encourage them to pull hard and keep the rope stretched horizontal. Then ask the young lady to use her finger to push the rope downward in the middle (figure 1). The boys will not succeed in keeping the rope tight and horizontal. 

Ask the students in the class to explain why the boys cannot keep the rope tight and horizontal. Make a simple drawing on the board and demonstrate that a vertical force of 50 N exerted by the girl requires each boy to exert a force of more than 700 N when the angle between the rope and horizontal is 1 degree. This is a surprisingly large force to compensate the small vertical force exerted by the finger.

```{tip}
Another version of this demonstration was published by {cite:t}`Vollebregt`. They connected two ropes on either side of a crate of beer and told the boys to pull so hard that the ropes would be horizontal. Impossible! 
```

```{figure} demo13_figure2.jpg
---
width: 70%
align: center
---
Neither here can the rope be pulled horizontal
```

## Physics background
The boys exert a horizontal force. While the rope is horizontal, this force does not have a vertical component. When the rope is pressed downward by the finger, there will be a vertical component of the force of the boys, however, it is $2\cdot F_{boys on rope} \cdot sin(\theta) = -F_{lady on rope}$. Angle $\theta$ is the angle between rope and the horizontal and so very small. So 
$$F_{boys on rope} = \frac{-F_{lady on rope}}{2·sin(\theta)} $$

## Follow up
Use photographs of cable car and powerline set-ups to calculate the tension in the cables.

## References

```{bibliography}
:filter: docname in docnames
```