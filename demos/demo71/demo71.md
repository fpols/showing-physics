

# Magic or Not? Nature of Science with a Siphon

Author: Peter Dekkers    \
Time:	  30 minutes	\
Age group:	14 - 18\
Concepts:	

## Introduction
You demonstrate a surprising phenomenon and use it to work on the understanding of the Nature of Science. It's about the journey, not the answer.

```{figure} demo71_figure1.jpg
---
width: 50%
align: center
---
Magic box. Photo: Aike Stortelder.
```

## Equipment
- 2 large beakers
- 1 small beaker
- Food coloring
- Water
- Box
- About 1.5 m of water hoses;
- 2 funnels
- Double-sided tape or tape
- Water-resistant caulk or glue
- Empty bottle

## Preparation
Place the box on a short side, lid at the back. Drill two holes on top, place the funnels in them. Drill a hole on each side of the box. Attach a hose directly from one funnel to the outside through a hole on the side. Place a large beaker underneath. Do the same for the other funnel, except that there is a siphon between the inlet and outlet. Cut the bottom of the plastic bottle you will use as a siphon. Drill a hole in the cap of a bottle, attach a long piece of hose, glue the hose securely, and put the cap back on the bottle. Attach the bottle upside down inside the box. Hang the hose from the second funnel into the bottle. The hose coming out of the cap first goes up to just below the top edge of the inverted bottle. Then out of the remaining hole on the side. Secure everything well. Make the hoses on the outside of equal length. Ensure that the outflow point of the second hose is under the cap of your bottle. A large beaker should fit under it. Ideally, the box looks symmetrical from the outside. Fill the siphon with water and some food coloring up to just below the rim. Fill the small beaker with water, just enough to start the siphon.

## Procedure
Ask students to pay close attention so they can describe exactly what they saw later. Then pour the water carefully through the first funnel. Collect it in the large beaker and pour it back into the small one. Have students write down their observations. Expected statements: you pour water into the funnel on one side and it flows out through the hose on that side. Then pour the liquid into the small beaker through the second funnel. If properly adjusted, nothing will happen at first, but then the liquid will start flowing out of the hose. Soon you'll notice that the color is different. Then you'll see that the outflow takes a long time. Does it stop? Yes, eventually it does. What's in the box? Discuss with each other and make a sketch. Does the sketch explain the observations?

Select questions for the follow-up discussion:
1. You've come up with an explanation, but where did it come from? What did you use to find it? Scientists work in the same way.
2. Is it necessary to test if your explanation is correct? Why or why not? How can you test your explanation?
3. Different explanations have been suggested in class. If you have to find the best explanation, what will you pay attention to?

### On the site
You can elucidate the following aspects of Nature of Science based on these questions. See also the introduction.
1. Curiosity is a key driver for scientists. If they don't understand something, it's not disappointing but rather interesting and exciting.
2. What you observe depends on what exists, but also on how you look. Scientists often only see something when they know what to look for.
3. To explain what you don't understand, you use knowledge you already have. Scientists do the same. For example, because you know water as a clear liquid whose color and quantity do not change easily, you probably did not find the first event surprising. There was nothing to explain there, but suddenly there was. The explanation you came up with is based on knowledge and experience, but also on your creativity and imagination. Scientists also use creativity and imagination when inventing explanations!
4. Explanations are not simply accepted. Scientists will thoroughly test them first. There are various ways to do this. For example, what should happen according to your explanation if you pour another cup of water into the box now? See the introduction for a further discussion of the characteristics of a good scientific explanation.

## Physics background
See Figure 2. Initially, the siphon is almost filled to the top (left). With the little extra liquid from the small beaker, the liquid at A turns the corner. The tube goes up and down but remains completely filled all the time. The force that drives the water out of the siphon and into the tube is due to the pressure difference between A (1 bar) and B (liquid pressure at depth AB). This becomes smaller as it empties, but is zero only when the siphon is completely empty.

```{figure} demo71_figure2.png
---
width: 50%
align: center
---
Diagram of the siphon, connected to one of the funnels. At the start, the siphon is almost completely filled with water and some dye.
```

```{tip}
Completely emptying a siphon remains peculiar, of course. Feel free to turn the box upside down after the discussion and show how it works. Perhaps searching for good explanations is just as important as having them.
```

## References
```{bibliography}
:filter: docname in docnames
```
Lederman, N. G. and Abd-El-Khalick, F. (1998) Avoiding De-natured Science - Activities That Promote Understandings of the Nature of Science. In W.F. McComas (Ed.) The nature of science in science education: Rationales and strategies. Dordrecht, Netherlands: Kluwer Academic Publishers. pp. 243-254.