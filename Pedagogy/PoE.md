# Predict Explain Observe Explain


In many phenomena, students and laypeople come up with predictions in which they have a lot of confidence, yet turn out to be wrong. Our intuition is often off the mark. It is precisely in these cases that demonstrations can be extra motivating and useful, but then as a teacher, you must optimally emphasize the contrast between prediction and outcome and try to stimulate the thinking of all students instead of just a few smart ones.

**All Students**

All students, therefore, individually formulate their expectations/predictions on paper with a rationale. All students observe the result of the demonstration. All students are involved in resolving the difference between prediction and observation. This can be done effectively with a Predict-Observe-Explain (POE) demonstration. Even better is the term PEOE (Predict-Explain-Observe-Explain) to emphasize that we ask students for a prediction with reasoning, not just a guess. This prediction can only be asked for in demonstrations where students themselves have prior knowledge and think they know what will happen. Therefore, not every demonstration is suitable for the PEOE model.

**Steps**

The steps in PEOE are illustrated in Table 1 with the trivial example of a large and small falling stone (Figure 1). (Both the mass and the volume of the stones are different). Many students (and their parents) expect a large stone to fall faster than a small one.


```{table} The various steps in the PEOE approach
| Step | Steps in PEOE | Example |
|------|---------------|---------|
| 1    | Show how you will perform the experiment or ask "how can I investigate whether...?". Then come up with a plan for the demonstration with the students and make sure it is clear to all students before they start predicting. | I (teacher) have two stones, a large and a small one. How can I investigate which one falls faster? |
| 2    | Predict – Explain: Let students individually write down their expectation (predict what will happen) and the reason for it. This can be in the notebook or on a special worksheet. It can also be with multiple-choice questions in www.Socrative.com or www.plickers.com. | I will let the stones fall simultaneously by pulling my hand downward (see figure 1). Write down which stone will hit the ground first. Explain why you think so. |
| 3    | A short inventory of expectations. It is most important that students realize there are different predictions, usually with 'reasonable' arguments. Because everyone has written down a prediction with a reason (or has voted on Socrative), any random student can be asked to contribute to the discussion instead of just volunteers. | Inventory of reasons for the three possible predictions (large or small stone or equal). Results of the vote and possibly arguments noted on the board. |
| 4    | Observe: Have each student write down their own observation before discussion begins. If this does not happen, students will 'adjust' their observations as soon as discussion starts, and differences in observation will not be clear. It is usually possible to repeat the experiment until there is consensus about the observation or measurement. | The teacher stands on the table with the two stones and lets them fall onto the table surface or onto a plank on the ground that makes a good noise. Everyone listens to the result. Repeat until everyone agrees on the result. |
| 5    | Explain: Ask students to discuss (think-pair-share) the explanation for the observation or measurement. In the subsequent class discussion (step 6), each student can again be asked to contribute. | Think-share-exchange ideas about whether size has anything to do with fall speed. |
| 6    | Class discussion. | Class discussion and conclusion that size apparently does not matter. Are there examples of objects that do not hit the ground equally? |
| 7    | Noting the complete explanation on the board or referencing the notebook. |  |
| 8    | Are there other similar phenomena that we can now also explain? Continue the demonstration with objects where air resistance plays a role, for example, first an A4 paper, then a crumpled ball, then placing the A4 sheet on top of a book and letting them fall together. |  |

```

In most PEOEs, you are ready with step 7. With falling objects, you must also investigate the role of friction as indicated in step 8.


``` {figure} Figures/poe.png
---
name: fig_poe
width: 70%
---
Hier een beschrijving
```
Only when students are aware of their own expectations/predictions will there be attention for the demonstration itself. Writing down the prediction and an accompanying rationale leads to more engagement than just asking classically and then getting an answer from a smart student. By asking all students for the reasoning behind their prediction, the teacher also gains insight into student thought processes. Students notice that different thoughts exist about the phenomenon; there are conflicting predictions and different ideas about the phenomenon. This generates motivation to find out how it really is. It helps students evaluate their own learning and construct new ideas.

{cite:t}`mitchell1992some` gives the following advice based on his extensive POE experience:

1.  It is very important that students realize that they are not alone in their prediction. Therefore, it is important to summarize the class's ideas and report back to the class. An effective way to do this is to have predictions written down in a handy format so that the teacher can quickly take inventory by walking around.
2.  Students must feel safe to give their opinion. Therefore, never link assessment to a PEOE, not even praise for a correct answer. Emphasize to students that you are interested in their ideas and that both wrong and correct predictions are important for the class reasoning process    and for their own learning process.
3. A discussion after the prediction where different predictions are explained and discussed often works well. This discussion precedes the observation, the execution of the demonstration itself.
4. At the end of PEOE, emphasize that incorrect predictions are often quite understandable or even 'logical' and that they are very useful in the learning process.

See {cite:t}`white2014probing` for a very complete description of the pedagogy. Google with 'POE' and 'physics demonstrations' as search terms to find examples. {cite:t}`liem1991invitations` "Invitations to Science Inquiry" contains over 400 demonstrations with surprising outcomes for students.

## References
```{bibliography}
:filter: docname in docnames
```