

# Air is not nothing: weighing air


Author:     \
Time:	15-30 minutes  	\
Age group:	10-12\
Concepts:	properties of matter, weight, buoyant force, gravity

## Introduction
For young children, 'air' is not really a concept they can grasp. After all, an empty glass contains: nothing. By comparing the weight of air to that of matches, air becomes more tangible. In higher grades, you can involve the effect of buoyant force. The setup is deceptively simple; making a correct prediction and explanation is not trivial.

## Equipment
* A few straws
* Several identical balloons
* A needle 
* A piece of fishing line (or string)

## Preparation
Thread the needle through both straws, attach loops of fishing line to the ends of one straw in such a way that there is balance when you attach empty balloons to them. See figure 1.

```{figure} demo30_figure1.JPG
---
width: 50%
align: center
---
Two empty balloons weigh the same.
```



## Procedure
**Predict**
1. Show that the balance can rotate with the balloons. Announce that one of the balloons will be inflated and rehung.\
Ask: *What do you think will happen then? Will the balance: 
   1. remain in equilibrium, 
   2. will the empty balloon go down, or 
   3. will the inflated balloon go down? 
Explain what you expect, and why you expect that.*

2. Discuss the expectations and reasoning. Conclude: the heaviest side will go down. So:
   - If answer 1 is correct, the air in the balloon weighs **nothing**.
   - If answer 2 is correct, an empty balloon weighs **more** than an inflated balloon.
   - If answer 3 is correct, an inflated balloon weighs **more** than an empty balloon.

3. For upper grades: Add: *Consider in your prediction that you must not neglect the buoyant force in this case! When inflating the balloon, both gravity and buoyant force may change.*

**Observe**
4. Inflate one of the balloons and attach it to the straw. If this is done carefully, there will no longer be an equilibrium: the air in the inflated balloon makes that side heavier. Equilibrium can be restored by, for example, putting matches in the still-empty balloon. Typically, you need 2 to 3 matches to restore equilibrium.

```{figure} demo30_figure2.JPG
---
width: 50%
align: center
---
The inflated balloon moves downward.
```
**Explain**
Explanation for lower grades: *A balance is a clever device for determining how much of something you have. There are all kinds of balances; our type has been used by people for thousands of years to measure weights. Our balance is special; you can even use it to measure the weight of the air in a balloon. That weight is very small but larger than that of a match.*

Explanation for upper grades: *When inflating the balloon, gravity increases (more mass), but so does the buoyant force (more volume). Because the air in the balloon is compressed compared to the surrounding air, the density has increased. Therefore, gravity has increased more than the buoyant force, causing the inflated balloon to descend.*

## Physics background
A balance compares the gravity on two objects. This works well when there is only a homogeneous gravitational field, so in an accelerating elevator but not in a medium with high density. In a scale that compares gravity with a spring force, it is often exactly the opposite.
The buoyant force on the inflated balloon is equal to the gravity that would act on that volume if ordinary air were there: $F_{\text{buoyant}} = F_{\text{g, ordinary air}}$. For the whole balance, the right side can be written as:
$$F_{\text{g, inflated balloon}} - F_{\text{buoyant}} = F_{\text{g, balloon + compressed air - ordinary air}} \\ = F_{\text{g, balloon + extra air}}$$

If you neglect the buoyant force on the left, then the gravity on the matches is equal to that on the extra air compressed into the balloon. Following the usual definition of 'weight' as the net force on the support, *this is also the weight of all the air in the inflated balloon.* The weight of that air is then not equal to the gravity acting on that air, and you do not make a mistake in the lower grades if you refer to 'the weight of the inflated balloon' in this experiment.
