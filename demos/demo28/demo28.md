

# Warm & cold, salty & fresh, and ocean currents

**REFERENTIES ONTBREKEN, FILMPJE MIST**
Author:     \
Time:	15 minutes  	\
Age group:	14 - 18\
Concepts:	temperature, density, convection, salt concentration

## Introduction
The Gulf Stream flows like a 100 – 200 km wide river across the Atlantic Ocean from the Caribbean to Europe. In the far north of the Atlantic Ocean, the water becomes so cold at the surface that it has a greater density than slightly warmer underlying water, it sinks and is carried back to the tropics at depths of several kilometers. This Atlantic conveyor belt is maintained by differences in temperature and salinity, wind, and the rotation of the earth (Coriolis force). A series of demonstrations will sequentially show that warm water 'floats' on cold water, fresh water on salt water, and that differences in temperature and salt concentration can cause currents. In the aquarium, we see the beginning of such a conveyor belt system that can transport a lot of energy due to the high specific heat capacity of water.

<div style='text-align: center;'>

```{code-cell} ipython3
:tags: [remove-input]
from IPython.display import YouTubeVideo
VideoWidth=600
YouTubeVideo("v=bN7E6FCuMbY", width=VideoWidth, align='center')
```

## Equipment
* 8 glasses or jam jars 
* Plastic card
* Aquarium or terrarium with tap water
* Red and blue dye (liquid food coloring)
* Warm (red) and cold water (blue), each 2 glasses
* Ice cubes colored blue, saltwater and tap water both colored (each 2 glasses)

## Preparation
Watch videos in this document and practice once. **1 VIDEO ONTBREEKT**

## Procedure
1. Teacher: *In the oceans, there are cold and warm ocean currents. The Gulf Stream brings so much warmth from the Caribbean to Europe that Northern Europe is habitable. In contrast, while corresponding parts of Eastern Canada are barely inhabited. Today we study how such a current arises.*
2. Take the glass of warm water, cover it with the plastic card, turn it over and place it carefully on the glass of cold water (see figures 1 & 2).
3. Draw the situation on the board and ask for a prediction: *If I remove the card, what will happen to the water and why?*
4. Teacher removes the plastic card. The water does not mix (figure 2).
5. Repeat with cold water on top and warm water below. *What will happen, why?*
6. Teacher removes plastic card. The water changes color (figure 3).
7. Discussion about explanation.
8. Optionally, measure exactly equal volumes of warm and cold water in graduated cylinders and weigh them on a digital scale with an accuracy of 0.1 g. Is there a difference?
9. Conclusion: *temperature differences can cause currents.*
10. Do the same with salt and fresh water.
11. Introduce the aquarium. *What will happen if I place colored ice cubes on one side of the water?* Draw the flow that could occur in the water.
12. *What if I add a little warm (red) water to the other side?* Draw the flow.
13. Now execute, ensure a clear background. We see a convection cell.


```{figure} demo28_figure1.jpg
---
width: 50%
align: center
---
Red-colored warm water and blue cold water.
```

```{figure} demo28_figure2.jpg
---
width: 50%
align: center
---
Warm above and cold below do not mix.
```

```{figure} demo28_figure3.jpg
---
width: 50%
align: center
---
Cold above and warm below do mix.
```
## Physics background
Water expands when heated, except between 0 and 4 degrees (anomaly of water). So, cold water has a greater density than warm water and will sink while warm water will rise. The same applies to salty versus fresh water. These mechanisms play a role in ocean currents, alongside the influence of wind and the rotation of the earth. Ponce de Leon, a successor of Columbus, had already noticed the Gulf Stream. As head of the American postal service, Benjamin Franklin became interested in the time differences between postal ships taking different routes (Winchester, 2010), and through interviews with captains, he mapped out the Gulf Stream. Because of the large specific heat capacity of water and the huge amount, the effect of the Gulf Stream on the climate in Western Europe is significant.

```{tip}
https://www.youtube.com/watch?v=bN7E6FCuMbY
```
**Als de inline video werkt moet deze tip weg**

## References
```{bibliography}
:filter: docname in docnames
```