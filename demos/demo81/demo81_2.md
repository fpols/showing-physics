

# Resonating Rod: Waves and Vibrations

Author:     \
Time:	5-10 minutes\
Age group:	14 - 18\
Concepts:	

## Introduction
A metal rod can produce beautiful sounds. However, how it sounds depends on where you hold it.

```{figure} demo81_2_figure1.JPG
---
width: 50%
align: center 
---
Holding the rod in the middle and tapping it against a table or striking it in another way produces a nice sound.
```


```{figure} demo81_2_figure2.JPG
---
width: 50%
align: center 
---
If you hold the rod at the end, you only hear a dull thud. The rod does not resonate.
```


## Equipment
Metal rod, such as a stand.

## Preparation
Test beforehand where to tap the rod and hold it in various places to get an idea of how it sounds. Optionally, try another rod made of different material.

## Procedure
1. Hold the rod in the middle and let it resonate.
2. What do you hear and why?
3. Then hold the rod a quarter from the top.
4. Can you predict whether you will hear a higher, lower, or the same tone now? (Predict-Explain-Observe-Explain)
5. In a class discussion, the intention is for students to understand that this relates to nodes and antinodes and overtones.
6. After that, you can choose another place to hold the rod one or two more times.
7. Control question: What will you hear if you hold the rod at one of the ends and let it resonate?

## Physics background
By striking the rod at one end (for example, against the table), many different vibrations are created in the rod. By holding the rod in the middle, a node is created there, leaving only vibrations with a node in the middle. Vibrations with a higher frequency dampen faster. Therefore, you hear the relatively low fundamental tone the best. Sometimes you have to wait for the overtones to die out.
By holding the rod a quarter length from the top, a vibration with a complete wavelength, thus resulting in an octave higher tone, fits as the fundamental tone.

## Tips
• Do not hold the rod too tightly, as this will cause excessive damping. \
• Measure the frequencies with a computer or the phyphox app.