

# Magic Memory Balls: Density of Solids and Floating and Sinking

Author:     Bart van Dalen\
Time:	  	  10 minutes\
Age group:	11-18\
Concepts:	  density, floating, sinking, buoyant force (Archimedes Law)

## Introduction
In this demonstration metal balls change into ping-pong balls. Of course that is impossible, but the initial explanation of the presenter also does not make sense. So how does it work? The observations challenge the students to find explanations. A very simple demo which challenges the audience to think about the concepts density, floating, and sinking.

## Equipment
* Three metal balls; 
* three pingpong balls; 
* corn used for making popcorn; 
* any container. 

```{figure} demo08_figure1.jpg
---
width: 50%
align: center
---
Balls before the transformation
```

## Preparation
Fill the container with the corn. Push the pingpong balls to the bottom of the container. They should not be visible. Keep the metal balls separate. 

## Procedure
Show the metal balls to the audience, pass them along. Meanwhile tell them that they look like normal metal balls, but that they are made of a special memory metal. Then put the balls on top of the corn and tell that they assume the properties of the corn when you heat them up. Let the audience tell you what happens to the corn when you make popcorn. Seduce the audience to use the terms “greater” and “white”. 
Put the metal balls on top of the corn, then put the lid on the container and “heat” the corn by shaking the container. The metal balls will sink while the Ping-Pong balls will rise. Open the container: no more metal balls, but instead Ping-Pong balls (figure 2)!

```{figure} demo08_figure2.jpg
---
width: 50%
align: center
---
Balls after the transformation
```

Of course nobody will believe that the metal really changed into plastic, but what happened then? Discussion will occur spontaneously. The surprise of this demonstration will captivate, but to figure out the secret one has to do some hard physics thinking.
Spontaneous reasoning is often going into the right direction, but needs some correction. Chances are that somebody will propose that the metal balls are heavier than the corn. Then let them compare the mass of the three balls with the whole container of corn. Then of course the metal balls are not heavier, but it is not a fair comparison. When you compare more or less equal volumes of corn and metal ball, then the metal ball indeed turns out heavier. That is a good opportunity to discuss density and the meaning of “fair comparison”.
If the metal balls “sink” because their density is greater than that of corn, can you then also explain that pingpong balls will rise when shaking the container? As a check you can put the Ping-Pong balls at the bottom of the container and leave out the metal balls. It was striking that half of the elementary education majors predicted that in this case the Ping-Pong balls would not rise. The characteristics of the buoyant force apparently are quite counter intuitive.

  ## Physics background
You will only understand the experiment when you become aware that there are two forces acting on the metal ball and the Ping-Pong ball and that you have to compare them before you can predict what will happen. The corn exerts a buoyant force upward which for the metal ball is smaller than gravity but for the Ping-Pong ball greater than gravity. So the metal balls sink while the Ping-Pong balls will float on the corn.

