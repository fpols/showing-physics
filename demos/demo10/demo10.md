

# Does van de Graaff not like a shower? Testing statements about electrostatics with a Van de Graaff generator and soap bubbles

Author:     Wouter Spaan\
Time:	  	  15 minutes\
Age group:	15 - 18\
Concepts:	  observe and explain; reasoning with charge, electrostatic induction, and the Coulomb force.

## Introduction
The demonstration of the interaction of Van de Graaff generator with soap bubbles is well-known. In the version described below our focus is on the development of explanation and reasoning skills. 

## Equipment
* Van de Graaff generator
* soap bubble gun
* video recording equipment (e.g. Cell phone)

```{figure} demo10_figure1.jpg
---
width: 50%
align: center
---
The soap bubble gun and the Van de Graaff generator. In this image, most bubbles move away from the generator. 
```

## Preparation
Make sure you can blow the bubbles from above to the Van de Graaff generator for best visibility.

## Procedure
Blow the bubbles from above to the Van de Graaff generator or let a student do this. Ask the students to observe and write down their observations. You will probably get remarks such as 'the bubbles move away from the generator'. A good observer may have seen that initially the bubbles are moving towards the generator.

```{figure} demo10_figure2.jpg
---
width: 50%
align: center
---
The bubbles on the left move towards the Van de Graaff generator, while the bubbles on the right move away from the generator. How is that possible?
```


Then you start the process of developing explanations and testing them with better observations. The concept cartoon (figure 3) can be helpful as a start. It is important to determine the observable consequences of the proposed explanations in a classroom discussion first, so that you can investigate which explanation is the best one:
1.	The bubbles are neutral (not charged) throughout the process: the bubbles will move towards the Van de Graaff generator just like small pieces of paper.
2.	The bubbles are charged from the start: depending on the charge of the bubbles they will move towards the generator or away from it.
3.	The bubbles acquire charge on the way: the bubbles will change their behavior halfway. First they move towards the generator (as in explanation 1) and then their behavior depends on the charge of the bubbles (as in explanation 2).

You can then record the experiment on video and play it on a screen. Blow several series of bubbles so that you will have enough video recordings. First the students can improve their own observations and then discuss which explanation best fits the observations. The film can be played more slowly and then you will see clearly that the bubbles initially move towards the Van de Graaff generator, only to turn around and move away eventually (or burst). Obviously, number 3 (the bubbles acquire charge on the way) is the correct conclusion, although some students will probably persevere in a different opinion. For a complete explanation it is important to focus on the moment the bubbles change their behavior, since there is no explanation to the crucial question how these bubbles get charged, yet. It turns out the bubbles change their behavior just after one of them hits the generator and pops. Sometimes it can also be seen that the bubbles move away from each other, but this is not always visible.

Finally, it is important to look back from the perspective of doing research. From 
possible explanations (hypotheses) we have formulated observable consequences (predictions) to investigate through an experiment which explanation is (probably) the correct one. This provides a nice insight into the world of research. Hopefully this also shows that doing research is fun.

```{tip}
There are even more ways to work on research skills with this demonstration:
•	If the students write down their observations, then you can choose to pay attention to the objective formulation of observations. For example, what is the difference between 'the bubbles are attracted by the sphere' and 'the bubbles move towards the sphere'?
•	You can discuss the difference between a hypothesis and a prediction. A hypothesis is independent of the experiment, the prediction is dependent on the set-up of the experiment. One possible hypothesis is ‘the bubbles are negatively charged from the beginning’, a potential corresponding prediction is 'the bubbles will move away from the negatively charged Van de Graaff generator’.
```

## Physics background
Initially, the bubbles are attracted by the Van de Graaff generator through electrostatic induction: the dipolar water molecules align with the electric field of the generator. Because the field is inhomogeneous, there is a force towards the charged sphere of the generator. As a result, initially the bubbles move towards the Van de Graaff generator. They have no net charge yet! When the first bubble touches the generator sphere, it will receive the same type of charge as the generator and it will burst. The drops also have the same charge and are thus repelled by the sphere. Those drops hit the bubbles that are still on their way to the generator, and as a result these bubbles also get the same type of charge as the Van de Graaff generator. This then causes a repulsive force and the bubbles will start moving away from the generator. A new series of bubbles will initially be attracted again and so on.

## References
```{bibliography}
:filter: docname in docnames
```