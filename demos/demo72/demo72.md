

# Roll and Stop


Author: Freek Pols    \
Time:	  15 - 20 minutes	\
Age group:	14 - 18\
Concepts:	

## Introduction
This is one of the 'Black Box' activities suitable for addressing how science works. Students observe, infer, devise hypotheses, and then test them. They need to be creative because figuring out what exactly is happening is not simple. We may even never know...

## Equipment
-Cylindrical container such as a paint can or PVC pipe with caps
- Viscous liquid (glycerol)
- A piece of (fish) lead that fits well inside the cylinder
- Plank set at a slight angle

## Preparation
Fill the cylinder completely with the liquid and add the lead (wash your hands afterward). Set up the plank at an angle so that the cylinder just starts rolling by itself.

## Procedure
1. What kind of motion will the cylinder make when you roll it off the plank? Sketch a graph. It is intended that students do not know what is inside the cylinder. Perform the demonstration.
2. Probably to everyone's surprise, the cylinder will roll, stop, roll again, and stop again. Discuss in pairs what might be happening here. Make a drawing of how you think the inside of the cylinder looks.
3. Discuss the different ideas centrally and write them on the board.
4. Have students come up with an experiment for each of the ideas (hypotheses) to test them. Remember, you cannot open the cylinder.
5. Perform the experiments, when possible. Is the hypothesis confirmed or not? Are there other hypotheses that are also ruled out as a result of the devised and performed experiment?
6. Control question: instead of testing students' knowledge, you can use metacognitive reflection here. Reflection should make it clearer for them what they have learned and why it is relevant.
7. Note down what you now know more about science and how science works. Ask for the answers and try to categorize them together.


Depending on the desired outcome, one or more aspects of NoS can be discussed, see https://www.nsta.org/nstas-official-positions/nature-science and the introduction on page ##.


## Physics background
Due to the viscous liquid, the piece of lead will not remain at the bottom of the cylinder, see figure 1. This exerts a torque causing the cylinder not to continue rolling. The lead sinks back to the bottom of the cylinder, allowing the cylinder to roll a bit further again.

```{figure} demo72_figure1.jpg
---
width: 50%
align: center
---
The setup consists of a cylinder filled with a viscous liquid containing a piece of (fish) lead.
```

## References
```{bibliography}
:filter: docname in docnames
```