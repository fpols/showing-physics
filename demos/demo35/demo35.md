

# Falling Candle

Author:     \
Time:	  	5 - 10 minutes\
Age group:	15 - 18\
Concepts:	heat transfer by convection, buoyancy, weightlessness, combustion

This demonstration is based on experiment 62 from part 2 of Walravens (n.d.), also published in NVOX (Walravens, 2011).

## Introduction
This experiment combines combustion with force and movement in a surprising way. The experiment lasts only a few seconds; the interesting part is predicting what will happen and finding an explanation.

## Equipment
* Tealight candle
* Large transparent jar with lid
* Drop of glue
* Matches

## Preparation
Glue the tealight candle onto the lid of the jar. Practice dropping and catching the jar a few times. Students should already understand how buoyancy, sinking, and convection heat transfer work. The explanation of the experiment relies on these principles in a not-so-obvious way.

```{figure} demo35_figure1.jpg
---
width: 50%
align: center
---
Tealight candle in a closed jar.
```

## Procedure
### Predict
Light the tealight candle and place the jar upside down over it (the jar is standing on its lid). Students can predict that the flame will extinguish soon, and they are correct. But what will happen if you lift the jar with the burning candle inside and then drop it?

Will the flame burn brighter, remain the same, or become less bright? And why do they expect that? Take some time to gather predictions and corresponding explanations. Then try it.

### Observe
Remove the jar from the lid when the flame is extinguished. Relight the tealight candle and let a strong flame develop. Place the jar back on the lid, hold it high in front of you, and drop it. A drop of about half a meter is sufficient, then catch it again.

The flame has been extinguished much faster than before, so not because there is insufficient oxygen. Why then?

```{tip}
* To drop the jar vertically, hold it on both sides and move your hands with the fall. Guide the observation because it happens quickly: 'Pay close attention, does the candle go out when catching it or even before?' If necessary, let the jar drop further, onto a cushion or into sand.
* The question of why the tealight candle goes out can be broken down into pieces, for example, as follows:
    * A flame needs oxygen to burn. What process provides oxygen?
    * Hot air 'rises'. What process causes that?
    * Does a cork float in water when both the cork and the water are in free fall? Does a stone sink then? How big is the upward force during free fall?
```

### Explain
Discuss which of the predictions have come true. Evaluate the given explanations. Explain, if necessary, what physicists think about it.


## Physics background
For the flame to burn, a continuous supply of oxygen is required. If this supply is cut off, the combustion stops immediately. When the tealight candle is on the table, the air near the flame heats up, expands, and its density decreases, causing it to rise. The pressure around the flame decreases, and nearby oxygen-rich air is pushed towards the flame by the surrounding atmosphere.

The net force on the hot air is the difference between its weight and the upward force caused by surrounding cold air. This upward force is equal to the weight of an equal volume of surrounding cold air. In short, hot air floats on cold air due to the difference in weight (per unit volume) between hot and cold air.

In free fall, the weight of both is zero, and thus the difference in weight is also zero. Convection of hot air stops, so does the supply of oxygen.

## Follow-up
Will the tealight candle also be extinguished if you don't drop the jar but throw it a bit (straight up) and catch it before it comes down? Does a flame tilt when going around a corner, and if so, in which direction (towards the inside or the outside of the curve)? How does a flame react to deceleration and acceleration?

## References
```{bibliography}
:filter: docname in docnames
```

