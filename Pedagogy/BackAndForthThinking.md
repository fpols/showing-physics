# Back-and-forth thinking at demonstrations
In natural sciences, describing and explaining phenomena is central. Describing and explaining develops from everyday language to professional language, from an arrangement of concepts into a theory or model. From these, hypotheses and predictions emerge that we test by going back to the phenomenon. Our 'improved' theoretical understanding then make us see more than we did the first time. New observations raise new questions, and so understanding of the phenomenon develops in a process of observation, reflection/questioning, and experimentation. This iterative and dynamic process of back-and-forth thinking between empiricism and theory at each step of the process is a more accurate description of science than the often described "linear scientific method" from research question to experiment to conclusions.

To properly define back-and-forth thinking, we distinguish between the domain of phenomena, objects and observations ('*hands-on*') and the domain of concepts and
ideas ('*minds-on*'), see {numref}`Figure {number} <fig:Domain_explainer>`.

```{figure} Figures/Domain_explainer.png
---
width: 50%
name: fig:Domain_explainer
---
In the domain of objects and observations, we observe and measure. In the domain of concepts and ideas, we try to reason with theories in their interrelationships. Between those two domains we can think back-and-forth with activities in four categories.
```

A back-and-forth thinking activity is an activity in which you use and link a hands-on aspect and a minds-on aspect in one reasoning session. As an example, we look at a demonstration in which the teacher creates a liquid column of "stacked" liquids of different densities [LINK], in which some objects are at a boundary plane, see {numref}`Figure {number} <fig:stacked_liquids>`. In the domain of *phenomena, objects and observations* are observations such as the stratification and precise location of objects. In the domain of *concepts and ideas* are the underlying concepts such as volume, mass, density and buoyancy. When you formulate an argument like "the object is at the interface of water and oil so the density of the object will be between that of oil and water," you are involved in back-and-forth thinking (BFT).

```{figure} Figures/stacked_liquids.jpg
---
width: 50%
name: fig:stacked_liquids
---
Liquid column of 'stacked' liquids
```

Back-and-forth thinking is shown schematically in {numref}`Figure {number} <fig:Domain_explainer>`. On the left is the world of phenomena in which we observe and measure and on the right is the world of concepts and ideas in which we reason with concepts and models. The connections are the "back-and-forth" activities, organized into four categories of *explaining*, *concluding*, *predicting* and *designing*. In explaining and concluding, we move from phenomena to theory. In *predicting*, the direction is reversed and we use theory to make statements about (possible) phenomena and measurements. We *design* experiments to test those predictions. Those concepts and ideas can be both macroscopic (temperature, energy, mass, density, concentration) and microscopic (particles, atoms, molecules, electrons). Making observations and learning new ideas, can produce wonder and curiosity, constantly creating new questions that we must answer in both domains. For example, a demonstration with possibilities in all categories is 'Force and motion with a bowling ball' [LINK].

The learning objectives of practicals and demonstrations usually require back-and-forth thinking, whereas a practical often focuses on actions and phenomena. It is therefore a didactic challenge to connect the world of phenomena with the world of concepts and models. Demonstrations are well suited to achieve this connection because the teacher is (much) more skilled in the hands-on aspects and will no longer be distracted by the pure phenomenon. A good example of this is 'Speed of light in a liquid' [LINK].

In *explaining*, students search for a natural science explanation for an observation or describe that observation using jargon. Instead of having students formulate their own explanation, concept cartoons provide an opportunity for students to select an explanation. Demonstrations in which explaining plays a major role include 'Lighting and blowing out an LED' [LINK], 'Cooking by cooling' [LINK], 'Shadow of a flame' [LINK] and 'A fountain in a flask' [LINK].

*Concluding* is about finding a general rule or finding some value that cannot be measured directly, such as the gravitational acceleration. Such a general rule can be a mathematical relationship. Statements about a property of an object or substance are also part of inference, such as whether or not an electric current conducts. For a conclusion to be complete, it must also be substantiated, which requires additional back-and-forth thinking, as becomes clear, for example, in 'Measuring 'stars' [LINK]. Thinking about the accuracy of a determination is done in [Determining g](../demos/demo73/demo73.ipynb) using a concept cartoon. Other demonstrations focusing on inference include 'Physics of the pan flute' [LINK], [Standing waves with an electric toothbrush](../demos/demo77/demo77.md) and 'Shoebox beamer' [LINK].

*Prediction* involves predictions of observations based on theory, not on a guess, and with substantiation. That substantiation can come from legitimate physics prior knowledge, but also from popular misconceptions based on "alternative theories." Prediction combines well with explanation. Demonstrations in which this happens include 'Flames on the turntable' [LINK] and 'Strange bike lights?' [LINK]. Other demonstrations in which redicting plays a central role plays, are, for example, [Up and down the hill: Accelerating along a Slope](../demos/demo69/demo69.md), [Cooling of metal spheres](../demos/demo75/demo75.md) and 'Kirchhoff' [LINK].

*Designing* includes designing an experiment as part of research, as well as engineering design. Although design is not usually the focus of demonstrations, aspects of it can be addressed with a question such as "*If I want to achieve ..., what should I change about the setup?*". This plays out in [Lorentz force on charged particles](../demos/demo84/demo84.md), 'Crookes' Radiometer' [LINK] and 'Juggling mirrors' [LINK].

Once students link theories and concepts to observable phenomena, new questions arise that provide an opportunity to further stimulate back-and-forth thinking. Sometimes this can already be done by challenging students to formulate the question using previously learned jargon, which highlights the essential importance of proper language. As an example, take a demonstration in which a large and a small stone are released and hit the ground at the same time. Would it make any difference if you took a very small stone? Do we get the same perception if they fall from a greater height? What happens if you attach a feather to one of the stones? If students are challenged to formulate those questions with concepts then they should start using terms like air friction, gravity and possibly inertia. Correct jargon is essential because you cannot arrive at an answerable question without it.

```{figure} Figures/combi_schakeling.png
---
width: 50%
name: fig:combi_schakeling
---
Mixed circuit
```

Often several didactic routes are possible for students to do back-and-forth thinking. The teacher chooses in advance which back-and-forth thinking -tasks the students will complete and which ones will not be covered this time. As a example, we will look at a PEOE demonstration of an electrical mixed circuit, see {numref}`Figure {number} <fig:combi_schakeling>`. 

The teacher chooses to focus on the concepts of (replacement) resistance and current flow via *predicting* and *explaining*. The starting question is *What happens to the brightness of bulbs **B** and **C** when you unscrew bulb **A**?* You can help students through multiple-choice options. After a brief exchange of answers and arguments, the teacher unscrews bulb **A**. Bulb **B** and **C** appear to turn off. The explanation is sought in class and linked to the concepts resistance (becomes very large) and current (becomes zero). You now tighten bulb **A** again and ask what happens to the brightness of bulbs **A** and **C** when you unscrew bulb **B**. The students must now formulate their own answer with justification. Again there is an exchange of ideas before you unscrew bulb **B**. Then you the assignment in pairs to think of reasons why the previously given incorrect explanations (which were undoubtedly there) could be incorrect in order to ultimately find the correct explanation. This version of the explanation task produces strong back-and-forth thinking. weather thinking. Finally, make sure that you relate the concepts of (replacement) resistance and current correctly to the explanation. 

With a different approach and through other back-and-forth thinking activities, other other concepts be developed. For example, drawing the circuit diagram based on clearly visible wires (use a camera!). Then students think back and forth between the abstract representation of the circuit diagram and the concrete built circuit. Yet another way is to ask students what needs to be changed about the circuit would have to change in order for all three bulbs to burn at the appropriate power lit without adding an extra bulb, a design activity. Possibilities abound. Our job as teachers is to make an appropriate selection from these to best develop conceptual understanding as possible!

> **How do we bring back-and-forth thinking into the didactics of demonstrations**
> 
> This can be done in solid **preparation** of the demonstration:
> 1. List the observable phenomena in the demonstration.
> 2. List the concepts associated with the phenomena. Which concepts fit the learning
     objective? <br>
     It works well to do steps 1 and 2 while you are setting up yourself.
> 3. Which phenomena only distract and create noise? On the contrary, which
     concepts do you want to avoid to avoid noise?
> 4. Devise a roadmap for the demonstration with explicit attention to the back-andforth thinking questions you will ask and the assignments you will give. <br> <br>
> See 'Force and motion with a bowling ball' [LINK] and [Lorentz force on charged particles](../demos/demo84/demo84.md) for elaborated examples of the roadmap.

## References

Berg, E. van den (2012). Natural Science and Engineering: thinking back and forth between concepts
and phenomena, reasoning with concepts and with evidence. NVOX, 2012, 37(4), 176-177. <br>

Naylor, S., Keogh, B. (2010). Concept cartoons in Science Education (revised edition). Millgate House
Publishers, United Kingdom. IN REFERENCES.BIB<br>

Osborne, J. (2011). Science teaching methods: a rationale for practices. School Science Review, December
2011, 93(343), 93-103. IN REFERENCES.BIB <br>

Spaan, W. Oostdam, R., Schuitema, J. & Pijls, M. (2022) Analyzing teacher behavior in synthesizing
hands-on and minds-on during practical work, Research in Science & Technological Education, DOI:
10.1080/02635143.2022.2098265 IN REFERENCES.BIB
