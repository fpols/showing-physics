# Cappuccino and specific heat versus heat of vaporization


Author:     Frits Hidden, Jorn Boomsma, Anton Schins, Ed van den Berg\
Time:	  	10 - 15 minutes, more if student computation work is integrated in the demo.\
Age group:	14 - 18\
Concepts:	Heat of vaporization ($L_v$) and condensation, specific heat, applying $Q = c \cdot m \cdot ΔT$ and $Q = m \cdot L \cdot v$

This demonstration has been published by the authors in {cite:t}`hidden2012`.

## Introduction
A cup of cappuccino is prepared by adding about 50 mL frothing, foaming milk to a cup of espresso. Whole milk is best for foaming and the ideal milk temperature when adding it to the espresso is 65°C. The espresso itself may be warmer than that. During the heating the milk should not burn as that would spoil the taste. The best way is to heat the milk slowly while stirring the milk to froth and create foam. Modern cappuccino machines in restaurants on the other hand do not have time for slow heating. Could we heat the milk by just adding hot water?

```{figure} demo02_figure2.JPG
---
width: 50%
align: center
---
A cappuccino
```

This is the question we pose to our high school students first. How many mL of 90 $^o$C hot water would be needed to heat 50 mL of milk from refrigerator temperature (say 4 $^o$C) to 65 $^o$C? Assume that the specific heat of milk is the same as the specific heat of water. Students answer the question on a worksheet and practise their computation skills. The answer: 122 g. This would mean an unacceptable dilution of the milk, 2.5 mL of water for every mL of milk. What would the answer be if we use boiling hot water of 100 $^o$C? Students calculate again, the answer is 87 g, still an unacceptable dilution. What then? What if we use steam?

## Equipment
* A large erlenmeyer flask
* Measurement cylinder of van 100 mL
* Thermometer
* Cork or rubber stopper with hole 
* Rubber tube
* Water
* Burner
* Heat resistant gloves (to handle the hot rubber tube) or tongs
* Access to refrigerator for cold water or milk

## Preparation
Fill the Erlenmeyer with water, preferably boiling water to prevent waiting by students. Connect the rubber tube with the stopper. Fill the measuring cylinder with about 50 ml water from the refrigerator (5 ml). This water functions as “milk”. Using real milk is fine too. 

## Procedure
The first part of the execution consists of computations. 

A cappuccino is made by ...... (here follows the text of the introduction but **without the information that a cappuccino machine uses steam**)...... but what can you do if there is no time to heat the milk slowly like in a restaurant or coffee shop? A cappuccino machine adds “hot water” to heat the milk. The maximum dilution that is acceptable for the taste is 10%. Such a small dilution does not matter as expresso is strong coffee with a small amount of water. 

> *Calculate on your worksheet how much water of 90 $^o$C has to be added to 50 ml milk of 5$^o$C to heat it to a temperature of 65$^o$C. Assume that the specific heat of milk equals that of water. The formulas and constants you need are on the worksheet (or on the board). (Answer 120 g).*

> *That (120 ml) is more than 2x as much as the milk (50 ml). Suppose we use water of 100 °C, how much do we need then? (Answer 86 g).*

> *That is still more than 1.5 times more water than milk!*

> *Suppose that water would not evaporate at 100$^o$C. How high should be the temperature of 5 g water to be added to 50 g milk of 5 $^o$C such that the final temperature of water and milk would become 65$^o$C? Just use the same formulas. (Answer: 665$^o$C).*

> *That is very high, 665 $^o$C, that is not going to work. Let’s now look at what happens if we use steam, which is what we get when we heat water above 100$^o$C.*

```{figure} demo02_figure1.jpg
---
width: 50%
align: center
---
some caption
```

The teacher explains the set-up and asks a student to come to the front to measure the volume of “milk” and its temperature (should be about 5$^o$C). 

Start the experiment. Light the burner. First get the pre-heated water in the Erlenmeyer to boil. Wait until steam comes out of the rubber tube. Then put the end of the rubber tube into the “milk”. The student measurer reads the temperature every 10 seconds. Another student does general observations and is asked to look for bubbles. At about 62 $^o$C the burner is removed. At about 65$^o$C the tube is removed using insulation gloves or a biceps. Do not wait too long removing the tube because after turning off the burner the remaining steam will condense and reverse the flow in the tube (suction). The observer student measures the final temperature (about 65$^o$C) and estimates how much water was added to the “milk” (about 7 g).

> *The milk is now 65$^o$C and we only needed about 7 g of steam! Not 86 g!  And we did not heat up to 665$^o$C either. How is that possible, where did the energy come from?*

## Physics background
The experiment clearly shows that much less steam of 100 $^o$C is needed than water of 100$^o$C. When steam condenses to water an enormous amount of energy is released: 2256 Joules per gram steam. This energy is used to heat up the milk.  This is comparable to the heat released bij cooling 665$^o$C water to 65$^o$C if the water would still be liquid rather than steam. A cappuccino machine uses this heat of vaporization to heat up the milk, a clever trick. That lots of energy is released is also clear from the noise. Furthermore, the machine simultaneously froths the milk. Have a nice cappuccino!

```{tip}
See the comment on withdrawing the tube timely from the milk before suction appears. 
A worksheet is available on the site. 
It is possible to do this experiment as student practical work, but watch out with the steam. 
```

```{warning}
Steam can cause bad burns. So watch out with the steam coming through the rubber tube, use insulating gloves or use tongs. 
```
```{admonition} Worksheet
Werkblad cappuccinoproef
De volgende formules kunnen je helpen met de opgaven op dit werkblad.
Algemene formules: Q = m ∙ cw ∙ ∆T en Q = L.m
In dit experiment: Qop(genomen door de melk) = Qaf(gestaan door het water)
50 g ∙ 4,2 J/g °C ∙ (65 °C – 5 °C) = mheet ∙ 4,2 J/g °C ∙ (Theet – 65 °C)

ρwater ≈ 1,0 g/ml ; cmelk ≈ cwater = 4,18.103 J/g °C ; L = 2,26 . 106 J/kg (verdampingswarmte)
[Tim, het symbool ρ is de Griekse letter rho).

Poging 1: water van 90 °C (Theet is 90 °C)
Bereken hoeveel gram water van 90° C we bij 50 g ‘melk’ (hier ook water) van 5° C moeten voegen opdat temperatuur van de ‘melk’ 65 °C wordt.
Antwoord: 

Poging 2: water van 100 °C (Theet is 100 °C)
Bereken hoeveel gram water van 100 °C we bij 50 g ‘melk’ van 5 °C moeten voegen opdat de temperatuur van de ‘melk’ 65° C wordt.
Antwoord: 

Poging 3: heel heet water als het geen stoom zou worden
Bereken Theet als we besluiten om 5 g heel heet water-niet-stoom bij 50 g ‘melk’ van 5 °C voegen opdat de temperatuur van de ‘melk’ 65 °C wordt (mheet is dus 5 g).
Antwoord: 
 
Poging 4: stoom van 100 °C
Bereken hoeveel gram stoom van 100 °C we bij 50 g ‘melk’ van 5 °C moeten voegen opdat de temperatuur van de ‘melk’ 65 °C wordt.
Antwoord: 


```

## References
```{bibliography}
:filter: docname in docnames
```
