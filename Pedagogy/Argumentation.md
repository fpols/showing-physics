# Argumentation 
## Development of inquiry skills through demonstrations
Conducting scientific research is a matter of making targeted and careful observations, using your knowledge to understand them, and vice versa, adjusting your knowledge and determining with which subsequent observations you can test and substantiate claims. Demonstrations are an excellent opportunity to practice this with students. Examples of the many important questions that need to be addressed during inquiry are:
* What can I conclude from these observations? 
* How confident am I about this, can the conclusion be strengthened? 
* If I change something, what will happen? 
* And if the outcome  changes or not, what will I learn from that? 

This back-and-forth thinking between practice and theory characterizes research (Van den Berg, 2012). *In demonstrations, you can stimulate that thinking, guide it, and help improve it in a pleasant, motivating and safe way.* As always, the art here is in maintaining the balance between challenging students, providing them with the autonomy to think and do for themselves on the one hand, and supporting, scaffolding, encouraging and guiding their thought and action on the other. Allow  students to choose and  do by themselves what they are already capable of. But as a teacher provide examples and support  with what is still too difficult. Select the learning objectives for your demonstration where they show cognitive readiness. There should not be too many objectives, and these should be achievable by the students with some effort. Provide  your students with a route using guiding questions and assignments, but also with opportunities for their own input and personal contributions.

Learning physics requires doing physics. Is it possible for  students to do so by participating in demonstrations? After all, in demonstrations scientific problems are not actually solved nor do students  manipulate the materials by themselves... They are meant to suggest to students  that with a little effort they can eventually understand the surprising phenomena - a guarantee that is not provided in real physics research. As a teacher you  know the outcome and conclusion in advance, all you have to do  as a teacher (difficult as that may be) is to produce the intended observations. It suffices to then present the accepted corresponding explanation, basically confirming what your students already (ought to) know. While this is a viable approach, it need not stop there. In your demonstration you may use the opportunity to  contribute to the development of inquiry skills which is not difficult at all.

To that end, make the research problems manageable before  confronting your students with them. Reenact the solving of those problems, as it were, during your demonstration, because there is a lot to learn from that. The easiest way to do this is to ask your students questions like:

* What do you think will happen?
* What do you know about the set up?
* What exactly did you see? What did you notice?
* How would you describe your observations as accurately as possible?
* Was what happened what you expected to happen?
* If we would repeat the demonstration, what would you pay special attention to?
* How can we improve our observations?
* Can you explain what happened and why it happened?
* Did we learn something from what was observed? How does that fit with what we already know? What remains unknown for now and how can we find out?

By finding the answers together, valuing them, testing them where possible, students practice the skills they need when they "learn physics" and "do physics" on their own later on. The demonstrations in Part XXXX of this book focus explicitly on these skills, but  in principle all the other demonstrations are suitable as well. Of course, you will have to adapt them to your own needs and include  your own ideas. Furthermore, you would not ask all of the questions suggested above in a single  demo as that would be far too strenuous for both students and teacher. Choose a clear, well-defined learning goal and give it  some extra attention using  a few appropriate questions. Examples follow below.

We  distinguish three levels of difficulty that  depending on what learning we aim for. In the simplest demonstration,  a surprising phenomenon is shown and your students practice scientifically "observing" and "interpreting". At the second level, observations are not merely interpreted explicitly, a conclusion is drawn from that interpretation and substantiated through  arguments based on the observations. The third level demonstration involves skills that address the limits of the knowledge found. Guiding questions are, for example, *What assumptions have been made?’ Or: ‘In what situation is the conclusion no longer valid?” Part of exploring  the limits to the knowledge found knowledge is also a consideration of its extension beyond those limits. Can the conclusion be generalized? Does the conclusion raise new questions, and how can they be answered?* 
In inquiry beyond these three levels  students need all their investigative skills combined.  As a teacher, you may realize that you no longer have all answers available. After all, at this most beautiful but most difficult level, students engage in real physics research.

## Argumentation
In scientific research, an answer to the research question does not merely suffice. It should be an as scientifically convincing as possible answer. In the classroom setting, then, you have at least two different challenges. *First*, how do you get students to give the most convincing answer? And *second*, how do you teach what counts as convincing in the natural sciences, and more specifically in physics?<br>
'Argumentation' is here understood as the *production and critical evaluation* of the most convincing possible answer to a scientific research question. There are many examples of practicals that focus on *enabling students to do inquiry* and contribute to the development of the above understandings. However, demonstrations often focus on showing connections and visualizing concepts. But these same demonstrations can be focused - if you frame them a little differently - on understanding evidence and the need to convince yourself and others that you have arrived at the best possible explanation for what you observe.

## Example: the most convincing conclusion
With Grade 10 students, you jointly measured on one setup to determine the relationship
between the length and resistance of a constantan wire. You ask your students to use the
values found (wire lengths *l* and the determined resistance) to write down a conclusion
about the relationship between length *l* and resistance *R* of the wire. Some typical
answers (apart from unusable statements) are listed below, numbered 1 to 4. In this
activity, you order them from *weak* to *stronger*. First the weakest:

1. As the length changes, so does the resistance.

Ask your students if they think this is a proper conclusion, why or why not, and if they can think of an even better conclusion. When thinking about the quality of the conclusion, a context may help. For example: the wire is part of the circuit in a device that breaks when too much current flows. The research task: determine the length of wire needed for a maximum but safe current of $X$ amperes. 

In this context, you make the question about the quality of the conclusion tangible. The answer helps create a device that works well.

Present student answers one at a time in increasing quality, always using the same questions as the first answer.

Typical examples:
2. As the length increases, so does the resistance.
3. If the length increases twice as much, the resistance also increases twice as    much.
4. The resistance is directly proportional to the length where $R = 14.5 *l$.
   Note that you can't do much with answers 1-3. The research was done for nothing.
   We can make answer 4 even more convincing by presenting the corresponding
   evidence (e.g., a graph).

Without a doubt, students can identify the fourth answer as the best. But what *makes* it the best? What are the characteristics of the best answer? What answer is helpful in determining exactly how long the wire should be in the fictional device? Only when they have answers to those questions has what they should remember from this activity been accomplished. With a little coaching, these students can figure out that an answer is better if it:
* contains more information ('increase' is more informative than 'change')
* gives more detail ('twice as large' is more detailed than 'increase')
* contains numbers in addition to words (only when you know the ratio can you calculate the length).

A nice final conclusion to this activity could be: *the answer to the research question should be consistent with the measurements and be as informative, complete and useful as possible*. The concepts of 'informative, complete and useful' are concrete and clear to the H4 students. It is a practical stepping stone towards the overarching, but abstract concepts of *validity* and *reliability* (of measurement results, interpretations and conclusions).

### 'Rebuilding a demonstration'
Argumentation is not only important in drawing conclusions; it plays a central role in *every* phase of an investigation. With some examples, we show how to easily supplement the demonstrations in this book for "learning to argue" in other phases of research. We do this first based on the demonstration 'Hill up...hill down' from this book and then with another demonstration after Pols et al. (2022). These practical examples can later be used at one's discretion to convert other demonstrations.

``` {figure} Figures/wheelvelocity.JPG
---
width: 70%
align: center
---
In the (v,t) diagram, the direction coefficient of upward motion is different from that of downward motion. 
```

Demonstration "Hill up...hill down" focuses on the idea that the acceleration of an object at its highest point is not 0. So the focus is on developing conceptual knowledge. How can we focus this same demonstration on argumentation?

If you look closely at the (v,t) diagram, you can see that the directional coefficient of the graph is not constant. When the trolley drives up, the acceleration seems greater than when it drives down. Is that a measurement error? Is the friction different? Is there some physical phenomenon that provides a possible explanation? The question then becomes, what can we do to investigate this? For example, turn the cart around and see if the graph remains the same. Should we perhaps look at other quantities (a,t) to get a better view of what exactly is happening? And if you have figured out that the direction of the frictional force reverses and that therefore causes there to be a kink in the (v,t) graph, how do you convince others of this? For example, what graph would you present?

What is important is that in a demonstration, you distinguish the different goals and then focus on one goal. Keep it manageable for your students and yourself, focus on one or two aspects of argumentation, and go into other parts next time. You don't have to cover everything in a real investigation all at once.

### The swinging pirate
Students first watch an exciting clip from a pirate movie in which the hero swings from one ship to another on a long rope, while swords flash and explosions go off everywhere. The assignment: design a new stunt for such a movie, in which the hero lands on the second ship at exactly the right moment. To do this, do research using a model: a cube hanging from a string. The first step in the research is an orientation to the situation and determining the research question.

*Orientation: What needs to be researched?* 

Translating the pirate's movement to a block on a string is not that difficult. Based on the context and knowledge about swings, students can create initial research questions related to a potential factor of influence on "swing time" (the time it takes the pirate to
reach the other side).

With the research questions they come up with, you can do exactly as you did with the conclusions about the constantan thread. Supplement what they come up with with your own ideas if necessary, organize them, show them again one at a time, and always ask questions about them before moving on to the next one: <br>
For a cube swinging back and forth on a string is the research question:
1. How fast does the cube swing?
2. Does the swing time change when the mass of the cube changes?
3. Does the swing time increase as the mass of the cube increases?
4. Is the pendulum time proportional to the mass of the cube?
5. What formula or graph can you use to find the pendulum time if you know the
   mass of the cube?

Related questions: *Now is this a good research question? Why or why not? Can you think of an even better one? Does the stunt person benefit from answering questions 1-4?* 

Together, trace what you recognize as a good research question. Again, you can aim for a nice final conclusion: physicists in their research often try to establish a *relationship* (or lack thereof) between an independent and dependent variable. This is a fundamental insight. If you can't figure out what to measure , you usually can't even begin your research!

*Controlling variables and measuring them fairly* 

Once a good research question is available, then you can start thinking about how you will make the measurements. For the pendulum test, students in grade 4 usually do figure out  that in addition to mass, the pendulum time may depend on the initial angle and length.

Before you then start measuring, you can ask students: how are we going to do this? Make that question concrete, for example as follows: *I have prepared tables to fill in the readings. Which do you think is best, which is worst, and why?*

``` {figure} Figures/VariableTabels.png
---
width: 80%
align: center
---
Tables at 'controlling variables' 
```

With the tables shown, you can get students thinking about the interval between measurements, the range in the independent variable(s), and the concept of fair measurement. In this way, you get them thinking in advance about the consequences of their choices, and their impact on the quality of the answer yet to be found. You help them take a step they very easily skip in independent work. After the discussion, the class can prepare their own tables that will be used in the demonstration, and think together about why they are satisfactory. 

Another option is to use an awkward choice (e.g., Table 1.4) in your demonstration and hold the discussion only after measurement. In this particular example, focus rather on the relationship of the length to the pendulum time, since the mass has no systematic influence and this unnecessarily complicates the discussion.

Then ask the class to draw a conclusion, and to check carefully whether that conclusion can be defended. Make that  checking carefully' concrete, with questions such as:
* Could the change also have been caused by something else? 
* Do you now also know what happens if you increase the length two more times? 
* How sure are you of the shape of the graph between measurements?*

Most students will only put time and energy into coming up with correct methods when they see what all that effort is good for. You can tell them that, but we think it is more instructive if students think about it themselves and  try to give their own reasons in an activating approach. In practicals, that quickly leads to cumbersomeness, but in demonstrations you can do it quite smoothly and collaboratively. These correct methods are of course not immediately ready and applicable, but fit into a learning line for 'learning to investigate'.

*Interpreting measurements* 

The influence of mass and (small) initial angle on period is often not explored in pendulum test cookbook practicals and demonstrations. A missed opportunity! Just when students expect a connection that is not there (according to theory) it becomes interpreting the data a real problem that requires a solution. After all, *while* there will be fluctuation in measurements due to measurement errors, does those now something, yes or no? What causes the fluctuations: the measurement method, outside influences, or a possible relationship between the quantities? Because students are often inclined to analyze and present their data in such a way as to make the connection they expect to confirm, you can do some work on this. Feel free to let them help with implementation and re-measure a few times: *Every time something different comes out of your measurement, couldn't that be better?* Or: *if we now repeat the measurement a few times and take the average, does that help?* And: *you measured really well, and you see something different than what you expected. Which is more convincing, what you see or what you expected?* Thus, you work on argumentation by focusing on reasons for repeating measurements, minimizing measurement error, and reporting dispersion.

If you need a definitive answer to the question of factor influence, set up two equally long pendulums, give them different masses (or small initial angles) and release them simultaneously; with careful preparation, they will keep swinging nicely side by side. But especially don't do that at the beginning if you want your students to learn to investigate. Also, exploit the power of demonstrations in which nothing actually happens!

*Representing measurements*

``` {figure} Figures/MeasurementGraphs.png
---
width: 50%
align: center
---
Representing measurement
```