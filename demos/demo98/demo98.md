

# Implosion

Author: Ineke Frederik\
Time:	15 minutes\
Age group:	From grade 8 onwards\
Concepts:	pressure, vapour pressure, forces

## Introduction
Here you demonstrate the force that can be exerted by the outside air. As a bit of water just is evaporated in a vessel and is then cooled down, an enormous force acts on the outside resulting in a spectacular implosion. A classic and spectacular demonstration! 


```{figure} demo98_figure1.JPG
---
width: 50%
align: center
---
Heating a can with a layer of water.
```

## Equipment
* Sealable metal container (slightly cleaned oil drum, a large paint can, an empty soft drink can); 
* gas burner large enough to heat the container; 
* room temperature water; 
* wet cloth; 
* cold water (possibly with ice cubes);
* pair of gloves.

## Preparation
Test the demonstration so that you know approximately how long it takes for the can to implode. 

## Procedure
Put a bit of water in the can and heat it. *What happens to the air in the can?* Let the water boil for some time, so that the water vapour can displace the air present. Close the - now hot - container with the cap tightly. Don't burn your hands! Turn off the gas. *What is now in the sealed vessel? What happens when the contents cool down?* Now bring a wet cloth over the vessel. Wet the cloth liberally with cold (ice) water. 

```{figure} demo98_figure2.JPG
---
width: 50%
align: center
---
Crumpled can.
```

```{tip} 
The living room version of this experiment uses a soda can. Now you need a container of cold water and a beaker pliers. When the water in the can is well boiled through, pick it up with the tongs and put it in the cold water with the opening facing down as quickly as possible. The stopper is not needed now. The can crumples.
The teacher can give a bonus to students who can show a video of their own imploding soda can.
```

## Physics background
The vessel is filled with air and a bit of liquid water. Heating the water creates water vapour. If the stopper is open, the vessel is filled with water vapour and the air is 'pushed' out. You close the vessel and cool it down. Now the water vapour condenses. Now, at lower temperatures, air can contain much less water vapour per litre of air - the water vapous thus condensates with the result that the pressure in the sealed vessel drops. The pressure is much lower than the pressure of the outside air and the vessel will be compressed from the outside. With a robust vessel like an oil drum, it can take several minutes for something to happen!

## Follow-up
There are several videos on [YouTube](http://www.youtube.com/watch?v=c5_ho2sc0fc) about this phenomenon.

