

# Chapter title


Author:     \
Time:	  	\
Age group:	14 - 18\
Concepts:	

## Introduction

## Equipment

## Preparation

## Procedure

```{figure} demo42_figure1.JPG
---
width: 50%
align: center
---
some caption
```

## Physics background

## Follow-up

## References
```{bibliography}
:filter: docname in docnames
```