

# Chapter title


Author: Wouter Spaan     \
Time:	  	\
Age group:	14 - 18\
Concepts:	

## Introduction

## Equipment

## Preparation

## Procedure

```{figure} demo49_figure1.jpg
---
width: 50%
align: center
---
some caption
```

## Physics background

## Follow-up

## References
```{bibliography}
:filter: docname in docnames
```