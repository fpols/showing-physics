

# Falling magnet and Newton's Third Law: does the magnet challenge gravity?

Author: Wouter Spaan\
Time:	10-15 minutes, extendable\
Age group: 16+\
Concepts:	Newton's First, Second and Thirs Law; constant velocity

```{figure} demo16_figure1.jpg
---
width: 50%
align: center
---
CAPTION
```

## Introduction
A magnet falling slowly through a copper tube is a well-known demonstration. It is possible to use the same phenomenon as an interesting, surprising and challenging application of Newton’s Third Law. In order to be able to measure with this set-up, the tube is suspended from a force sensor which measures its weight. This demonstration is not suitable to introduce the Third Law. However, it can provide an adequate test of its application with high-level students.

## Equipment
* Copper tube
* Force sensor
* Small strong magnet
* (Copper tube cut open)
* (Stronger magnet)

## Preparation
Build the experimental set-up as pictured in figures 1 and 2.

```{figure} demo16_figure2.jpg
---
width: 50%
align: center
---
CAPTION
```

## Procedure
The students are probably familiar with a magnet falling slowly through the copper tube. If not, make sure to show it in advance. The explanation of the phenomenon is not strictly required for the learning goals mentioned, but your students are probably too curious not to tell them. The next step is to hang the tube from the force sensor and show a measurement of the force for a couple of seconds (without the magnet). It will show the weight of the tube, which equals the gravitational force on the tube.

Next, you ask your students what will happen to the measured force when you drop the magnet in the tube. Ideally all students should get a chance to draw a graph, e.g. by using small white-boards. Instruct them to draw three distinct parts: a) before the magnet has been dropped in the tube, b) while the magnet is falling and c) after the magnet has left the tube. This will result in different graphs. Only after sufficient and thorough discussion do you show the experiment. Then you can ask your students to explain this result using a free body diagram showing all the forces on the magnet and on the tube, while the magnet has a constant velocity. During the closing discussion you can emphasize the importance of the constant speed.

As an important check whether the explanation is clear and students can apply it themselves an additional experiment can be performed with the open tube. In this case the magnet will fall faster than in the uncut tube (but still considerably slower than in free fall). Ask the students to predict whether the increase in force is larger than, equal to or smaller than the increase measured with the uncut tube. Some students will persist in their preconception and you will probably hear comments like: ‘The speed of the magnet is higher and so is the increase in force’ or ‘The magnet is decelerated less, and so is the increase in force’. Once again mother nature provides an unexpected result. Lastly you can compare the measured increase with the weight of the magnet (at rest).

```{tip}
• Take your time to make and explain all drawings (F,t-graphs and force-diagrams).

• This demonstration provides an interesting opportunity to discuss the concept of apparent weightlessness. Is the magnet weightless during its fall?

```

## Physics background
Through the Lorentz force the copper tube will provide an upward force on the magnet, which prevents it from accelerating like in free fall. The magnet in turn provides an equal and downward force on the tube. Therefore, the force required to lift the tube increases with the same amount. It is this force you measure with the sensor. Once the magnet reaches a constant speed (which happens very fast), the increase in force is equal to the gravity on the magnet as the net force on the magnet equals zero.

## Follow-up
With the open tube, the force is less constant than with the closed tube. In the open tube one can see the magnet rotating. Perhaps rotation is less in a closed tube. Why would that be?

## References
```{bibliography}
:filter: docname in docnames
```