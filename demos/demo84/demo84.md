

# Lorentz Force on Charged Particles


Author:     \
Time:	 10 - 15 minutes\
Age group:	12 - 18\
Concepts:	

## Introduction
The Lorentz force on charged particles is more abstract and difficult to visualize than the Lorentz force on a current-carrying wire. In this demonstration, you make the Lorentz force on ions in a solution convincingly visible. The Lorentz force is already known, so students can engage in robust back-and-forth thinking in this situation.

```{figure} demo84_figure1.jpg
---
width: 50%
align: center 
---
Setup with a salt solution in an aquarium tank with two electrodes connected to a DC power source. Two disk magnets are placed under the aquarium. The dye is located directly above the magnets. No current is flowing yet.
```



```{figure} demo84_figure2.jpg
---
width: 50%
align: center 
---
The situation immediately after the current has been switched on for about one second. A force perpendicular to the direction of the current is generated above the magnets, causing a vortex. The magnets have become visible.
```


## Equipment
- Tank with a layer of saline solution about one centimeter deep
- Food coloring
- Voltage source
- 2 long electrodes, which can be made from a copper strip
- 2 disk magnets
- Camera for visibility.

## Preparation
Set up the arrangement without the dye (see figure 1) and let it rest for over a minute. Add the dye just before turning on the power. A camera enhances visibility.

## Procedure
First, explain the setup and ask a question to check if the students understand the setup. In which direction will the current flow shortly? Emphasize that both positive Na+ ions and negative Cl- ions are present in the solution.
This demonstration is not suited for the students to make predictions before they have seen the experiment. So only after the execution, the explanation follows. The result remains visible for a while, allowing students to work on the core question based on the result: _which of these magnets has its north pole facing upwards?_

Answering this question requires a lot of back-and-forth thinking. You perceive the direction of the Lorentz force and know where the positive and negative poles are connected (hands-on domain). You must link this to the direction of a magnetic field using the left hand rule for the Lorentz force and then translate it back to how the magnets are positioned.

Students will likely answer that question based on the direction of the Lorentz force on a current-carrying wire. It is worth discussing what would change if you reason from the Na+ ions (nothing) and what would change if you reason from the Cl- ions (they move opposite to the direction of the current). From experience, it has been found that some students develop the misconception that the negative ions are only influenced by the upper magnet (with the red dye) and the positive ions are only influenced by the lower magnet (with the green dye), and vice versa. It is therefore important to emphasize once again that both the positive and negative ions experience a force simultaneously in the same direction.

Control question: The teacher provides a sketch of the result and asks the students to figure out how to achieve that with these materials. Much back-and-forth thinking is guaranteed here.

## Physics background
```{figure} demo84_figure3.PNG
---
width: 50%
align: center 
---
Force diagram for a positive and a negative ion for the upper magnet from figures 1 and 2. The sequence of reasoning is: direction of $F_L$ is observed, direction of $v$ follows from potential direction, direction of $I$ follows from $v$ and particle charge, combining and applying left hand rule gives direction of $B$ (upward out of the water).
```


```{tip}
A slightly different variant of this demonstration can be found in ShowdeFysica 2, number B31 (page 132).
```
