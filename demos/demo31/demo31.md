

# Blowing out a Light Bulb
Author:     \
Time:	  	15 minutes\
Age group:	12+\
Concepts:	NTC (Negative Temperature Coefficient), series circuit, power, heat generation

## Introduction
The primary goal of this demonstration is to demonstrate the NTC behavior of glass. With this demo, you can discuss concepts from electrical engineering at many levels. You can prompt students to think about heating due to current flow.

## Equipment
* Random light bulb
* 40-60 W light bulb (see tips)
* Two sockets (for mains voltage), preferably one ceramic for fire resistance
* Mains voltage switch
* Plug with grounding
* Gas burner

## Preparation
* Completely break the glass of one of the light bulbs. This works best with a hammer while the light bulb is completely wrapped in a layer of paper. Then remove the filament from the light bulb. Also remove any support for the filament, for example, with a glass saw. The former lamp will then look like in figure 3 (without it glowing).
* Place the modified bulb and the intact bulb in a suitable socket, which is not too high on the rim (due to later heating). Connect these lamps in series with a switch.

```{warning}
 The glass shards are razor-sharp!
```

## Procedure
* Draw the circuit on the board, set up the arrangement, and relate the setup to the circuit. The burner is not visible. Then ask the class if the bulb will light up when the switch is flipped. After the discussion, flip the switch and bring out the burner.
* Carefully heat the glass in the roaring flame: dip it briefly into the flame and then remove it. After heating for a while, the bulb will light up. While continuing to heat in the flame, you can ask the class what will happen if you stop heating and place the fitting with glass down. This usually leads to lively discussion. When that discussion has lasted long enough and has been sufficiently in-depth, remove the glass from the flame. To the amazement of a large part of the class, the bulb remains on, no matter how long you wait.
* Finally, demonstrate how to turn off the light bulb: blowing against the glass cools it down. Eventually, the bulb goes out. It takes effort, so you can pretend to run out of breath just before the piece of glass stops glowing (practice a few times). If you stop blowing at that moment and the power of the bulb is well chosen, the piece of glass will heat up again, and the bulb will slowly turn on again.

```{figure} demo31_figure3.tiff
---
width: 50%
align: center
---
Figure 1. Circuit diagram of the setup. The resistor consists of a piece of glass (see figure 3).
```

```{figure} demo31_figure1.jpg
---
width: 50%
align: center
---
Figure 2. If you (carefully!) heat the piece of glass, the resistance value decreases and the light bulb lights up.
```

```{figure} demo31_figure2.jpg
---
width: 50%
align: center
---
Figure 3. The piece of glass continues to conduct even after it is removed from the flame. By blowing against this glass, you can cool it down and turn off the light bulb.
```


## Physics background
Glass behaves like an NTC because, in terms of electron structure, it somewhat resembles a semiconductor. At room temperature, the conduction band is empty, and the band gap is too large for the electrons to cross. When heated, the electrons gain enough thermal energy to cross the band gap, and the glass conducts better and better. The ions in the glass play no role in the conduction.

```{tip}
* The power of the light bulb relative to the exact properties and dimensions of the piece of glass is critical: if the power is too low, the current is too small to keep the glass warm. If the power is too high, you cannot blow out the lamp. So, try it out properly. The author had to use lamps of $40$ W, $60$ W, and even $75$ W for the desired result in different versions of this demonstration. This also provides an opportunity to discuss with the students the effect of the power of the burning lamp.
* If you find a well-working combination, stock up on light bulbs (exactly the same); in a few years, they may no longer be available.
```
```{warning}
**Safety and environment**: Make sure to wear safety glasses (the same for the front row of students or place a screen)
```

## References
```{bibliography}
:filter: docname in docnames
```