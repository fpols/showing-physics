

# Bouncing Ball in a Tube

Author: Norbert van Veen\
Time:	30 minutes\
Age group:	all\
Concepts:	Frequency, amplitude

## Introduction
A demonstration likely to be suitable for both lower and upper-level students. In lower levels, it helps to illustrate sound as vibration and explains concepts like frequency and amplitude. In upper levels, it serves as an analogy for the photoelectric effect, making quantities like threshold energy, kinetic energy, and cutoff frequency tangible.

```{figure} demo83_figure1.PNG
---
width: 50%
align: center 
---
Bouncing ball in a tube above a JBL speaker.
```

## Equipment
* Bead or ping pong ball
* Transparent tube
* Speaker (e.g., a JBL variant)
* Tone generator (e.g. phyphox app)

## Preparation
Place the transparent tube over the speaker and connect a sound source to the speaker. Ensure that the tube does not make contact with the speaker's cone.
For lower levels, choose a song that starts calmly and where the bass gradually builds up. Then, use the tone generator with a frequency that results in a nice bounce of the ball.
In preparation for upper levels (analogy with the photoelectric effect), you need to set multiple frequencies. Note at which sound intensity the ball bounces fairly high for a frequency of 100 Hz.

## Procedure
### Lower level - Sound is a vibration with frequency and amplitude.
1. Place the ball in the transparent tube on the speaker. Play music. Let the students observe.
2. Question: *What causes the ball to bounce?*
3. Play a pure frequency through the speaker. Observe the ball in the tube. Change the frequency higher or lower and have students note differences and similarities.
4. Choose a frequency where the ball bounces well and increase or decrease the volume (sound intensity) of the speaker. Have the students predict what they will see.
5. Control question: What happens to the ball if the speaker vibrates at a very high (inaudible) frequency?

### Higher level - Photoelectric effect analogy
1. Choose several frequencies (at a constant volume) and mark the maximum height the ball or bead reaches.
2. Record the measurements in a table and plot the graph in a diagram with height on the vertical axis and frequency on the horizontal axis.
3. Discuss the analogy with the photoelectric effect. See the physics background.
4. Control question: Why doesn't the analogy hold if the sound intensity is not constant per frequency?


## Physics background
### Lower level 
The speaker vibrates and transfers this vibration to the ball, causing it to move up and down. The amplitude of the sound determines how vigorously the speaker cone vibrates, and thus the sound intensity, which is reflected in how high the ball averages.
The frequency is how often the cone vibrates and determines the pitch of the sound; the ball will move faster up and down at higher tones.

### Higher level
{cite:t}`barretto2022physical` conceived and executed this experiment and also clearly described the analogy with the photoelectric effect. The bouncing ball is in a bound state in the tube, just like conduction electrons in a metal. At higher frequencies, the ball will bounce higher. Thus, the height is a measure of the kinetic energy of the ball. The sound frequency is analogous to the light frequency (the absorbed energy), and the sound intensity is analogous to the intensity of the light (quantity of photons). The analogy does not hold when using low-frequency sounds with high amplitude. In that case, the ball could still 'escape'. With the photoelectric effect, no current can be generated below the cutoff frequency. The measured data provide an analogous graph to the graph of kinetic energy against frequency of the photoelectric effect (see figure 2).

```{figure} demo83_figure2.PNG
---
width: 50%
align: center 
---
Diagram according to {cite:t}`barretto2022physical`: an analogous graph to the photoelectric effect. Point out to the students the intercepts on the axes and the analogous intercepts on the diagram of the photoelectric effect. Also, indicate where the analogy no longer holds.
```

```{tip}
- Pay attention to the ratio of bead/ball to tube diameter. The tube should be reasonably narrow so that the bead or ball jumps as vertically as possible.
- It's an analogy with the photoelectric effect, but note where the analogy deviates from the concept.
```

## References
```{bibliography}
:filter: docname in docnames
```