

# Chapter title


Author: Freek Pols    \
Time:	  	\
Age group:	14 - 18\
Concepts:	

## Introduction

## Equipment

## Preparation

## Procedure

```{figure} demo45_figure1.JPG
---
width: 50%
align: center
---
some caption
```

## Physics background

## Follow-up

## References
```{bibliography}
:filter: docname in docnames
```