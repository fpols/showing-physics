

# Black and white: Differences between heating and cooling


Author: Norbert van Veen    \
Time:	15 - 30 minutes  	\
Age group:	Lower & Upper secondary \
Concepts:	Heat, radiation, absorption

```{figure} demo99_figure1.jpeg
---
width: 50%
align: center
---
The white and black test tube in the holder both heated by the light bulb. 
```

## Introduction
A simple demonstration of heating up and cooling down a black and a white test tube. When heating up, students will mostly give a good prediction. When cooling down, however, they will often be guided by the statement "heating up faster will also cool down faster". However, this statement does not hold true. 

## Equipment
* White and black test tube in a holder (e.g. surrounded with white and black paper); 
* a light bulb (> 40 W) or a high-power halogen spotlight; 
* 2 temperature sensors; CoachLab and computer;
* two rubber stoppers with hole; kettle; measuring cylinder to measure 20 mL of water.

```{figure} demo99_figure2.jpeg
---
width: 50%
align: center
---
The test tubes filled with hot water and temperature sensors connected to a CoachLabII+
```
## Preparation
Part 1: Place the light bulb about 20 cm away in front of the test tubes.
Insert the temperature sensors through the rubber stoppers and place them in the test tubes. Connect the temperature sensors to a Coach interface. Set a 10-minute measurement in Coach 7. 

Part 2: Heat water in a kettle. Pour the hot water into the measuring cylinder. Pour 20 mL of water into each test tube. Put the temperature sensors in the test tubes.

## Procedure
**Part 1:**
1.	Give an intro about the experiment. You can tell about absorption of light by different coloured objects.
2.	Predict in which test tube the air warms up fastest and why. 
3.	Discuss the results with the students.

**Part 2:**
4.	Explain that you will now fill the test tubes with hot water and then measure the cooling. 
5.	Predict in which test tube the water cools down fastest and why.
6.	Perform the test. 
7.	Discuss the results of the test with the students.
8.	Control question: Does it matter what colour the radiators in a house are? 

```{figure} demo99_figure3.jpeg
---
width: 50%
align: center
---
The cooling test tube tubes radiating equally in the infrared.
```

```{figure} demo99_figure4.jpeg
---
width: 50%
align: center
---
Heating and cooling of the two test tubes. When cooling down, the white test tube is slightly warmer, but the cooling curve is almost identical to that of the black test tube. To clearly show the difference of cooling, the (T,t) graph of the white tube is started slightly earlier.
```
## Physics background
In the infrared, both white and black paint will absorb the same amount of radiation. Since the black test tube absorbs more radiation in the visible part, it will rise in temperature faster.

When objects start emitting ir radiation, it is quite a different story. The emissivity of an object determines how well it can emit ir radiation. Since the emissivity of white and black paint is almost the same, they will emit the same amount of ir-radiation and therefore cool down equally fast {cite:p}`bartels1990darker`.


```{tip}
You can video-stream with the FLIR-ir camera so that you can also clearly see that the test tubes differ in temperature in part 1 and are and remain approximately the same temperature in part 2 (see figure 4).
Use a halogen spotlight or a lampshade to focus the light better otherwise you will blind the students and they will not see the test as clearly.
```


## References
```{bibliography}
:filter: docname in docnames
```