

# Rotating cubes

Author: Karel Langendonck    \
Time: 10 - 30 minutes	  	\
Age group:	Grade 10 and higher\
Concepts:	

## Introduction
Students often find concepts linked to circular motion difficult. Examples include the difference between orbital velocity and angular velocity and understanding the centripetal force. In this demonstration, combine these concepts with the frictional force to create a situation where only one force provides the centripetal force. 

```{figure} demo58_figure1.JPG
---
width: 50%
align: center
---
The turntable with LEGO bricks
```

## Equipment
Turntable; 
cubes of not too large mass (e.g. LEGO bricks or DUPLO dolls); 
scales; 
ruler.

## Preparation
Place a cube with a not too large mass on the turntable of a record player, as shown in the picture opposite. LEGO bricks have been used as mass cubes in this setup. Determine the distance from the centre at which the cube will just slide.

## Procedure
1.	Place a cube at a not too large distance from the centre of the turntable and measure this distance (r).
2.	Turn on the turntable. If all goes well, the cube will remain on the turntable.
3.	Have students calculate the orbital velocity and angular velocity of the cube. Have them do this for both a turntable frequency of 33 rpm and 45 rpm.
4.	Have students calculate the sliding friction force acting on the cube.
5.	Stop the turntable again, place the cube slightly further from the centre of the turntable, measure the radius of the circular path again and turn the turntable on again. 
6.	Have students again calculate the orbital velocity, angular velocity and working sliding friction force.
7.	Repeat the above step as many times as necessary until the point is reached at which the cube swings off the turntable. At that point, the point is reached where the maximum sliding friction force on the cube is at issue. You can now calculate (or have calculated) the maximum value for the static coefficient of friction (and hence the dynamic coefficient of friction).
8.	Control question: Can you get a coefficient of friction greater than 1?

## Physics background
Three forces act on the cube (see figure 2): the gravitational force $F_z$, the normal force $F_N$ and the shear friction force $F_w$. At the moment the cube is on the turntable of the record player, it is the sliding friction force that provides the centripetal force. The following then applies:
$$F_w=F_{c}=\frac{mv^2}{r}$$

Also, $F_w = μ_s \cdot F_N$ 
From this, a formula for the static coefficient of friction μs can be derived.
For a derivation of this, REFERENCE TO WEBSITE.

```{figure} demo58_figure2.jpg
---
width: 50%
align: center
---
Three forces on the cube
```

```{tip}
- A few testers reported that the turntable was not smooth enough. Without the rubber mat or with a plastic cover around the mat or smeared with oil, it went well.
- This experiment is not very exciting in its execution, so it is important to involve the students well in the execution and collection of the measurement results. You can then let the students calculate some things in between. 
- Calculating the orbital period of the cube (and thus the orbital and angular velocity) will not be as simple and logical for all pupils. Pay explicit attention to this.
```
## Further investigation
In the experiment, you can vary the orbital time (where a frequency of 78 rpm is also possible on certain types of turntables), the mass of the cube and the type of surface of the cube and/or turntable. This allows experimentation with a diversity of variables, which also makes the experiment suitable as a practical for students.
One of the testers reports: On the same radius, you can apply different masses and compare whether the mass makes a difference. Is a larger mass more likely to be thrown off the turntable?


## References
```{bibliography}
:filter: docname in docnames
```