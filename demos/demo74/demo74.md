

# Shadow of a flame: Visualisation of heat flow

Author:     \
Time:	  	15-20 minutes\
Age group:	14 - 18\
Concepts:	

## Introduction
This is a demonstration everyone must see because it showcases such beautiful physics. Depending on the purpose, you can emphasize different aspects.

```{figure} demo74_fig1.JPG
---
width: 50%
align: center
---
The density current is visible
```

## Equipment
* Tea lights
* Dark room
* Strong lightsource (e.g. phone flashlight)
* Ice cream

## Preparation
None

## Procedure
Start this demo with the rotating spiral (https://www.nvon.nl/leswerk/proevenboek-51a-de-draaiende-spiraal) and let the students explain what is happening. You can also feel the heat flow yourself. Just keep your hand at various places, always the same distance from the flame. You will naturally notice where you feel the heat the most. Then, make the flow visible by darkening the room and holding a bright lamp close to the flame, projecting the flow onto the wall.

***Provide an explanation for why the flow is visible.***

It's nice to attach an ice cream to a stand. You can also feel the flow by holding your hand in the airflow

***What direction does the flow have?***

```{figure} demo74_fig2.JPG
---
width: 50%
align: center
---
The shadow of a flame
```

## Physics background
"The flame heats the air. The density of the warm air is now lower than that of the surroundings, causing it to rise. Due to the density difference in the air, light passes through different media and undergoes refraction. This can be seen as 'the shadow of the flame'. The flow visible here is an example of this. Initially, there is laminar flow, but it becomes turbulent at a certain point. Gently wave your hand and you'll see the laminar flow immediately become turbulent.

