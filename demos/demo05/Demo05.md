

# No Force Needed to Sustain Constant Speed: Newton’s First Law

Author:     Peter Dekkers\
Time:	  	  15-45 minutes\
Age group:	15 - 16\
Concepts:	  Force, speed, acceleration, Newton’s first law

## Introduction
In this demonstration of Newton’s First Law you can direct students’ thoughts and observations to details that are easily overlooked in a students' practical. By challenging students to express their expectations/predictions, compare these and then test them in observations, you can direct the way the students adjust and shape their concepts and beliefs. Different from most demonstrations of Newton’s First Law, no attempt is made to reduce friction to zero. It is merely made negligible.

## Equipment
A smooth, flat table that is as long as possible. A retort stand that is as high as possible, with clamps and two pulleys; a hanger with slotted masses; a thin cord that is twice as long as the table; two spring-balances. A cart. Power source and electromotor; two more retort stands each with a clamp and a pulley.

```{figure} demo05_figure1.JPG
---
width: 50%
align: center
---
A schematic of the experimental setup
```

## Preparation
Build the set-up, see figure 1. Pull the cart toward the right by hand. A mass, attached to the cart by a cord running along pulleys, creates an opposing force. The table is as long as possible, the top pulley is positioned as high as possible, so as to create a long runway. An electric motor (not shown) propels the revolving cord. Select slotted masses so that friction is negligible up to speeds of about 1 m/s, and choose matching spring-balances.

## Procedure
The cord is revolving uniformly, so a knot in the cord moves along at constant speed. So if you pull along the cart, with a hand that moves along with the knot, that cart has the same constant speed as the knot. The hand and cart take a moment to attain that speed, but then the spring-balances can be read. 
*How does the pulling force compare to the opposing force, during that part of the movement? What changes if we select a greater constant speed?* Answering these questions is the aim of the demonstration.

A Predict-Observe-Explain approach is appropriate here. An example is presented in the worksheet below. Students predict in advance, in writing, what they expect will be measured in answering the two questions. Then the experiment is carried out. A (slightly) larger speed is realised by increasing the potential difference driving the electric motor. A conclusion is formulated on the basis of the observations. It is applied in similar contexts to explore whether it is generally applicable. 

```{figure} demo05_figure2.JPG
---
width: 50%
align: center
---
The actual setup
```

## Physics background
If an object moves along at constant velocity, with both a propelling force and a force counteracting the propulsion acting on it, then these forces are equal, irrespective of the speed of the object. Furthermore, maintaining a larger speed does not require a larger propelling force, if friction is negligible. To maintain the speed, it is enough for propelling force and counteracting force to be equal in size. The ultimate example is a space ship, where the rocket engine can be switched off once the target speed has been reached. *It is sometimes claimed that Newton’s First Law can be verified only in these idealised, frictionless circumstances. But here we see that also in ordinary situations, the net force is zero if the velocity is constant.*

```{tip}
:class: dropdown
Teachers sometimes tend to omit the revolving cord, but then how do you know or verify that the speed is constant?  
The electric motor might be attached directly to the cart, to pull it along. However, especially if you involve students in carrying out the measurements, there is added value in students feeling in their hands that maintaining a higher speed does not require a greater pull. 

Both pulling forces must be made to act at the same height on the cart, or torque may affect and disturb the measurements. If during testing the forward force turns out to be greater than the opposing force, check if the cord did not fall off a pulley. If not, increase the hanging mass. Using about 0.3 kg is normally enough to make friction negligible.  

It is not so easy to maintain a constant speed, (let your assistants) practice a few times before measuring. 
In a prototype version of the demonstration, studying the at-rest situation was included. This turned out to be (1) distracting, because friction can no longer be neglected, and (2) unnecessary, because students are well aware that the forces should be equal in that case.
```

## Follow-up
Students often start raising objections after a short while: “Yes but, surely the forces are not equal at the beginning, when the cart starts to move?” Be alert to note and respond to these remarks, because this is precisely where students’ views do accord with Newton’s. From a science educational perspective, our aim is that students come to distinguish conceptually between situations where the cart is accelerating, and those where the speed is constant. Rather than exposing ‘misconceptions’, pay a great deal of attention to this conceptual differentiation, when students express it. Encourage the idea that physics does accord with what they already know about motion and its causes. But express that knowledge more precisely and accurately. 
Obviously, a single experiment will not cause students to expand and differentiate their understandings of force and motion to become fully Newtonian. Many students will still speak of the ‘force of motion’ when they explain why a skater or cyclist keep on moving beyond the finish line, after they stop propelling themselves. In developing Newtonian descriptions and explanations, students need to know when and why something is called a force in physics, and develop the concept of ‘interaction’ {cite}`dekkers1998`, and distinguish force from momentum.

## References
```{bibliography}
:filter: docname in docnames
```