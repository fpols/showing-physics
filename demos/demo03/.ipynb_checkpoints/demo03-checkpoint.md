# Light in darkness

Author: Freek Pols\
Time: 15-20 minuten\
Age group: 12-18\
Concepts: Light, light matter interaction

## Introduction
What is light? Why is it dark at night while there are so many stars? Why do we see objects (not)? These are relevant questions that we ask ourselves. But these are as well interesting questions from a physics perspective. This demo, adapted from Demircioglu & Cin (2019), lets students consider answers to these questions in an inquisitive way.

<video id="myVideo" width="240" height="320" controls>
 	  	<source src="laserdemo.mp4" type="video/mp4">
 		Your user agent does not support the HTML5 Video element.
	</video>
    
## Equipment
* Laser
* vacuumpump & bell
* darkened room
* lightning matches
* candle

## Preparation
Light the candle and put it under the vacuumbell so that smoke is trapped beneath it. Make sure there is enough smoke! The smoke will make the laserbeam visible, see figure 1. Darken the classroom but leave the lights on.

![figure](dm03_figure1.jpg) <center><i> Fig.1 - The demo is easy to set up.</i></center>

## Procedure
Start the demo with asking the broad question why it is dark at night even with so many stars. If there are countless stars, would the sky not be bright at night as well? Explain that the vacuumbell can be used as a model for the universe: there are hardly particles in space. Moreover, we can be regarded as a star.
 
Turn the laser on and ask students what they observe. Is the laserbeam visible? Everywhere? Outside and inside the bell? Now, turn of the lights. What can we see now? Is the laserbeam visible in and outside the bell? Is there any difference in light intensity? If so, what is a possible reason (hypothesis)? How can we test this hypothesis?

![figure](dm03_figure2.jpg) <center><i> Fig.2 - The laserbeam is well visible, but the entire bell is illuminated as well!</i></center>

Ask students to write down their observation when you turn on the vacuumpump. If the bell has been sufficiently vacuumed, you can turn off the pump and ask whether there is light visible in the bell jar. Is there still light anyway? Why do we see things, and how do we relate this demo, and what happens, with being dark at night?

![figure2](dm03_figure3.jpg) <center><i> Fig.3 - When the bell got more and more vcuum, the lightintensity of the laserbeam is less until it is not visible at all anymore.</i></center>

## Physics background
To see objects, there needs to be a light-matter interaction. Because the universe is almost entirely empty, these interactions are hardly there. The light that we see at night are directly coming from the stars. Many lightbeams 'miss' the earth and will never be observed. There are some exemptions of the light-matter interactions. The moon does not produce light itself but reflects the light coming from the sun. 

## References
Demircioglu, S., Cin, M.O., 2019, An argumentation-based demonstration experiment in teaching the light–matter interaction, Physics Education

