# Making a slanting tower stacking blocks

Author:     Maarten van Woerkom\
Time: 		20 minutes\
Age group: 	12 and up\
Concepts: 	center of mass, stable equilibrium

```{figure} demo04_figure6.jpg
---
width: 50%
align: center
---
A challenge: Building a slanting tower where the top block is not above the bottom block.
```

## Introduction
If you put two blocks on top of each other, then the upper one remains stable if the center of mass is above the lower block. So the upper block cannot stick out more than ½ x the length of the block. <br>
*Would it be possible to pile up blocks such that the upper one sticks out farther?*

## Equipment
* A number of equal blocks.
```{figure} demo04_figure1.jpg
---
width: 50%
align: center
---
The demo requires only some blocks.
```

## Procedure
Pile some blocks on top of each other with the instruction: *how can you make the end of the top block stick out as much as possible?* In other words, *how can you achieve the maximum distance between the front of the top block and the front of the bottom block?* You can use as many blocks as you want.
Is it possible that the back of the top block passes the front of the bottom block?

```{figure} demo04_figure2.jpg
---
width: 50%
align: center
---
We can try to use the approach that first comes into mind... but probably won't succeed.
```

Usually blocks are stacked one by one. See the 4 blocks in figure 2. First the green block (1), then the orange block (2) that sticks out 1/3 of the block length, then another orange block (3) that also sticks out 1/3 of the block length.<br>
The center of mass of the orange blocks together is now exactly above the left side of the green block at the bottom. So if you would place another block sticking out, the red one, the pile would tip over...<br>
It is also possible to arrange the pile in reversed order: from top to bottom (figure 3). Put block one on top of block 2 (counting from above), then blocks 1 and 2 on top of block 3, etc.

```{figure} demo04_figure5.jpg
---
width: 50%
align: center
---
Building the pile in reversed order requires to find the center of mass each time.
```

## Physics background
When you use the reverse piling method, starting from the top, then very quickly you reach a point where the overhang is greater than the block length. 
* Center of mass of block 1 is above the front of block 2;
* Center of mass of block 1 and block 2 together is above the front of block 3;
* Center of mass of blocks 1 – 3 is above the front of block 4.
All of that above block 5 and the overhang is already more than one block length.

```{figure} demo04_figure3.jpg
---
width: 50%
align: center
---
The physics (and mathematics) behind stacking blocks
```

```{Tip}
* Instead of blocks of wood you can also use CD cases or big Lego blocks.
* If you google with the term “markthal Rotterdam”, then you will see on the photographs that the 228 apartments were stacked in a way you will now understand. Teacher Eline Bijker actually started her demonstration of stacking blocks with a photograph of the market hall in Rotterdam (figure 4) and asked her students which construction principle was used. The Rotterdam “markthal” is an indoor market with apartments stacked into an arch shape overhead. It was opened in 2014.
```

```{figure} demo04_figure4.jpg
---
width: 50%
align: center
---
The market hall in Rotterdam is stacked accourding to the principle above. Figure obtained from: https://commons.wikimedia.org/wiki/File:Markthal-Rotterdam.jpg
```

