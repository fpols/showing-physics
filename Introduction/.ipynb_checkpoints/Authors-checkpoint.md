# About the authors

**Leo te Brinke**

**Ed van den Berg** is a retired physics teacher trainer and got his PhD. in Physics Education - on the topic of teaching practical work. 

```{image} authors/auth_Peter.jpg
:width: 150px
:align: left
```
**Peter Dekkers** is a physics teacher trainer and got his PhD. in Physics Education - on the topic of teaching practical work. 
<br><br><br><p>
\_
```{image} authors/auth_Ineke.jpg
:width: 150px
:align: left
```
**Ineke Frederik** is a retired physics teacher trainer. 

```{image} authors/auth_Karel.jpg
:width: 150px
:align: left
```
**Karel Langendonck** was a secondary school physics teacher and is now a physics teacher trainer at the ...

```{image} authors/auth_Freek.jpg
:width: 150px
:align: left
```
**Freek Pols** was a secondary school physics teacher, got his PhD. in Physics Education - on the topic of teaching scientific inquiry - and is the coordinator of the first year physics lab course.

**Wim Sonneveld** is a retired secondary school physics teacher and physics teacher trainer. 

**Wouter Spaan** is a physics teacher trainer at the Hogeschool van Amsterdam.

```{image} authors/auth_Kirsten.jpg
:width: 150px
:align: left
```
**Kirsten Stadermann** was a secondary school physics teacher, got her PhD. in Physics Education - on the topic of nature of science in teaching quantum mechanice - and is now a physics teacher trainer.

```{image} authors/auth_Norbert.JPG
:width: 150px
:align: left
```
**Norbert van Veen** is a secondary school physics teacher and trainer at CMA.

**Maarten van Woerkom**

**Contributors**
    